﻿using Utils;
using System;
//using Sap.Data.Hana;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;

namespace DataBase
{
    static class Conexao
    {
        #region Consulta Banco
        public static DataTable ConsultaBanco(string sQuery)
        {
            if (ConstantUtils.Servidor != "Server2016\\SQLEXPRESS")
            {
                //HanaConnection conn = new HanaConnection($"Server={ConstantUtils.Servidor};UserID={ConstantUtils.SAP_USER_BD};Password={ConstantUtils.SAP_SENHA_BD}");

                //conn.Open();

                //HanaDataAdapter dataAdapter = new HanaDataAdapter(sQuery, conn);
                //// Create a new DataTable and use your adapter to fill the table.
                DataTable table = new DataTable();
                //dataAdapter.Fill(table);

                //conn.Close();

                return table;
            }
            else
            {
                SqlConnection connSQL = new SqlConnection($"Server={ConstantUtils.Servidor};Database={ConstantUtils.SAP_BASE};User ID={ConstantUtils.SAP_USER_BD};Password={ConstantUtils.SAP_SENHA_BD}");

                connSQL.Open();

                SqlDataAdapter dataAdapterSQL = new SqlDataAdapter(sQuery, connSQL);

                DataTable tableSQL = new DataTable();
                dataAdapterSQL.Fill(tableSQL);

                connSQL.Close();

                return tableSQL;
            }  
        }
        #endregion


        #region ConsultaBanco: Sobrecarga TEntity
        public static TEntity ConsultaBanco<TEntity>(string QUERY)
        {
            DataTable DATA_TABLE = new DataTable();
            string DATA_JSON = string.Empty;

            TEntity OBJECT;

            if (ConstantUtils.Servidor != "Server2016\\SQLEXPRESS")
            {
                //HanaConnection conn = new HanaConnection($"Server={ConstantUtils.Servidor};UserID={ConstantUtils.SAP_USER_BD};Password={ConstantUtils.SAP_SENHA_BD}");

                //conn.Open();

                //HanaDataAdapter dataAdapter = new HanaDataAdapter(QUERY, conn);
                //// Create a new DataTable and use your adapter to fill the table.
                //DataTable table = new DataTable();
                //dataAdapter.Fill(DATA_TABLE);

                //conn.Close();
            }
            else
            {
                SqlConnection connSQL = new SqlConnection($"Server={ConstantUtils.Servidor};Database={ConstantUtils.SAP_BASE};User ID={ConstantUtils.SAP_USER_BD};Password={ConstantUtils.SAP_SENHA_BD}");

                connSQL.Open();

                SqlDataAdapter dataAdapterSQL = new SqlDataAdapter(QUERY, connSQL);
                dataAdapterSQL.Fill(DATA_TABLE);

                connSQL.Close();
            }
           
            if (DATA_TABLE.Rows.Count > 0)
            {
                DATA_JSON = DATA_TABLE.Rows[0][0].ToString();
            }
            else
            {
                DATA_JSON = "";
            }

            OBJECT = JsonConvert.DeserializeObject<TEntity>(DATA_JSON);


            return OBJECT;
        }
        #endregion
    }
}
