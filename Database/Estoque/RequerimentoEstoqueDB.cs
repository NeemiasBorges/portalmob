﻿using Database;
using Models;
//using PowerOne.Repositorio;
using PowerOne.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PowerOne.Database.Estoque
{
    public class RequerimentoEstoqueDB
    {
        bool bCriou = false;

        ConsultaGeral oConsultaGeral = new ConsultaGeral();

        public void CreateTable()
        {
            if (!oConsultaGeral.VerificaTabela("P1_OIGE".ToUpper()))
            {
                bCriou = MetaData.CriaTabelasUsuario("P1_OIGE", "P1 - Req.Saida Mercad.", SAPbobsCOM.BoUTBTableType.bott_Document);
            }
        }

        public void AddField()
        {
            if (!oConsultaGeral.VerificaCampo("P1_NOMEFILIAL", "@P1_OIGE"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_NOMEFILIAL", "P1_OIGE", "Nome da Filial do Depósito", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, "", null);
            }

            if (!oConsultaGeral.VerificaCampo("P1_IDFILIAL", "@P1_OIGE"))
            {
               bCriou = MetaData.CriaCampoUsuario("P1_IDFILIAL", "P1_OIGE", "ID da Filial do Depósito", SAPbobsCOM.BoFieldTypes.db_Numeric, SAPbobsCOM.BoFldSubTypes.st_None, 11, "",  null);
            }

            if (!oConsultaGeral.VerificaCampo("P1_STATUS", "@P1_OIGE"))
            {
              bCriou =  MetaData.CriaCampoUsuario("P1_STATUS", "P1_OIGE", "STATUS", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 2, "", null);
            }

            if (!oConsultaGeral.VerificaCampo("P1_DTLANC", "@P1_OIGE"))
            {
               bCriou = MetaData.CriaCampoUsuario("P1_DTLANC", "P1_OIGE", "DATA LANÇAMENTO", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 15, "", null);
            }

            if (!oConsultaGeral.VerificaCampo("P1_DTEFET", "@P1_OIGE"))
            {
               bCriou = MetaData.CriaCampoUsuario("P1_DTEFET", "P1_OIGE", "DATA EFETIVAÇÃO", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 15, "", null);
            }

            if (!oConsultaGeral.VerificaCampo("P1_Owner", "@P1_OIGE"))
            {
              bCriou =  MetaData.CriaCampoUsuario("P1_Owner", "P1_OIGE", "Solicitante", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 254, "", null);
            }

            if (!oConsultaGeral.VerificaCampo("P1_Comments", "@P1_OIGE"))
            {
              bCriou =  MetaData.CriaCampoUsuario("P1_Comments", "P1_OIGE", "Observações", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 254, "", null);
            }
        }
    }
}