﻿using Models;
using PortalMOB.Database;
using System.Collections.Generic;

namespace Database.Estoque
{
    public class Item
    {
        bool bCriou = false;
        ConsultaGeral oConsultaGeral = new ConsultaGeral();
        public void AddField()
        {
            List<ValoresValidos> oValoresValidos = new List<ValoresValidos>();

            oValoresValidos.Add(new ValoresValidos { Valor = "RB", Descricao = "Reembolso" });
            oValoresValidos.Add(new ValoresValidos { Valor = "LD", Descricao = "Lançamento de Despesas" });
            oValoresValidos.Add(new ValoresValidos { Valor = "PC", Descricao = "Prestação de Compras" });
            oValoresValidos.Add(new ValoresValidos { Valor = "", Descricao = "Geral" });

            if (!oConsultaGeral.VerificaCampo("P1_ITMF", "OITM"))
            {
                bCriou = MetaData.CriaCampoUsuario("P1_ITMF", "OITM", "Categoria do Item", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, "", oValoresValidos);
            }
        }
    }
}
