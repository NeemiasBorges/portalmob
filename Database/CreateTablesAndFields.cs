﻿using PowerOne.Database.Estoque;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Database
{
    public class CreateTablesAndFields
    {
        public static void Main()
        {
            /*Requerimento Estoque*/
            RequerimentoEstoque();
        }

        #region Requerimento Estoque
        public static void RequerimentoEstoque()
        {
            RequerimentoEstoqueDB requerimentoEstoqueDB = new RequerimentoEstoqueDB();
            RequerimentoEstoqueLinhasDB requerimentoEstoqueLinhasDB = new RequerimentoEstoqueLinhasDB();

            requerimentoEstoqueDB.CreateTable();
            requerimentoEstoqueDB.AddField();

            requerimentoEstoqueLinhasDB.CreateTable();
            requerimentoEstoqueLinhasDB.AddField();
        }
        #endregion
    }
}