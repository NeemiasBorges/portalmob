﻿using PortalMOB.Models;
using PowerOne.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PortalMOB.Repositories.Interfaces
{
    public interface IConfiguracoesRepository
    {
        Tema GetTema(string code);
        Resultado ConfigurarTema(string code, string cor, string icone);
        ConfiguracoesEmail GetConfiguracoesEmail();

    }
}