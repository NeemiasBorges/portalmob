﻿using PowerOne.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PortalMOB.Repositories.Interfaces
{
    public interface IAutorizacoesRepository
    {
        string CriarAutorizacoes(Login user);
        Autorizacoes PegarAutorizacoes(string code);
        AutorizacoesGrupo PegarGrupoAutorizacoes(Autorizacoes autorizacoes);
    }
}