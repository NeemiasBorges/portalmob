﻿using PowerOne.Models;
using PowerOne.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PortalMOB.Repositories.Interfaces
{
    public interface ILoginRepository
    {

        Login Login(Login login);
        int ConsultaId(Login login);

        bool ConsultaCadastro(int Id_User);
        string AddAcess(Login login, int id_User);
        string GetMacAddress();

        void EditarSenha(AlterarSenhaViewModel alterarSenha);

        string GetIdUsuarioPorEmail(string Email);
       
    }
}