﻿using Models.Estoque;
using PowerOne.Models;
using System.Collections.Generic;

namespace PortalMOB.Repositories.Interfaces
{
    public interface IEstoqueRepository
    {
        int contEstoque();

        List<RequerimentoEstoque> List(Login login, string dtInicio, string dtFim, string status);

        RequerimentoEstoque GetByKey(int DocEntry);
    }
}