﻿using DataBase;
using Newtonsoft.Json;
using PortalMOB.Repositories.Interfaces;
using PowerOne.Database;
using PowerOne.Models;
using PowerOne.Util;
using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace PowerOne.Repository
{



    public class AutorizacoesRepository : IAutorizacoesRepository
    {
        //#region Serviços 
        //LoginService _loginService = new LoginService();
        //#endregion

        //#region Criar Autorizações para Todos os Usuários
        //public string CriarAutorizacoes()
        //{
        //    //SAPbobsCOM.Company oCompany = Conexao.Company;
        //    //SAPbobsCOM.UserTable oAutorizacoes = oCompany.UserTables.Item("P1_ACCESS");

        //    string msg = null;

        //    //List<Login> idusers = _loginService.GetAllIdUsers();

        //    //try
        //    //{
        //    //    foreach (Login user in idusers)
        //    //    {
        //    //        if (!oAutorizacoes.GetByKey(user.Id.ToString()))
        //    //        {
        //    //            oAutorizacoes.Code = user.Id.ToString();
        //    //            oAutorizacoes.Name = user.User;
        //    //            oAutorizacoes.UserFields.Fields.Item("U_P1_EMPId").Value = user.Id.ToString();
        //    //            if (oAutorizacoes.Add() != 0)
        //    //            {
        //    //                msg = oCompany.GetLastErrorDescription();
        //    //            }
        //    //        }
        //    //    }
        //    //}
        //    //catch (Exception e)
        //    //{
        //    //    msg = e.Message;
        //    //}

        //    //System.Runtime.InteropServices.Marshal.ReleaseComObject(oAutorizacoes);


        //    //if (msg != null)
        //    //{
        //    //    Log.Gravar(msg, Log.TipoLog.Erro);
        //    //}

        //    return msg;
        //}
        //#endregion

        #region Criar Autorizacao 
        public string CriarAutorizacoes(Login user)
        {
            //SAPbobsCOM.Company oCompany = Conexao.Company;
            //SAPbobsCOM.UserTable oAutorizacoes = oCompany.UserTables.Item("P1_ACCESS");

            string msg = null;

            //if (!oAutorizacoes.GetByKey(user.Id.ToString()))
            //{
            //    oAutorizacoes.Code = user.Id.ToString();
            //    oAutorizacoes.Name = user.User;
            //    oAutorizacoes.UserFields.Fields.Item("U_P1_EMPId").Value = user.Id.ToString();
            //    if (oAutorizacoes.Add() != 0)
            //    {
            //        msg = oCompany.GetLastErrorDescription();
            //    }
            //}

            //if (msg != null)
            //{
            //    Log.Gravar(msg, Log.TipoLog.Erro);
            //}

            return msg;
        }
        #endregion

        #region Pesquisar Autorizações do Usuário
        public Autorizacoes PegarAutorizacoes(string code)
        {
            Autorizacoes autorizacao = new Autorizacoes();
            try
            {

                StringBuilder SQL = new StringBuilder();
                SQL.AppendLine("SELECT  \"Code\" AS \"UserId\",  \"Name\" AS \"UserName\" ,COALESCE(\"U_P1_Grupo\",0) AS \"IdGrupo\", \"U_P1_ReqS\" AS \"ReqEstoque\" ,  \"U_P1_NewReqS\" AS \"CriarReqEstoque\" ,\"U_P1_Type\" AS \"Tipo\" ," +
                    "\"U_P1_LSTATUS\" AS \"Licensa\" ,\"U_P1_Coorder\" AS \"Coordenador\" FROM \"@P1_ACCESS\"");
                SQL.AppendLine("WHERE \"Code\" = '" + code + "' FOR JSON PATH");
                
                autorizacao = Conexao.ConsultaBanco<Autorizacoes[]>(SQL.ToString())[0];
            }
            catch (Exception e)
            {

                autorizacao = JsonConvert.DeserializeObject<Autorizacoes>("{\"ReqEstoque\": 'R',\"CriarReqEstoque\": 'R',\"Coordenador\" : 'N',\"IdGrupo\" : '0'}");
            }
            return autorizacao;
        }
        #endregion

        //#region Pegar Autorizações
        //public List<Autorizacoes> List()
        //{
        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

        //    List<Autorizacoes> autorizacoes = new List<Autorizacoes>();

        //    StringBuilder SQL = new StringBuilder();
        //    SQL.AppendLine("SELECT T0.*, T1.\"firstName\", T1.\"lastName\" FROM \"@P1_ACCESS\" T0 ");
        //    SQL.AppendLine("INNER JOIN \"OHEM\" T1 ON (T0.\"U_P1_EMPId\" = T1.\"empID\") ");
        //    SQL.AppendLine("ORDER BY CAST(\"U_P1_EMPId\" as NUMERIC)");

        //    oRecordSet.DoQuery(SQL.ToString());
        //    while (!oRecordSet.EoF)
        //    {
        //        Autorizacoes autorizacao = new Autorizacoes();
        //        autorizacao.UserId = int.Parse(oRecordSet.Fields.Item("Code").Value.ToString());
        //        autorizacao.UserName = oRecordSet.Fields.Item("firstName").Value.ToString();
        //        autorizacao.UserLastName = oRecordSet.Fields.Item("lastName").Value.ToString();
        //        autorizacao.SolCompra = oRecordSet.Fields.Item("U_P1_ConsSol").Value.ToString();
        //        autorizacao.CriarSolCompra = oRecordSet.Fields.Item("U_P1_NewSol").Value.ToString();
        //        autorizacao.PedCompra = oRecordSet.Fields.Item("U_P1_PedComp").Value.ToString();
        //        autorizacao.CriarPedCompra = oRecordSet.Fields.Item("U_P1_NewPedC").Value.ToString();
        //        autorizacao.ReqEstoque = oRecordSet.Fields.Item("U_P1_ReqS").Value.ToString();
        //        autorizacao.CriarReqEstoque = oRecordSet.Fields.Item("U_P1_NewReqS").Value.ToString();
        //        autorizacao.Reembolso = oRecordSet.Fields.Item("U_P1_Reemb").Value.ToString();
        //        autorizacao.CriarReembolso = oRecordSet.Fields.Item("U_P1_NewReem").Value.ToString();
        //        autorizacao.CriarModeloEmail = oRecordSet.Fields.Item("U_P1_NewMailM").Value.ToString();
        //        autorizacao.ModeloEmail = oRecordSet.Fields.Item("U_P1_MailMod").Value.ToString();
        //        autorizacao.AssistEnvio = oRecordSet.Fields.Item("U_P1_AstEnvio").Value.ToString();
        //        autorizacao.Clientes = oRecordSet.Fields.Item("U_P1_Client").Value.ToString();
        //        autorizacao.CriarGrpClientes = oRecordSet.Fields.Item("U_P1_NewGPCnt").Value.ToString();
        //        autorizacao.GrpClientes = oRecordSet.Fields.Item("U_P1_GPCnt").Value.ToString();
        //        autorizacao.RegistroEnvEmail = oRecordSet.Fields.Item("U_P1_LogEmail").Value.ToString();
        //        autorizacao.RelatCobranças = oRecordSet.Fields.Item("U_P1_RTCobran").Value.ToString();
        //        autorizacao.IdGrupo = oRecordSet.Fields.Item("U_P1_Grupo").Value.ToString();
        //        autorizacao.Tipo = oRecordSet.Fields.Item("U_P1_Type").Value.ToString();
        //        autorizacao.Licensa = oRecordSet.Fields.Item("U_P1_LSTATUS").Value.ToString();
        //        autorizacao.CriarLancamentoDesp = oRecordSet.Fields.Item("U_P1_NewDesp").Value.ToString();
        //        autorizacao.LancamentoDesp = oRecordSet.Fields.Item("U_P1_Desp").Value.ToString();
        //        autorizacao.CriarPrestacaoContas = oRecordSet.Fields.Item("U_P1_PrestAcc").Value.ToString();
        //        autorizacao.PrestacaoContas = oRecordSet.Fields.Item("U_P1_NewPrest").Value.ToString();
        //        autorizacao.RevisaoHoras = oRecordSet.Fields.Item("U_P1_RHora").Value.ToString();
        //        autorizacao.ApontamentoHoras = oRecordSet.Fields.Item("U_P1_APHoras").Value.ToString();
        //        autorizacao.Revisor = oRecordSet.Fields.Item("U_P1_RevHora").Value.ToString();
        //        autorizacao.RelatorioTimesheet = oRecordSet.Fields.Item("U_P1_ReportTS").Value.ToString();
        //        autorizacao.VisualizarContratos = oRecordSet.Fields.Item("U_P1_ContractView").Value.ToString();
        //        autorizacao.PN = oRecordSet.Fields.Item("U_P1_PN").Value.ToString();
        //        autorizacao.OportVenda = oRecordSet.Fields.Item("U_P1_OV").Value.ToString();
        //        autorizacao.MembroComite = oRecordSet.Fields.Item("U_P1_Comite").Value.ToString();


        //        autorizacao.Coordenador = oRecordSet.Fields.Item("U_P1_Coorder").Value.ToString();
        //        autorizacao.ConsultaPN = oRecordSet.Fields.Item("U_P1_SearchPN").Value.ToString();

        //        autorizacoes.Add(autorizacao);

        //        oRecordSet.MoveNext();
        //    }


        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
        //    oRecordSet = null;

        //    return autorizacoes;
        //}
        //#endregion

        //#region Pegar Autorizações do Grupo
        //public List<AutorizacoesGrupo> ListGrupo()
        //{
        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

        //    List<AutorizacoesGrupo> autorizacoes = new List<AutorizacoesGrupo>();

        //    StringBuilder SQL = new StringBuilder();
        //    SQL.AppendLine("SELECT * FROM \"@P1_ACCGRP\"");

        //    oRecordSet.DoQuery(SQL.ToString());
        //    while (!oRecordSet.EoF)
        //    {
        //        AutorizacoesGrupo autorizacao = new AutorizacoesGrupo();
        //        autorizacao.Code = oRecordSet.Fields.Item("Code").Value.ToString();
        //        autorizacao.Name = oRecordSet.Fields.Item("Name").Value.ToString();
        //        autorizacao.SolCompra = oRecordSet.Fields.Item("U_P1_ConsSol").Value.ToString();
        //        autorizacao.CriarSolCompra = oRecordSet.Fields.Item("U_P1_NewSol").Value.ToString();
        //        autorizacao.PedCompra = oRecordSet.Fields.Item("U_P1_PedComp").Value.ToString();
        //        autorizacao.CriarPedCompra = oRecordSet.Fields.Item("U_P1_NewPedC").Value.ToString();
        //        autorizacao.ReqEstoque = oRecordSet.Fields.Item("U_P1_ReqS").Value.ToString();
        //        autorizacao.CriarReqEstoque = oRecordSet.Fields.Item("U_P1_NewReqS").Value.ToString();
        //        autorizacao.Reembolso = oRecordSet.Fields.Item("U_P1_Reemb").Value.ToString();
        //        autorizacao.CriarReembolso = oRecordSet.Fields.Item("U_P1_NewReem").Value.ToString();
        //        autorizacao.CriarModeloEmail = oRecordSet.Fields.Item("U_P1_NewMailM").Value.ToString();
        //        autorizacao.ModeloEmail = oRecordSet.Fields.Item("U_P1_MailMod").Value.ToString();
        //        autorizacao.AssistEnvio = oRecordSet.Fields.Item("U_P1_AstEnvio").Value.ToString();
        //        autorizacao.Clientes = oRecordSet.Fields.Item("U_P1_Client").Value.ToString();
        //        autorizacao.CriarGrpClientes = oRecordSet.Fields.Item("U_P1_NewGPCnt").Value.ToString();
        //        autorizacao.GrpClientes = oRecordSet.Fields.Item("U_P1_GPCnt").Value.ToString();
        //        autorizacao.RegistroEnvEmail = oRecordSet.Fields.Item("U_P1_LogEmail").Value.ToString();
        //        autorizacao.RelatCobranças = oRecordSet.Fields.Item("U_P1_RTCobran").Value.ToString();
        //        autorizacao.CriarLancamentoDesp = oRecordSet.Fields.Item("U_P1_NewDesp").Value.ToString();
        //        autorizacao.LancamentoDesp = oRecordSet.Fields.Item("U_P1_Desp").Value.ToString();
        //        autorizacao.CriarPrestacaoContas = oRecordSet.Fields.Item("U_P1_PrestAcc").Value.ToString();
        //        autorizacao.PrestacaoContas = oRecordSet.Fields.Item("U_P1_NewPrest").Value.ToString();
        //        autorizacao.RevisaoHoras = oRecordSet.Fields.Item("U_P1_RHora").Value.ToString();
        //        autorizacao.ApontamentoHoras = oRecordSet.Fields.Item("U_P1_APHoras").Value.ToString();
        //        autorizacao.Revisor = oRecordSet.Fields.Item("U_P1_RevHora").Value.ToString();
        //        autorizacao.RelatorioTimesheet = oRecordSet.Fields.Item("U_P1_ReportTS").Value.ToString();
        //        autorizacao.VisualizarContratos = oRecordSet.Fields.Item("U_P1_ContractView").Value.ToString();
        //        autorizacao.PN = oRecordSet.Fields.Item("U_P1_PN").Value.ToString();
        //        autorizacao.OportVenda = oRecordSet.Fields.Item("U_P1_OV").Value.ToString();
        //        autorizacao.MembroComite = oRecordSet.Fields.Item("U_P1_Comite").Value.ToString();
        //        autorizacao.ConsultaPN = oRecordSet.Fields.Item("U_P1_SearchPN").Value.ToString();

        //        autorizacoes.Add(autorizacao);

        //        oRecordSet.MoveNext();
        //    }

        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
        //    oRecordSet = null;

        //    return autorizacoes;
        //}
        //#endregion

        //#region Atualizar Autorizações
        //public string Update(Autorizacoes autorizacao)
        //{
        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.UserTable oAutorizacoes = oCompany.UserTables.Item("P1_ACCESS");

        //    string msg = null;
        //    if (oAutorizacoes.GetByKey(autorizacao.UserId.ToString()))
        //    {
        //        oAutorizacoes.UserFields.Fields.Item("U_P1_ConsSol").Value = autorizacao.SolCompra;
        //        oAutorizacoes.UserFields.Fields.Item("U_P1_NewSol").Value = autorizacao.CriarSolCompra;
        //        oAutorizacoes.UserFields.Fields.Item("U_P1_PedComp").Value = autorizacao.PedCompra;
        //        oAutorizacoes.UserFields.Fields.Item("U_P1_NewPedC").Value = autorizacao.CriarPedCompra;
        //        oAutorizacoes.UserFields.Fields.Item("U_P1_ReqS").Value = autorizacao.ReqEstoque;
        //        oAutorizacoes.UserFields.Fields.Item("U_P1_NewReqS").Value = autorizacao.CriarReqEstoque;
        //        oAutorizacoes.UserFields.Fields.Item("U_P1_Reemb").Value = autorizacao.Reembolso;
        //        oAutorizacoes.UserFields.Fields.Item("U_P1_NewReem").Value = autorizacao.CriarReembolso;
        //        //oAutorizacoes.UserFields.Fields.Item("U_P1_NewMailM").Value = autorizacao.CriarModeloEmail;
        //        //oAutorizacoes.UserFields.Fields.Item("U_P1_MailMod").Value = autorizacao.ModeloEmail;
        //        //oAutorizacoes.UserFields.Fields.Item("U_P1_AstEnvio").Value = autorizacao.AssistEnvio;
        //        //oAutorizacoes.UserFields.Fields.Item("U_P1_Client").Value = autorizacao.Clientes;
        //        //oAutorizacoes.UserFields.Fields.Item("U_P1_NewGPCnt").Value = autorizacao.CriarGrpClientes;
        //        //oAutorizacoes.UserFields.Fields.Item("U_P1_GPCnt").Value = autorizacao.GrpClientes;
        //        //oAutorizacoes.UserFields.Fields.Item("U_P1_LogEmail").Value = autorizacao.RegistroEnvEmail;
        //        //oAutorizacoes.UserFields.Fields.Item("U_P1_RTCobran").Value = autorizacao.RelatCobranças;
        //        oAutorizacoes.UserFields.Fields.Item("U_P1_NewDesp").Value = autorizacao.CriarLancamentoDesp;
        //        oAutorizacoes.UserFields.Fields.Item("U_P1_Desp").Value = autorizacao.LancamentoDesp;
        //        oAutorizacoes.UserFields.Fields.Item("U_P1_PrestAcc").Value = autorizacao.CriarPrestacaoContas;
        //        oAutorizacoes.UserFields.Fields.Item("U_P1_NewPrest").Value = autorizacao.PrestacaoContas;
        //        oAutorizacoes.UserFields.Fields.Item("U_P1_RHora").Value = autorizacao.RevisaoHoras;
        //        oAutorizacoes.UserFields.Fields.Item("U_P1_APHoras").Value = autorizacao.ApontamentoHoras;
        //        oAutorizacoes.UserFields.Fields.Item("U_P1_RevHora").Value = autorizacao.Revisor;
        //        oAutorizacoes.UserFields.Fields.Item("U_P1_ReportTS").Value = autorizacao.RelatorioTimesheet;
        //        oAutorizacoes.UserFields.Fields.Item("U_P1_ContractView").Value = autorizacao.VisualizarContratos;
        //        oAutorizacoes.UserFields.Fields.Item("U_P1_PN").Value = autorizacao.PN;
        //        oAutorizacoes.UserFields.Fields.Item("U_P1_OV").Value = autorizacao.OportVenda;
        //        oAutorizacoes.UserFields.Fields.Item("U_P1_Comite").Value = autorizacao.MembroComite;
        //        oAutorizacoes.UserFields.Fields.Item("U_P1_SearchPN").Value = autorizacao.ConsultaPN;

        //        oAutorizacoes.UserFields.Fields.Item("U_P1_Coorder").Value = autorizacao.Coordenador == "Y" ? autorizacao.Coordenador : "N";

        //        if (oAutorizacoes.Update() != 0)
        //        {
        //            msg = oCompany.GetLastErrorDescription();
        //        }
        //    }
        //    else
        //    {
        //        msg = "Usuário não encontrado";
        //    }

        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oAutorizacoes);

        //    if (msg != null)
        //    {
        //        Log.Gravar(msg, Log.TipoLog.Erro);
        //    }

        //    return msg;
        //}
        //#endregion

        //#region Criar Grupos
        //public string CriarGrupos(string code, string name)
        //{
        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.UserTable oAutorizacoes = oCompany.UserTables.Item("P1_AccGrp");

        //    string msg = null;
        //    oAutorizacoes.Code = code;
        //    oAutorizacoes.Name = name;
        //    if (oAutorizacoes.Add() != 0)
        //    {
        //        msg = oCompany.GetLastErrorDescription();
        //        Log.Gravar(msg, Log.TipoLog.Erro);
        //    }

        //    return msg;
        //}
        //#endregion

        //#region Atualizar Autorizações de Grupos
        //public string AtualizarAutorizacoesGrupos(AutorizacoesGrupo autorizacoes)
        //{
        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.UserTable oAutorizacoes = oCompany.UserTables.Item("P1_AccGrp");

        //    string msg = null;

        //    try
        //    {
        //        if (oAutorizacoes.GetByKey(autorizacoes.Code))
        //        {
        //            oAutorizacoes.UserFields.Fields.Item("U_P1_ConsSol").Value = autorizacoes.SolCompra;
        //            oAutorizacoes.UserFields.Fields.Item("U_P1_NewSol").Value = autorizacoes.CriarSolCompra;
        //            oAutorizacoes.UserFields.Fields.Item("U_P1_PedComp").Value = autorizacoes.PedCompra;
        //            oAutorizacoes.UserFields.Fields.Item("U_P1_NewPedC").Value = autorizacoes.CriarPedCompra;
        //            oAutorizacoes.UserFields.Fields.Item("U_P1_ReqS").Value = autorizacoes.ReqEstoque;
        //            oAutorizacoes.UserFields.Fields.Item("U_P1_NewReqS").Value = autorizacoes.CriarReqEstoque;
        //            oAutorizacoes.UserFields.Fields.Item("U_P1_Reemb").Value = autorizacoes.Reembolso;
        //            oAutorizacoes.UserFields.Fields.Item("U_P1_NewReem").Value = autorizacoes.CriarReembolso;

        //            //oAutorizacoes.UserFields.Fields.Item("U_P1_NewMailM").Value = autorizacoes.CriarModeloEmail;
        //            //oAutorizacoes.UserFields.Fields.Item("U_P1_MailMod").Value = autorizacoes.ModeloEmail;
        //            //oAutorizacoes.UserFields.Fields.Item("U_P1_AstEnvio").Value = autorizacoes.AssistEnvio;
        //            //oAutorizacoes.UserFields.Fields.Item("U_P1_Client").Value = autorizacoes.Clientes;
        //            //oAutorizacoes.UserFields.Fields.Item("U_P1_NewGPCnt").Value = autorizacoes.CriarGrpClientes;
        //            //oAutorizacoes.UserFields.Fields.Item("U_P1_GPCnt").Value = autorizacoes.GrpClientes;
        //            //oAutorizacoes.UserFields.Fields.Item("U_P1_LogEmail").Value = autorizacoes.RegistroEnvEmail;
        //            //oAutorizacoes.UserFields.Fields.Item("U_P1_RTCobran").Value = autorizacoes.RelatCobranças;

        //            oAutorizacoes.UserFields.Fields.Item("U_P1_NewDesp").Value = autorizacoes.CriarLancamentoDesp;
        //            oAutorizacoes.UserFields.Fields.Item("U_P1_Desp").Value = autorizacoes.LancamentoDesp;
        //            oAutorizacoes.UserFields.Fields.Item("U_P1_PrestAcc").Value = autorizacoes.CriarPrestacaoContas;
        //            oAutorizacoes.UserFields.Fields.Item("U_P1_NewPrest").Value = autorizacoes.PrestacaoContas;
        //            oAutorizacoes.UserFields.Fields.Item("U_P1_RHora").Value = autorizacoes.RevisaoHoras;
        //            oAutorizacoes.UserFields.Fields.Item("U_P1_APHoras").Value = autorizacoes.ApontamentoHoras;
        //            oAutorizacoes.UserFields.Fields.Item("U_P1_RevHora").Value = autorizacoes.Revisor;
        //            oAutorizacoes.UserFields.Fields.Item("U_P1_ReportTS").Value = autorizacoes.RelatorioTimesheet;
        //            oAutorizacoes.UserFields.Fields.Item("U_P1_ContractView").Value = autorizacoes.VisualizarContratos;
        //            oAutorizacoes.UserFields.Fields.Item("U_P1_PN").Value = autorizacoes.PN;
        //            oAutorizacoes.UserFields.Fields.Item("U_P1_OV").Value = autorizacoes.OportVenda;
        //            oAutorizacoes.UserFields.Fields.Item("U_P1_Comite").Value = autorizacoes.MembroComite;
        //            oAutorizacoes.UserFields.Fields.Item("U_P1_SearchPN").Value = autorizacoes.ConsultaPN;


        //            if (oAutorizacoes.Update() != 0)
        //            {
        //                msg = oCompany.GetLastErrorDescription();
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        msg = e.Message;
        //    }

        //    if (msg != null)
        //    {
        //        msg = oCompany.GetLastErrorDescription();
        //        Log.Gravar(msg, Log.TipoLog.Erro);
        //    }

        //    return msg;
        //}
        //#endregion

        //#region Atualizar Grupo de Usuário
        //public string AtualizarGrupoDoUsuario(string idUser, string idGrupo)
        //{
        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.UserTable oAccess = (SAPbobsCOM.UserTable)oCompany.UserTables.Item("P1_ACCESS");
        //    string msg = null;

        //    try
        //    {
        //        if (oAccess.GetByKey(idUser))
        //        {
        //            oAccess.UserFields.Fields.Item("U_P1_Grupo").Value = idGrupo;

        //            if (oAccess.Update() != 0)
        //            {
        //                msg = oCompany.GetLastErrorDescription();
        //            }
        //        }
        //        else
        //        {
        //            msg = "Usuário não encontrado na tabela de autorizações.";
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        msg = e.Message;
        //    }


        //    if (msg != null)
        //    {
        //        Log.Gravar(msg, Log.TipoLog.Erro);
        //    }

        //    return msg;
        //}
        //#endregion


        #region Pegar Grupo Autorizações de Usuário
        public AutorizacoesGrupo PegarGrupoAutorizacoes(Autorizacoes autorizacoes)
        {
            AutorizacoesGrupo autorizacao = new AutorizacoesGrupo();
            try
            {
                   StringBuilder SQL = new StringBuilder();

                    SQL.AppendLine("SELECT  \"Code\" AS \"UserId\",  \"Name\" AS \"UserName\" ,  \"U_P1_ReqS\" AS \"ReqEstoque\" ,  \"U_P1_NewReqS\" AS \"CriarReqEstoque\" ,\"U_P1_Type\" AS \"Tipo\" ," +
                       "\"U_P1_LSTATUS\" AS \"Licensa\" ,\"U_P1_Coorder\" AS \"Coordenador\" FROM \"@P1_ACCGRP\"");
                    SQL.AppendLine("WHERE \"Code\" = '" + autorizacoes.IdGrupo + "' FOR JSON PATH");

                    autorizacao = Conexao.ConsultaBanco<AutorizacoesGrupo[]>(SQL.ToString())[0];
            }
            catch (Exception e)
            {

                     autorizacao = JsonConvert.DeserializeObject<AutorizacoesGrupo>("{\"ReqEstoque\": 'R',\"CriarReqEstoque\": 'R',\"Coordenador\" : 'N'}");
            }
            return autorizacao;

        }
        #endregion

        //#region Pesquisar Autorizações do Cliente
        //public Autorizacoes PegarAutorizacoesPN(string code)
        //{
        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

        //    StringBuilder SQL = new StringBuilder();
        //    SQL.AppendLine("SELECT COALESCE(T0.\"U_P1_CodGrupo\",'') \"U_P1_CodGrupo\", COALESCE(T1.\"U_P1_RECR\",'N') \"U_P1_RECR\", COALESCE(T1.\"U_P1_ATTDADOS\",'N') \"U_P1_ATTDADOS\", COALESCE(T1.\"U_P1_CNTRVIS\",'N') \"U_P1_CNTRVIS\", COALESCE(T1.\"U_P1_EMAILSEND\",'N') \"U_P1_EMAILSEND\", COALESCE(T1.\"U_P1_FTRVIEW\",'N') \"U_P1_FTRVIEW\" FROM \"OCPR\" T0");
        //    SQL.AppendLine("INNER JOIN \"@P1_GRPCNTC\" T1 ON T1.\"Code\" = T0.\"U_P1_CodGrupo\" WHERE T0.\"CntctCode\" = '" + code + "'");

        //    Autorizacoes autorizacao = new Autorizacoes();

        //    oRecordSet.DoQuery(SQL.ToString());
        //    oRecordSet.MoveFirst();
        //    if (!oRecordSet.EoF)
        //    {
        //        autorizacao.RecebeEmail          = oRecordSet.Fields.Item("U_P1_RECR").Value.ToString();
        //        autorizacao.AttDados             = oRecordSet.Fields.Item("U_P1_ATTDADOS").Value.ToString();
        //        autorizacao.Questiona            = oRecordSet.Fields.Item("U_P1_EMAILSEND").Value.ToString();
        //        autorizacao.VisualizarFatura     = oRecordSet.Fields.Item("U_P1_FTRVIEW").Value.ToString();
        //        autorizacao.ExibeContrato        = oRecordSet.Fields.Item("U_P1_CNTRVIS").Value.ToString();
        //    }
        //    else
        //    {
        //        autorizacao.RecebeEmail          = "N";
        //        autorizacao.AttDados             = "N";
        //        autorizacao.Questiona            = "N";
        //        autorizacao.VisualizarFatura     = "N";
        //        autorizacao.ExibeContrato        = "N";
        //    }


        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
        //    oRecordSet = null;

        //    return autorizacao;
        //}
        //#endregion

        //#region Atualizar Definições de Bloqueio
        //public string SetLock(string StatusBloqueio, string IsHour, DateTime HourLock, int DaysOfLock)
        //{
        //    string msg = null;
        //    int iRetcode;
        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.UserTable oConfDoc = oCompany.UserTables.Item("P1_CONFDC");

        //    try
        //    {
        //        bool bExiste = false;
        //        bExiste = oConfDoc.GetByKey("1");

        //        oConfDoc.UserFields.Fields.Item("U_P1_STBLOCK").Value = String.IsNullOrEmpty(StatusBloqueio) ?"N" :StatusBloqueio;
        //        oConfDoc.UserFields.Fields.Item("U_P1_ISHOUR").Value = String.IsNullOrEmpty(IsHour) ?"N" : IsHour;
        //        oConfDoc.UserFields.Fields.Item("U_P1_HOURLOCK").Value = HourLock;
        //        oConfDoc.UserFields.Fields.Item("U_P1_DAYSLOCK").Value = DaysOfLock;

        //        if (bExiste)
        //        {
        //            iRetcode = oConfDoc.Update();
        //        }
        //        else
        //        {
        //            oConfDoc.Code = "1";
        //            oConfDoc.Name = "Financeiro";

        //            iRetcode = oConfDoc.Add();
        //        }

        //        System.Runtime.InteropServices.Marshal.ReleaseComObject(oConfDoc);
        //        oConfDoc = null;
        //    }
        //    catch (Exception e)
        //    {
        //        msg = e.Message;
        //    }

        //    if (msg != null)
        //    {
        //        Log.Gravar(msg, Log.TipoLog.Erro);
        //    }

        //    return msg;
        //}
        //#endregion

        //#region Pegar Definições de Bloqueio
        //public ConfDocumento PegarConfBloqueio()
        //{
        //    ConfDocumento configuracoesBloqueio = new ConfDocumento();

        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

        //    StringBuilder SQL = new StringBuilder();
        //    SQL.AppendLine("SELECT \"U_P1_STBLOCK\", \"U_P1_ISHOUR\", COALESCE(\"U_P1_DAYSLOCK\",'0') AS \"U_P1_DAYSLOCK\", \"U_P1_HOURLOCK\" FROM \"@P1_CONFDC\" ");
        //    SQL.AppendLine("WHERE \"Code\" = '1' ");

        //    oRecordset.DoQuery(SQL.ToString());

        //    oRecordset.MoveFirst();
        //    if (!oRecordset.EoF)
        //    {
        //        configuracoesBloqueio.StatusBloqueio     = oRecordset.Fields.Item("U_P1_STBLOCK").Value.ToString();
        //        configuracoesBloqueio.IsHourBloqueio     = oRecordset.Fields.Item("U_P1_ISHOUR").Value.ToString();
        //        configuracoesBloqueio.HorasBloqueio      = TrataHora.HorasViaString(oRecordset.Fields.Item("U_P1_HOURLOCK").Value.ToString());
        //        configuracoesBloqueio.BeforeDaysBloqueio = int.Parse(oRecordset.Fields.Item("U_P1_DAYSLOCK").Value.ToString());
        //    }

        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
        //    oRecordset = null;

        //    return configuracoesBloqueio;
        //}
        //#endregion


        //#region Atualizar Permissões de lançamento  em datas anteriores
        //public string AtualizarPermLancamento(string UserID, DateTime LimitDate, string YNLimitDate)
        //{
        //    string msg = null;

        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.UserTable oAutorizacoes = oCompany.UserTables.Item("P1_ACCESS");

        //    if (oAutorizacoes.GetByKey(UserID))
        //    {
        //        oAutorizacoes.UserFields.Fields.Item("U_P1_YNLIMITDT").Value = YNLimitDate;
        //        oAutorizacoes.UserFields.Fields.Item("U_P1_LIMITDATE").Value = LimitDate;

        //        if (oAutorizacoes.Update() != 0)
        //        {
        //            msg = oCompany.GetLastErrorDescription();
        //        }
        //    }
        //    else
        //    {
        //        msg = "Usuário não encontrado";
        //    }

        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oAutorizacoes);

        //    if (msg != null)
        //    {
        //        Log.Gravar(msg, Log.TipoLog.Erro);
        //    }

        //    return msg;
        //}
        //#endregion

        //#region Pegar Permiçãoo de lançamento por usuário
        //public Autorizacoes GetPermLancamentoByUser(string UserID)
        //{
        //    Autorizacoes permicao = new Autorizacoes();

        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

        //    StringBuilder SQL = new StringBuilder();
        //    SQL.AppendLine("SELECT \"U_P1_YNLIMITDT\", \"U_P1_LIMITDATE\" FROM \"@P1_ACCESS\"");
        //    SQL.AppendLine("WHERE \"Code\" = '" + UserID + "'");

        //    oRecordSet.DoQuery(SQL.ToString());
        //    oRecordSet.MoveFirst();
        //    if (!oRecordSet.EoF)
        //    {
        //        permicao.YNLimitDate = oRecordSet.Fields.Item("U_P1_YNLIMITDT").Value.ToString();
        //        permicao.LimitDate = DateTime.Parse(oRecordSet.Fields.Item("U_P1_LIMITDATE").Value.ToString());
        //    }

        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
        //    oRecordSet = null;


        //    return permicao;
        //}
        //#endregion
    }
}