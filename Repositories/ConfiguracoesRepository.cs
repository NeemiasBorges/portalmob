﻿using DataBase;
using Newtonsoft.Json;
using PortalMOB.Models;
using PortalMOB.Repositories.Interfaces;
using PowerOne.Database;
using PowerOne.Models;
using PowerOne.Util;
using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils;

namespace PowerOne.Repository
{
    public class ConfiguracoesRepository : IConfiguracoesRepository
    {
        #region Configurar Email
        //public string ConfigurarEmail(ConfiguracoesEmail configuracoesEmail)
        //{
        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.UserTable oConfig = oCompany.UserTables.Item("P1_EMAILCONF");

        //    string msg = null;
        //    int iRet = -1;
        //    bool bExiste = oConfig.GetByKey("1"); //verificando se já existe


        //    oConfig.UserFields.Fields.Item("U_P1_Email").Value = configuracoesEmail.Email;
        //    oConfig.UserFields.Fields.Item("U_P1_Senha").Value = configuracoesEmail.Senha;
        //    oConfig.UserFields.Fields.Item("U_P1_Servidor").Value = configuracoesEmail.Servidor;
        //    oConfig.UserFields.Fields.Item("U_P1_Porta").Value = configuracoesEmail.Porta;

        //    if (!bExiste)//se já existir
        //    {
        //        oConfig.Code = "1";
        //        oConfig.Name = "1";

        //        iRet = oConfig.Add();
        //    }
        //    else
        //    {
        //        iRet = oConfig.Update();
        //    }

        //    if (iRet != 0)
        //    {
        //        msg = oCompany.GetLastErrorDescription();
        //    }

        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oConfig);
        //    oConfig = null;

        //    if (msg != null)
        //    {
        //        Log.Gravar(msg, Log.TipoLog.Erro);
        //    }

        //    return msg;
        //}
        #endregion

        #region Configurar Anexo de Email
        //public string ConfiguraAnexo(ConfiguracoesEmail configuracoesAnexo)
        //{
        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.UserTable oConfig = oCompany.UserTables.Item("P1_EMAILCONF");

        //    string msg = null;
        //    int iRet = -1;
        //    bool bExiste = oConfig.GetByKey("1"); //verificando se já existe

        //    oConfig.UserFields.Fields.Item("U_P1_DrBoleto").Value = configuracoesAnexo.DirBoleto;
        //    oConfig.UserFields.Fields.Item("U_P1_DrNtFisc").Value = configuracoesAnexo.DirNotaFiscal;

        //    if (!bExiste)//se já existir
        //    {
        //        oConfig.Code = "1";
        //        oConfig.Name = "1";

        //        iRet = oConfig.Add();
        //    }
        //    else
        //    {
        //        iRet = oConfig.Update();
        //    }

        //    if (iRet != 0)
        //    {
        //        msg = oCompany.GetLastErrorDescription();
        //    }

        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oConfig);
        //    oConfig = null;

        //    if (msg != null)
        //    {
        //        Log.Gravar(msg, Log.TipoLog.Erro);
        //    }

        //    return msg;
        //}
        #endregion

        #region Pegar Configuracoes de Email
        public ConfiguracoesEmail GetConfiguracoesEmail()
        {
            //SAPbobsCOM.Company oCompany = Conexao.Company;
            //SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            ConfiguracoesEmail configEmail = null;

            //StringBuilder SQl = new StringBuilder();

            //SQl.AppendLine("SELECT * FROM \"@P1_EMAILCONF\"");

            //oRecordset.DoQuery(SQl.ToString());

            //oRecordset.MoveFirst();
            //if (!oRecordset.EoF)
            //{
            //    configEmail = new ConfiguracoesEmail(); //ele só não será null se existir configurações
            //    configEmail.Email = oRecordset.Fields.Item("U_P1_Email").Value.ToString();
            //    configEmail.Senha = oRecordset.Fields.Item("U_P1_Senha").Value.ToString();
            //    configEmail.Servidor = oRecordset.Fields.Item("U_P1_Servidor").Value.ToString();
            //    configEmail.Porta = int.Parse(oRecordset.Fields.Item("U_P1_Porta").Value.ToString());

            //    configEmail.DirBoleto = oRecordset.Fields.Item("U_P1_DrBoleto").Value.ToString();
            //    configEmail.DirNotaFiscal = oRecordset.Fields.Item("U_P1_DrNtFisc").Value.ToString();
            //}


            //System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            //oRecordset = null;

            return configEmail;
        }
        #endregion

        //#region Configurar Arquivos para Impressão
        //public string ConfigurarModeloImpressao(string code, string name, string path)
        //{
        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.UserTable oConfig = oCompany.UserTables.Item("P1_RPTDOC");

        //    string msg = null;
        //    int iRet = -1;

        //    bool bExiste = oConfig.GetByKey(code); //verificando se já existe

        //    oConfig.UserFields.Fields.Item("U_P1_Diretorio").Value = path;

        //    if (!bExiste)//se já existir
        //    {
        //        oConfig.Code = code;
        //        oConfig.Name = name;

        //        iRet = oConfig.Add();
        //    }
        //    else
        //    {
        //        iRet = oConfig.Update();
        //    }

        //    if (iRet != 0)
        //    {
        //        msg = oCompany.GetLastErrorDescription();
        //    }

        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oConfig);
        //    oConfig = null;

        //    if (msg != null)
        //    {
        //        Log.Gravar(msg, Log.TipoLog.Erro);
        //    }

        //    return msg;
        //}
        //#endregion

        //#region Pegar Diretorio de Modelo para Impressao
        //public string GetDiretorioModeloImpressao(string code)
        //{
        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

        //    string path = null;

        //    StringBuilder SQL = new StringBuilder();

        //    SQL.AppendLine("SELECT \"U_P1_Diretorio\" FROM \"@P1_RPTDOC\"");
        //    SQL.AppendLine("WHERE \"Code\" = '" + code + "'");

        //    oRecordset.DoQuery(SQL.ToString());

        //    if (!oRecordset.EoF)
        //    {
        //        path = oRecordset.Fields.Item("U_P1_Diretorio").Value.ToString();
        //    }

        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
        //    oRecordset = null;

        //    return path;
        //}
        //#endregion

        #region Configurações de Tema
        public Resultado ConfigurarTema(string code, string cor, string icone)
        {
            //SAPbobsCOM.UserTable oConfig = oCompany.UserTables.Item("P1_TEMA");
            Resultado resultado = new Resultado();
            string resul = "";
            string jsonEnvio = "{\"Code\" : " + code + "";

            if (!String.IsNullOrEmpty(icone))
            {
                jsonEnvio +=  ",\"U_P1_Icones\" : \"" + icone + "\"";
            }
            if (!String.IsNullOrEmpty(cor))
            {
                jsonEnvio += ",\"U_P1_Color\":   \"" + cor + "\"";
            }
            jsonEnvio += "} ";

            try
            {
                resul = RequestUtils.Request(ConstantUtils.URL_SERVICE_LAYER + "U_P1_TEMA('" + code + "')", RequestUtils.PATCH, DAL.LoginDAL.sessionId, jsonEnvio, false);
            }
            catch (Exception e)
            {
                resultado.Msg = e.Message;
                resultado.Tipo= "error";
                Log.Gravar(resultado.Msg, Log.TipoLog.Erro);
            }

            //if (resultado.Msg == null)
            //{
            //    resultado.Msg = resul;
            //    resultado.Tipo = "success";
            //}

            return resultado;
        }
        #endregion

        #region Pegar Configuracoes de Tema
        public Tema GetTema(string code)
        {

            Tema configTema = null;
            try
            {
                StringBuilder SQL = new StringBuilder();
                SQL.AppendLine("SELECT \"Code\", \"Name\", COALESCE(\"U_P1_Color\",'RED') AS \"Color\", COALESCE(\"U_P1_Icones\",'PRO') AS \"Icon\" FROM \"@P1_TEMA\"");
                SQL.AppendLine("WHERE \"Code\" = '" + code + "' FOR JSON PATH");

                configTema = Conexao.ConsultaBanco<Tema[]>(SQL.ToString())[0];
            }
            catch (Exception)
            {
                configTema = JsonConvert.DeserializeObject<Tema>("{\"Color\" : 'RED', \"Icon\": 'PRO'}");
            }

            return configTema;

        }
        #endregion

        //#region Configurar Centros de Custo
        //public string ConfigurarCentroCusto(int[] dimencoes)
        //{
        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.CompanyService oCompanyService = oCompany.GetCompanyService();

        //    SAPbobsCOM.DimensionsService oDIMService =
        //        (SAPbobsCOM.DimensionsService)
        //        oCompanyService.GetBusinessService(SAPbobsCOM.ServiceTypes.DimensionsService);

        //    string msg = null;
        //    try
        //    {
        //        for (int i = 1; i <= 5; i++)
        //        {
        //            SAPbobsCOM.DimensionParams oDIMParams =
        //                (SAPbobsCOM.DimensionParams)
        //                oDIMService.GetDataInterface
        //                (SAPbobsCOM.DimensionsServiceDataInterfaces.dsDimensionParams);

        //            oDIMParams.DimensionCode = i;
        //            SAPbobsCOM.Dimension oDIM = oDIMService.GetDimension(oDIMParams);
        //            if (dimencoes.Contains(i))
        //            {
        //                oDIM.UserFields.Item("U_P1_Ativado").Value = "Y";
        //            }
        //            else
        //            {
        //                oDIM.UserFields.Item("U_P1_Ativado").Value = "N";
        //            }
        //            oDIMService.UpdateDimension(oDIM);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        msg = e.Message;
        //    }

        //    if (msg != null)
        //    {
        //        Log.Gravar(msg, Log.TipoLog.Erro);
        //    }

        //    return msg;
        //}
        //#endregion

        //#region Pegar Configurações de Centro de Custo
        //public int[] PegarConfiguracoesCentroCusto()
        //{
        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
        //    int[] ctCusto = new int[5];

        //    StringBuilder SQL = new StringBuilder();
        //    SQL.AppendLine("SELECT T0.\"DimCode\" FROM ODIM T0");
        //    SQL.AppendLine("WHERE T0.\"U_P1_Ativado\" = 'Y'");

        //    oRecordset.DoQuery(SQL.ToString());

        //    oRecordset.MoveFirst();
        //    int i = 0;
        //    while (!oRecordset.EoF)
        //    {
        //        ctCusto[i] = int.Parse(oRecordset.Fields.Item("DimCode").Value.ToString());
        //        oRecordset.MoveNext();
        //        i++;
        //    }

        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
        //    oRecordset = null;

        //    return ctCusto;
        //}
        //#endregion

        //#region Configurar Visibilidade Projeto
        //public string ConfigurarDocMkt(string VerProjeto, string GrpItm, string UndMedida)
        //{
        //    int iRetcode;
        //    string msg = null;

        //    try
        //    {
        //        SAPbobsCOM.Company oCompany;
        //        oCompany = Conexao.Company;
        //        SAPbobsCOM.UserTable oAccess = oCompany.UserTables.Item("P1_CONFDC");
        //        if (oAccess.GetByKey("1"))
        //        {
        //            if (!String.IsNullOrEmpty(VerProjeto))
        //            {
        //                oAccess.UserFields.Fields.Item("U_P1_PROJ").Value = VerProjeto.ToString();
        //            }
        //            else
        //            {
        //                oAccess.UserFields.Fields.Item("U_P1_PROJ").Value = "N";
        //            }
        //            if (!String.IsNullOrEmpty(GrpItm))
        //            {
        //                oAccess.UserFields.Fields.Item("U_P1_SHOWITM").Value = GrpItm.ToString();
        //            }
        //            else
        //            {
        //                oAccess.UserFields.Fields.Item("U_P1_SHOWITM").Value = "N";
        //            }
        //            if (!String.IsNullOrEmpty(UndMedida))
        //            {
        //                oAccess.UserFields.Fields.Item("U_P1_VISUND").Value = UndMedida.ToString();
        //            }
        //            else
        //            {
        //                oAccess.UserFields.Fields.Item("U_P1_VISUND").Value = "N";
        //            }
        //            iRetcode = oAccess.Update();
        //        }
        //        else
        //        {
        //            oAccess.Code = "1";
        //            oAccess.Name = "ConfDoc";
        //            if (!String.IsNullOrEmpty(VerProjeto))
        //            {
        //                oAccess.UserFields.Fields.Item("U_P1_PROJ").Value = VerProjeto.ToString();
        //            }
        //            else
        //            {
        //                oAccess.UserFields.Fields.Item("U_P1_PROJ").Value = "N";
        //            }
        //            if (!String.IsNullOrEmpty(GrpItm))
        //            {
        //                oAccess.UserFields.Fields.Item("U_P1_SHOWITM").Value = GrpItm.ToString();
        //            }
        //            else
        //            {
        //                oAccess.UserFields.Fields.Item("U_P1_SHOWITM").Value = "N";
        //            }
        //            if (!String.IsNullOrEmpty(UndMedida))
        //            {
        //                oAccess.UserFields.Fields.Item("U_P1_VISUND").Value = UndMedida.ToString();
        //            }
        //            else
        //            {
        //                oAccess.UserFields.Fields.Item("U_P1_VISUND").Value = "N";
        //            }
        //            iRetcode = oAccess.Add();
        //        }

        //        if (iRetcode != 0)
        //        {
        //            msg = oCompany.GetLastErrorDescription();
        //            Log.Gravar(msg, Log.TipoLog.Erro);
        //        }

        //        System.Runtime.InteropServices.Marshal.ReleaseComObject(oAccess);
        //        oAccess = null;

        //        if (msg != null)
        //        {
        //            Log.Gravar(msg, Log.TipoLog.Erro);
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        msg = e.Message;
        //        Log.Gravar(msg, Log.TipoLog.Erro);
        //    }

        //    return msg;
        //}
        //#endregion

        //#region CONFIGURAÇÕES DO DOCUMENTO
        //public string ConfFinanceiro(string Lancamento, string Prestacao, string Reembolso, ConfDocumento opcoes)
        //{
        //    int iRetcode;
        //    string msg = null;
        //    try
        //    {
        //        SAPbobsCOM.Company oCompany;
        //        oCompany = Conexao.Company;
        //        SAPbobsCOM.UserTable oAccess = oCompany.UserTables.Item("P1_CONFDC");
        //        if (oAccess.GetByKey("1"))
        //        {
        //            if (!String.IsNullOrEmpty(Reembolso))
        //            {
        //                oAccess.UserFields.Fields.Item("U_P1_CONFREEM").Value = Reembolso.ToString();
        //            }
        //            if (!String.IsNullOrEmpty(Lancamento))
        //            {
        //                oAccess.UserFields.Fields.Item("U_P1_CONFLD").Value = Lancamento.ToString();
        //            }
        //            if (!String.IsNullOrEmpty(Prestacao))
        //            {
        //                oAccess.UserFields.Fields.Item("U_P1_CONFPC").Value = Prestacao.ToString();
        //            }
        //            if (!String.IsNullOrEmpty(opcoes.CardcodeVisivel))
        //            {
        //                oAccess.UserFields.Fields.Item("U_P1_CodCliente").Value = opcoes.CardcodeVisivel.ToString();
        //            }
        //            else
        //            { oAccess.UserFields.Fields.Item("U_P1_CodCliente").Value = "N"; }
        //            if (!String.IsNullOrEmpty(opcoes.CamposProcesso))
        //            {
        //                oAccess.UserFields.Fields.Item("U_P1_Processo").Value = opcoes.CamposProcesso.ToString();
        //            }
        //            else
        //            { oAccess.UserFields.Fields.Item("U_P1_Processo").Value = "N"; }
        //            if (!String.IsNullOrEmpty(opcoes.ViSolicitante))
        //            {
        //                oAccess.UserFields.Fields.Item("U_P1_ViSolicitante").Value = opcoes.ViSolicitante.ToString();
        //            }
        //            else { oAccess.UserFields.Fields.Item("U_P1_ViSolicitante").Value = "N"; }
        //            if (!String.IsNullOrEmpty(opcoes.VisStatus))
        //            {
        //                oAccess.UserFields.Fields.Item("U_P1_VisStatus").Value = opcoes.VisStatus.ToString();
        //            }
        //            else { oAccess.UserFields.Fields.Item("U_P1_VisStatus").Value = "N"; }
        //            if (!String.IsNullOrEmpty(opcoes.VisMovimento))
        //            {
        //                oAccess.UserFields.Fields.Item("U_P1_VisMovimento").Value = opcoes.VisMovimento.ToString();
        //            }
        //            else { oAccess.UserFields.Fields.Item("U_P1_VisMovimento").Value = "N"; }
        //            iRetcode = oAccess.Update();
        //        }
        //        else
        //        {
        //            oAccess.Code = "1";
        //            oAccess.Name = "Financeiro";
        //            if (!String.IsNullOrEmpty(Reembolso))
        //            {
        //                oAccess.UserFields.Fields.Item("u_P1_CONFREEM").Value = Reembolso.ToString();
        //            }
        //            if (!String.IsNullOrEmpty(Lancamento))
        //            {
        //                oAccess.UserFields.Fields.Item("U_P1_CONFLD").Value = Lancamento.ToString();
        //            }
        //            if (!String.IsNullOrEmpty(opcoes.CamposProcesso))
        //            {
        //                oAccess.UserFields.Fields.Item("U_P1_Processo").Value = opcoes.CamposProcesso.ToString();
        //            }
        //            if (!String.IsNullOrEmpty(Prestacao))
        //            {
        //                oAccess.UserFields.Fields.Item("U_P1_CONFPC").Value = Prestacao.ToString();
        //            }
        //            if (!String.IsNullOrEmpty(opcoes.CardcodeVisivel))
        //            {
        //                oAccess.UserFields.Fields.Item("U_P1_CodCliente").Value = opcoes.CardcodeVisivel.ToString();
        //            }
        //            if (!String.IsNullOrEmpty(opcoes.VisStatus))
        //            {
        //                oAccess.UserFields.Fields.Item("U_P1_ViSolicitante").Value = opcoes.ViSolicitante.ToString();
        //            }
        //            if (!String.IsNullOrEmpty(opcoes.ViSolicitante))
        //            {
        //                oAccess.UserFields.Fields.Item("U_P1_VisStatus").Value = opcoes.VisStatus.ToString();
        //            }
        //            if (!String.IsNullOrEmpty(opcoes.VisMovimento))
        //            {
        //                oAccess.UserFields.Fields.Item("U_P1_VisMovimento").Value = opcoes.VisMovimento.ToString();
        //            }
        //            iRetcode = oAccess.Add();
        //        }


        //        if (iRetcode != 0)
        //        {
        //            msg = oCompany.GetLastErrorDescription();
        //            Log.Gravar(msg, Log.TipoLog.Erro);
        //        }

        //        System.Runtime.InteropServices.Marshal.ReleaseComObject(oAccess);
        //        oAccess = null;

        //        if (msg != null)
        //        {
        //            Log.Gravar(msg, Log.TipoLog.Erro);
        //        }

        //    }
        //    catch { }
        //    return msg;
        //}

        //#endregion


    }
}
