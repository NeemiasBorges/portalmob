﻿using System;
using System.Collections.Generic;
using Models.Estoque;
using System.Globalization;
using System.Text;

using DataBase;
using PowerOne.Util;
using static PowerOne.Util.Log;
using PortalMOB.Repositories.Interfaces;
using PowerOne.Models;
using Utils;
using DAL;

//using CrystalDecisions.CrystalReports.Engine;
//using CrystalDecisions.Shared;

namespace PortalMOB.Repositories
{
    public class EstoqueRepository : IEstoqueRepository
    {
        //        #region Serviços
        //        //private ItemEstoqueService _itemEstoqueService = new ItemEstoqueService();
        //        //EnvioEmail envio = new EnvioEmail();
        //        //DepositoService _depositoService = new DepositoService();
        //        //AprovacaoService _aprovacaoService = new AprovacaoService();
        //        #endregion

        #region Contagem de documentos por semana
        public int contEstoque()
        {
            int ContagemEstoque = 0;

            DateTime inicio = DateTime.Now;
            DateTime fim = inicio.AddDays(-7);

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT count(\"DocEntry\") as Count FROM  \"@P1_OIGE\" ");
            SQL.AppendLine("WHERE \"CreateDate\" BETWEEN '" + fim.ToString("yyyyMMdd") + "' AND '" + inicio.ToString("yyyyMMdd") + "' ");

            try
            {
                ContagemEstoque = Conexao.ConsultaBanco<int[]>(SQL.ToString())[0];
            }
            catch (Exception e)
            {
                Log.Gravar(e.Message, TipoLog.Erro);
            }

            return ContagemEstoque;
        }

        #endregion

        #region Pesquisar Requerimentos e Listar
        public List<RequerimentoEstoque> List(Login login, string dtInicio, string dtFim, string status)
        {
            List<RequerimentoEstoque> requerimentos = new List<RequerimentoEstoque>();

            string url = $"{ConstantUtils.URL_SERVICE_LAYER}P1_OIGE?$orderby=DocEntry desc&$top=50";

            StringBuilder SQL = new StringBuilder();

            SQL.AppendLine("SELECT TOP 50 T0.\"DocEntry\", T0.\"DocNum\", T0.\"U_P1_IDFILIAL\", ");
            SQL.AppendLine("CASE WHEN COALESCE(T0.\"U_P1_STATUS\",'O') = 'O'  THEN 'Aberta' WHEN COALESCE(T0.\"U_P1_STATUS\",'O') = 'A' THEN 'Aprovada' ELSE 'Cancelada' END \"U_P1_STATUS\", ");
            SQL.AppendLine("T0.\"U_P1_DTLANC\", ");
            SQL.AppendLine("CONCAT(CONCAT(H.\"firstName\", '.'), H.\"lastName\") as \"U_P1_Owner\" ");
            SQL.AppendLine("FROM \"@P1_OIGE\" T0 INNER JOIN OHEM H ");
            SQL.AppendLine("ON T0.\"U_P1_Owner\" = H.\"empID\" ");

            if (login.Autorizacoes.ReqEstoque != "T" && login.Autorizacoes.Grupo.ReqEstoque != "T")
                url += $"$filter=U_P1_Owner eq '{login.Id}'";

            #region Filtros

            if (!String.IsNullOrEmpty(dtInicio))
                url += $" and U_P1_DTLANC gte '{DateTime.ParseExact(dtInicio, "yyyy-MM-dd", CultureInfo.InvariantCulture):yyyyMMdd}'";

            if (!String.IsNullOrEmpty(dtFim))
                url += $" and U_P1_DTLANC gte '{DateTime.ParseExact(dtInicio, "yyyy-MM-dd", CultureInfo.InvariantCulture):yyyyMMdd}'";

            if (status != "T")
                url += $" and U_P1_STATUS eq 'O'";        

            #endregion

            SQL.AppendLine("ORDER BY T0.\"DocEntry\" DESC ");
            SQL.AppendLine("FOR JSON PATH ");

            try
            {
                string response = RequestUtils.Request(url, RequestUtils.GET, LoginDAL.sessionId, "", false);

                //requerimentos = Conexao.ConsultaBanco<List<RequerimentoEstoque>>(SQL.ToString());
            }
            catch (Exception e)
            {
                Log.Gravar(e.Message, TipoLog.Erro);
            }

            return requerimentos;
        }
        #endregion

        #region Pegar Requerimento Específico
        public RequerimentoEstoque GetByKey(int DocEntry)
        {
            RequerimentoEstoque requerimento = new RequerimentoEstoque();

            StringBuilder SQL = new StringBuilder();

            SQL.AppendLine("SELECT T0.\"DocEntry\",H.\"email\", T0.\"DocNum\", T0.\"U_P1_IDFILIAL\", T0.\"U_P1_NOMEFILIAL\", ");
            SQL.AppendLine("CASE WHEN COALESCE(T0.\"U_P1_STATUS\",'O') = 'O'  THEN 'Aberta' WHEN COALESCE(T0.\"U_P1_STATUS\",'O') = 'A' THEN 'Aprovada' ELSE 'Cancelada' END \"DocStatus\",");
            SQL.AppendLine("T0.\"U_P1_DTLANC\", ");
            SQL.AppendLine("CONCAT(CONCAT(H.\"firstName\", '.'), H.\"lastName\") as \"Requisitante\" ");
            SQL.AppendLine("FROM \"@P1_OIGE\" T0 INNER JOIN OHEM H ");
            SQL.AppendLine("ON T0.\"U_P1_Owner\" = H.\"empID\" ");
            SQL.AppendLine($"WHERE T0.\"DocEntry\" = {DocEntry} ");

            //if (!oRecordSet.EoF)
            //{
            //    requerimentoEstoque.DocEntry = int.Parse(oRecordSet.Fields.Item("DocEntry").Value.ToString());
            //    requerimentoEstoque.DocNum = int.Parse(oRecordSet.Fields.Item("DocNum").Value.ToString());
            //    requerimentoEstoque.DataLancamento = DateTime.Parse(oRecordSet.Fields.Item("U_P1_DTLANC").Value.ToString());
            //    requerimentoEstoque.Requisitante = oRecordSet.Fields.Item("Requisitante").Value.ToString();
            //    requerimentoEstoque.Status = oRecordSet.Fields.Item("DocStatus").Value.ToString();
            //    requerimentoEstoque.Bp_ID = int.Parse(oRecordSet.Fields.Item("U_P1_IDFILIAL").Value.ToString());
            //    requerimentoEstoque.Bp_Name = oRecordSet.Fields.Item("U_P1_NOMEFILIAL").Value.ToString();
            //    requerimentoEstoque.Email = oRecordSet.Fields.Item("email").Value.ToString();
            //}

            try
            {
                requerimento = Conexao.ConsultaBanco<RequerimentoEstoque>(SQL.ToString());
            }
            catch (Exception e)
            {
                Log.Gravar(e.Message, TipoLog.Erro);
            }

            //requerimentoEstoque.Itens = ListarLinhas(DocEntry, null);

            return requerimento;
        }
        #endregion

        //        #region Create
        //        public string Create(RequerimentoEstoque requerimento, Login user)
        //        {
        //            Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-br", true);
        //            SAPbobsCOM.Company oCompany = Conexao.Company;

        //            string msg = null;

        //            try
        //            {
        //                SAPbobsCOM.GeneralService oGeneralService;
        //                SAPbobsCOM.GeneralDataParams teste;
        //                SAPbobsCOM.GeneralData oGeneralData;
        //                SAPbobsCOM.GeneralDataCollection oSons;
        //                SAPbobsCOM.GeneralData oSon;
        //                SAPbobsCOM.CompanyService sCmp;

        //                sCmp = oCompany.GetCompanyService();

        //                // Pegando o Servico que vai tratar o UDO
        //                oGeneralService = sCmp.GetGeneralService("P1_OIGE");

        //                // Inserindo os dados do UDO
        //                oGeneralData = (SAPbobsCOM.GeneralData)oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData);
        //                oGeneralData.SetProperty("U_P1_STATUS", "O");
        //                oGeneralData.SetProperty("U_P1_DTLANC", DateTime.Now);
        //                oGeneralData.SetProperty("U_P1_Owner", user.Id.ToString());
        //                oGeneralData.SetProperty("U_P1_IDFILIAL", requerimento.Bp_ID);
        //                oGeneralData.SetProperty("U_P1_NOMEFILIAL", requerimento.Bp_Name);
        //                if (!String.IsNullOrEmpty(requerimento.Observacao))
        //                {
        //                    oGeneralData.SetProperty("U_P1_Comments", requerimento.Observacao);
        //                }

        //                oSons = oGeneralData.Child("P1_IGE1");

        //                foreach (var item in requerimento.Itens)
        //                {
        //                    oSon = oSons.Add();

        //                    oSon.SetProperty("U_P1_ItemCode", item.CodigoItem);
        //                    oSon.SetProperty("U_P1_Quantity", item.Quantidade);
        //                    if (!String.IsNullOrEmpty(item.Observacao))
        //                    {
        //                        oSon.SetProperty("U_P1_Comments", item.Observacao);
        //                    }
        //                    if (String.IsNullOrEmpty(item.CodigoDeposito))
        //                    {
        //                        oSon.SetProperty("U_P1_WhsCode", _depositoService.WhsDefault(requerimento.Bp_ID));
        //                    }
        //                    else
        //                    {
        //                        oSon.SetProperty("U_P1_WhsCode", item.CodigoDeposito);
        //                    }
        //                    oSon.SetProperty("U_P1_LinStatus", "O");

        //                    if (!String.IsNullOrEmpty(item.OcrCode))
        //                    {
        //                        oSon.SetProperty("U_P1_CC01", item.OcrCode);
        //                    }
        //                    if (!String.IsNullOrEmpty(item.OcrCode2))
        //                    {
        //                        oSon.SetProperty("U_P1_CC02", item.OcrCode2);
        //                    }
        //                    if (!String.IsNullOrEmpty(item.OcrCode3))
        //                    {
        //                        oSon.SetProperty("U_P1_CC03", item.OcrCode3);
        //                    }
        //                    if (!String.IsNullOrEmpty(item.OcrCode4))
        //                    {
        //                        oSon.SetProperty("U_P1_CC04", item.OcrCode4);
        //                    }
        //                    if (!String.IsNullOrEmpty(item.OcrCode5))
        //                    {
        //                        oSon.SetProperty("U_P1_CC05", item.OcrCode5);
        //                    }

        //                    if (!String.IsNullOrEmpty(item.Projeto))
        //                    {
        //                        oSon.SetProperty("U_P1_PROJ", item.Projeto);
        //                    }

        //                }

        //                teste = oGeneralService.Add(oGeneralData);
        //            }
        //            catch (Exception ex)
        //            {
        //                msg = ex.Message;
        //            }

        //            if (msg != null)
        //            {
        //                Log.Gravar(msg, Log.TipoLog.Erro);
        //            }
        //            else
        //            {
        //                NotificaResp(user, requerimento.Itens);
        //            }

        //            return msg;
        //        }

        //        #endregion

        //        #region Listar Linhas do Requerimento
        //        public List<RequerimentoEstoqueLinhas> ListarLinhas(int DocEntry, string[] linhas)
        //        {
        //            SAPbobsCOM.Company oCompany = Conexao.Company;
        //            SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

        //            List<RequerimentoEstoqueLinhas> requerimentosLinhas = new List<RequerimentoEstoqueLinhas>();

        //            StringBuilder SQL = new StringBuilder();

        //            SQL.AppendLine("SELECT T0.\"DocEntry\", T0.\"DocNum\",");
        //            SQL.AppendLine("case when T0.\"U_P1_STATUS\" = 'O' then 'Aberta' else 'Fechada' end \"DocStatus\", G0.\"ItmsGrpNam\", ");
        //            SQL.AppendLine("T0.\"U_P1_DTLANC\", T1.\"LineId\", T1.\"U_P1_ItemCode\",i.\"ItemName\", T1.\"U_P1_Quantity\", o.\"WhsCode\",o.\"WhsName\",");
        //            SQL.AppendLine("case when T1.\"U_P1_LinStatus\" = 'O' then 'Aberta'");
        //            SQL.AppendLine("when T1.\"U_P1_LinStatus\" = 'C'");
        //            SQL.AppendLine("then 'Cancelada' else 'Fechada' end \"LinStatus\",");
        //            SQL.AppendLine("i.\"InvntryUom\",");
        //            SQL.AppendLine("T1.\"U_P1_PROJ\",T1.\"U_P1_CC01\", T1.\"U_P1_CC02\", T1.\"U_P1_CC03\", T1.\"U_P1_CC04\", T1.\"U_P1_CC05\",T1.\"U_P1_PROJ\",");
        //            SQL.AppendLine("C1.\"OcrName\", C2.\"OcrName\" \"OcrName2\", C3.\"OcrName\" \"OcrName3\", C4.\"OcrName\" \"OcrName4\", C5.\"OcrName\" \"OcrName5\"");
        //            SQL.AppendLine(" FROM \"@P1_OIGE\"  T0 inner join \"@P1_IGE1\"  T1 on T0.\"DocEntry\" = T1.\"DocEntry\"");
        //            SQL.AppendLine("left join OWHS o on t1.\"U_P1_WhsCode\" = o.\"WhsCode\"");
        //            SQL.AppendLine("inner join oitm i on t1.\"U_P1_ItemCode\" = i.\"ItemCode\"");
        //            SQL.AppendLine("inner join OITB G0 on i.\"ItmsGrpCod\" = G0.\"ItmsGrpCod\" ");
        //            SQL.AppendLine("left join OOCR C1 on T1.\"U_P1_CC01\" = C1.\"OcrCode\"");
        //            SQL.AppendLine("left join OOCR C2 on T1.\"U_P1_CC02\" = C2.\"OcrCode\"");
        //            SQL.AppendLine("left join OOCR C3 on T1.\"U_P1_CC03\" = C3.\"OcrCode\"");
        //            SQL.AppendLine("left join OOCR C4 on T1.\"U_P1_CC04\" = C4.\"OcrCode\"");
        //            SQL.AppendLine("left join OOCR C5 on T1.\"U_P1_CC05\" = C5.\"OcrCode\"");
        //            SQL.AppendLine("WHERE T0.\"DocEntry\" = '" + DocEntry + "'");

        //            if (linhas != null)
        //            {
        //                SQL.AppendLine("AND T1.\"LineId\" in (" + string.Join(",", linhas) + ")");
        //            }

        //            oRecordSet.DoQuery(SQL.ToString());

        //            oRecordSet.MoveFirst();
        //            while (!oRecordSet.EoF)
        //            {
        //                RequerimentoEstoqueLinhas linha = new RequerimentoEstoqueLinhas();
        //                linha.DocEntry = int.Parse(oRecordSet.Fields.Item("DocEntry").Value.ToString());
        //                linha.DocNum = int.Parse(oRecordSet.Fields.Item("DocNum").Value.ToString());
        //                linha.DocStatus = oRecordSet.Fields.Item("DocStatus").Value.ToString();
        //                linha.DataRequisicao = DateTime.Parse(oRecordSet.Fields.Item("U_P1_DTLANC").Value.ToString());
        //                linha.IdLinha = int.Parse(oRecordSet.Fields.Item("LineId").Value.ToString());
        //                linha.CodigoItem = oRecordSet.Fields.Item("U_P1_ItemCode").Value.ToString();
        //                linha.DecricaoItem = oRecordSet.Fields.Item("ItemName").Value.ToString();
        //                linha.GroupItem = oRecordSet.Fields.Item("ItmsGrpNam").Value.ToString();
        //                //linha.BatchNumber = oRecordSet.Fields.Item("BatchNumber").Value.ToString();

        //                if (String.IsNullOrEmpty(oRecordSet.Fields.Item("U_P1_Quantity").Value.ToString().Trim()))
        //                {
        //                    linha.Quantidade = 0;
        //                }
        //                else
        //                {
        //                    linha.Quantidade = double.Parse(oRecordSet.Fields.Item("U_P1_Quantity").Value.ToString().Trim());
        //                }
        //                linha.StatusLinha = oRecordSet.Fields.Item("LinStatus").Value.ToString();
        //                linha.UndMedida = oRecordSet.Fields.Item("InvntryUom").Value.ToString();
        //                linha.CodigoDeposito = oRecordSet.Fields.Item("WhsCode").Value.ToString();
        //                linha.DescricaoDeposito = oRecordSet.Fields.Item("WhsName").Value.ToString();

        //                linha.Projeto = oRecordSet.Fields.Item("U_P1_PROJ").Value.ToString();

        //                linha.OcrCode = oRecordSet.Fields.Item("U_P1_CC01").Value.ToString();
        //                linha.OcrName = oRecordSet.Fields.Item("OcrName").Value.ToString();
        //                linha.OcrCode2 = oRecordSet.Fields.Item("U_P1_CC02").Value.ToString();
        //                linha.OcrName2 = oRecordSet.Fields.Item("OcrName2").Value.ToString();
        //                linha.OcrCode3 = oRecordSet.Fields.Item("U_P1_CC03").Value.ToString();
        //                linha.OcrName3 = oRecordSet.Fields.Item("OcrName3").Value.ToString();
        //                linha.OcrCode4 = oRecordSet.Fields.Item("U_P1_CC04").Value.ToString();
        //                linha.OcrName4 = oRecordSet.Fields.Item("OcrName4").Value.ToString();
        //                linha.OcrCode5 = oRecordSet.Fields.Item("U_P1_CC05").Value.ToString();
        //                linha.OcrName5 = oRecordSet.Fields.Item("OcrName5").Value.ToString();

        //                linha.GetQtdDisp = _itemEstoqueService.GetQtdDisponivelByItem(linha.CodigoItem, linha.CodigoDeposito);

        //                requerimentosLinhas.Add(linha);
        //                oRecordSet.MoveNext();
        //            }

        //            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
        //            oRecordSet = null;

        //            return requerimentosLinhas;
        //        }
        //        #endregion

        //        #region Alterar Linha
        //        public string AlterarLinha(RequerimentoEstoqueLinhas requerimentoLinha)
        //        {
        //            string msg = null;

        //            SAPbobsCOM.Company oCompany = Conexao.Company;

        //            try
        //            {
        //                SAPbobsCOM.GeneralService oGeneralService;
        //                SAPbobsCOM.GeneralData oGeneralData;
        //                SAPbobsCOM.GeneralDataParams oGeneralParams;
        //                SAPbobsCOM.GeneralData oLinha;
        //                SAPbobsCOM.GeneralDataCollection oLinhas;
        //                SAPbobsCOM.CompanyService sCmp;

        //                sCmp = oCompany.GetCompanyService();

        //                // Pegando o Servico que vai tratar o UDO
        //                oGeneralService = sCmp.GetGeneralService("P1_OIGE");

        //                oGeneralParams = ((SAPbobsCOM.GeneralDataParams)(oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralDataParams)));

        //                oGeneralParams.SetProperty("DocEntry", requerimentoLinha.DocEntry);

        //                oGeneralData = oGeneralService.GetByParams(oGeneralParams);

        //                // seleciona a tabela que vou usar do UDO 
        //                oLinhas = oGeneralData.Child("P1_IGE1");

        //                // pega a linha a editar
        //                oLinha = oLinhas.Item(requerimentoLinha.IdLinha - 1);

        //                // definindo o novo valor
        //                oLinha.SetProperty("U_P1_Quantity", requerimentoLinha.Quantidade);
        //                oLinha.SetProperty("U_P1_WhsCode", requerimentoLinha.CodigoDeposito);

        //                if (!String.IsNullOrEmpty(requerimentoLinha.OcrCode))
        //                {
        //                    oLinha.SetProperty("U_P1_CC01", requerimentoLinha.OcrCode);
        //                }

        //                if (!String.IsNullOrEmpty(requerimentoLinha.OcrCode2))
        //                {
        //                    oLinha.SetProperty("U_P1_CC02", requerimentoLinha.OcrCode2);
        //                }

        //                if (!String.IsNullOrEmpty(requerimentoLinha.OcrCode3))
        //                {
        //                    oLinha.SetProperty("U_P1_CC03", requerimentoLinha.OcrCode3);
        //                }

        //                if (!String.IsNullOrEmpty(requerimentoLinha.OcrCode4))
        //                {
        //                    oLinha.SetProperty("U_P1_CC04", requerimentoLinha.OcrCode4);
        //                }

        //                if (!String.IsNullOrEmpty(requerimentoLinha.OcrCode5))
        //                {
        //                    oLinha.SetProperty("U_P1_CC05", requerimentoLinha.OcrCode5);
        //                }

        //                if (!String.IsNullOrEmpty(requerimentoLinha.Projeto))
        //                {
        //                    oLinha.SetProperty("U_P1_PROJ", requerimentoLinha.Projeto);
        //                }

        //                oGeneralService.Update(oGeneralData);
        //            }
        //            catch (Exception e)
        //            {
        //                msg = e.Message;
        //            }

        //            if (msg != null)
        //            {
        //                Log.Gravar(msg, Log.TipoLog.Erro);
        //            }

        //            return msg;
        //        }
        //        #endregion

        //        #region Aprovar Requerimento de Estoque(Gerar Saída de Estoque)
        //        public string AprovarRequerimento(Login login, string[] linesChecked, string observacoes, int DocEntry, int Bp_ID, List<RequerimentoEstoqueLinhas> AdmLoteList, bool tipo)
        //        {
        //            SAPbobsCOM.Company oCompany = Conexao.Company;
        //            string msgErro = null;

        //            if (oCompany.InTransaction)
        //            {
        //                oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
        //            }

        //            oCompany.StartTransaction();
        //            List<RequerimentoEstoqueLinhas> linhas;
        //            try
        //            {
        //                if (tipo)
        //                {
        //                    linhas = AdmLoteList;
        //                    msgErro = InserirSaidaMercadoria(login, observacoes, DocEntry, linhas, Bp_ID, true);
        //                }
        //                else
        //                {
        //                    //Log.Gravar("ListarLinhas: " + msgErro, Log.TipoLog.Erro);
        //                    linhas = ListarLinhas(DocEntry, linesChecked);
        //                    msgErro = InserirSaidaMercadoria(login, observacoes, DocEntry, linhas, Bp_ID, false);
        //                }

        //                if (msgErro != null)
        //                {
        //                    //Log.Gravar("EndTransaction: " + msgErro, Log.TipoLog.Erro);
        //                    if (oCompany.InTransaction)
        //                    {
        //                        oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
        //                    }
        //                }
        //                else
        //                {
        //                    //Log.Gravar("FecharRequerimento: " + msgErro, Log.TipoLog.Erro);
        //                    msgErro = AprovaRequerimentoViaSaidaMercadoria(DocEntry, linhas);
        //                    oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
        //                }
        //            }
        //            catch (Exception e)
        //            {
        //                msgErro = e.Message;

        //                if (oCompany.InTransaction)
        //                {
        //                    oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
        //                }
        //            }

        //            if (msgErro != null)
        //            {
        //                Log.Gravar(msgErro, Log.TipoLog.Erro);
        //            }
        //            else
        //            {
        //                NotificaUsuario(login, DocEntry);
        //            }

        //            return msgErro;
        //        }
        //        #endregion

        //        #region Inserir Requisição
        //        /*
        //         * Gerar um documento de saída de estoque a partir da requisição
        //         */
        //        public string InserirSaidaMercadoria(Login login, string observacoes, int docEntry, List<RequerimentoEstoqueLinhas> linhas, int Bp_ID, bool tipo)
        //        {
        //            Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-br", true);
        //            string erroMsg = null;
        //            //bool sCamposObrigatorio = true;
        //            SAPbobsCOM.Company oCompany = Conexao.Company;
        //            try
        //            {
        //                SAPbobsCOM.Documents oSaidaEstoque;
        //                oSaidaEstoque = (SAPbobsCOM.Documents)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenExit);

        //                oSaidaEstoque.DocDate = DateTime.Now;
        //                oSaidaEstoque.BPL_IDAssignedToInvoice = Bp_ID;
        //                oSaidaEstoque.Reference2 = docEntry.ToString();
        //                int x = 0;
        //                foreach (var linha in linhas)
        //                {

        //                    if (linha.DocStatus == "Aberta")
        //                    {
        //                        if (x > 0)
        //                        {
        //                            oSaidaEstoque.Lines.Add();
        //                        }
        //                        oSaidaEstoque.Lines.ItemCode = linha.CodigoItem;
        //                        SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

        //                        string SQL = "SELECT T0.\"UomEntry\" FROM OUOM T0 where T0.\"UomCode\"  = '" + linha.UndMedida + "' ";
        //                        oRecordset.DoQuery(SQL);

        //                        if (!oRecordset.EoF)
        //                        {
        //                            oSaidaEstoque.Lines.UoMEntry = int.Parse(oRecordset.Fields.Item("UomEntry").Value.ToString());
        //                        }

        //                        oSaidaEstoque.Lines.Quantity = linha.Quantidade;
        //                        oSaidaEstoque.Lines.WarehouseCode = linha.CodigoDeposito;
        //                        oSaidaEstoque.Lines.CostingCode = linha.OcrCode;
        //                        oSaidaEstoque.Lines.CostingCode2 = linha.OcrCode2;
        //                        oSaidaEstoque.Lines.CostingCode3 = linha.OcrCode3;
        //                        oSaidaEstoque.Lines.CostingCode4 = linha.OcrCode4;
        //                        oSaidaEstoque.Lines.CostingCode5 = linha.OcrCode5;

        //                        if (tipo)
        //                        {
        //                            if (linha.ManBtchNum == null)
        //                            {
        //                                int i = 0;
        //                                foreach (var linhaLote in linha.Lotes)
        //                                {
        //                                    if (linhaLote.Quantity != 0)
        //                                    {
        //                                        if (i != 0)
        //                                        {
        //                                            oSaidaEstoque.Lines.BatchNumbers.Add();
        //                                        }
        //                                        oSaidaEstoque.Lines.BatchNumbers.BatchNumber = linhaLote.BatchNum;
        //                                        oSaidaEstoque.Lines.BatchNumbers.Quantity = linhaLote.Quantity;
        //                                        oSaidaEstoque.Lines.BatchNumbers.InternalSerialNumber = linhaLote.SysNumber;
        //                                        i++;
        //                                    }
        //                                }
        //                            }
        //                        }

        //                    }
        //                    x++;
        //                }

        //                oSaidaEstoque.Comments = observacoes;

        //                if (oSaidaEstoque.Add() != 0)
        //                {
        //                    erroMsg = oCompany.GetLastErrorDescription();
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                erroMsg = ex.Message;
        //            }

        //            if (erroMsg != null)
        //            {
        //                Log.Gravar(erroMsg, Log.TipoLog.Erro);
        //            }
        //            //else
        //            //{

        //            //    NotificaResp(login, linhas);
        //            //}

        //            return erroMsg;
        //        }
        //        #endregion

        //        #region Fechar Requerimento
        //        public string FecharRequerimento(int DocEntry, List<RequerimentoEstoqueLinhas> linhas)
        //        {
        //            Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-br", true);

        //            string msg = null;

        //            SAPbobsCOM.Company oCompany = Conexao.Company;

        //            SAPbobsCOM.GeneralService oGeneralService;
        //            SAPbobsCOM.GeneralData oGeneralData;

        //            SAPbobsCOM.CompanyService sCmp;
        //            SAPbobsCOM.GeneralDataParams oGeneralParams;

        //            SAPbobsCOM.GeneralDataCollection oSons;
        //            SAPbobsCOM.GeneralData oSon;


        //            sCmp = oCompany.GetCompanyService();

        //            try
        //            {
        //                if (VerificarRequisicao(DocEntry))
        //                {
        //                    oGeneralService = sCmp.GetGeneralService("P1_OIGE");

        //                    //oGeneralData = (SAPbobsCOM.GeneralData)oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData);
        //                    oGeneralParams = (SAPbobsCOM.GeneralDataParams)oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralDataParams);

        //                    oGeneralParams.SetProperty("DocEntry", DocEntry);
        //                    oGeneralData = oGeneralService.GetByParams(oGeneralParams);
        //                    oSons = oGeneralData.Child("P1_IGE1");

        //                    //Se linhas for igual a Null, é o usuário clicou em fechar documento
        //                    if (linhas == null)
        //                    {
        //                        linhas = ListarLinhas(DocEntry, null);
        //                        oGeneralData.SetProperty("U_P1_STATUS", "C");
        //                        oGeneralData.SetProperty("U_P1_DTEFET", DateTime.Now);
        //                    }// Se todas as linhas forem fechadas
        //                    else if (CountOpenedLines(DocEntry) == linhas.Count)
        //                    {
        //                        oGeneralData.SetProperty("U_P1_STATUS", "C");
        //                        oGeneralData.SetProperty("U_P1_DTEFET", DateTime.Now);
        //                    }


        //                    foreach (RequerimentoEstoqueLinhas linha in linhas)
        //                    {
        //                        if (linha.DocStatus == "Aberta")
        //                        {
        //                            oSons.Item(linha.IdLinha - 1).SetProperty("U_P1_LinStatus", "F");
        //                            oSons.Item(linha.IdLinha - 1).SetProperty("U_P1_DTEFET", DateTime.Now);
        //                        }
        //                    }

        //                    oGeneralService.Update(oGeneralData);
        //                }
        //                else
        //                {
        //                    msg = "Erro: requerimento de Estoque não encontrado!";
        //                }
        //            }
        //            catch (Exception e)
        //            {
        //                msg = e.Message;
        //            }

        //            if (msg != null)
        //            {
        //                Log.Gravar(msg, Log.TipoLog.Erro);
        //            }

        //            return msg;
        //        }
        //        #endregion

        //        #region Muda Status Requerimento
        //        public string AprovaRequerimentoViaSaidaMercadoria(int DocEntry, List<RequerimentoEstoqueLinhas> linhas)
        //        {
        //            Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-br", true);

        //            string msg = null;

        //            SAPbobsCOM.Company oCompany = Conexao.Company;

        //            SAPbobsCOM.GeneralService oGeneralService;
        //            SAPbobsCOM.GeneralData oGeneralData;

        //            SAPbobsCOM.CompanyService sCmp;
        //            SAPbobsCOM.GeneralDataParams oGeneralParams;

        //            SAPbobsCOM.GeneralDataCollection oSons;
        //            SAPbobsCOM.GeneralData oSon;


        //            sCmp = oCompany.GetCompanyService();

        //            try
        //            {
        //                if (VerificarRequisicao(DocEntry))
        //                {
        //                    oGeneralService = sCmp.GetGeneralService("P1_OIGE");

        //                    //oGeneralData = (SAPbobsCOM.GeneralData)oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData);
        //                    oGeneralParams = (SAPbobsCOM.GeneralDataParams)oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralDataParams);

        //                    oGeneralParams.SetProperty("DocEntry", DocEntry);
        //                    oGeneralData = oGeneralService.GetByParams(oGeneralParams);
        //                    oSons = oGeneralData.Child("P1_IGE1");

        //                    //Se linhas for igual a Null, é o usuário clicou em fechar documento
        //                    if (linhas == null)
        //                    {
        //                        linhas = ListarLinhas(DocEntry, null);
        //                        oGeneralData.SetProperty("U_P1_STATUS", "A");
        //                        oGeneralData.SetProperty("U_P1_DTEFET", DateTime.Now);
        //                    }// Se todas as linhas forem fechadas
        //                    else if (CountOpenedLines(DocEntry) == linhas.Count)
        //                    {
        //                        oGeneralData.SetProperty("U_P1_STATUS", "A");
        //                        oGeneralData.SetProperty("U_P1_DTEFET", DateTime.Now);
        //                    }


        //                    foreach (RequerimentoEstoqueLinhas linha in linhas)
        //                    {
        //                        if (linha.DocStatus == "Aberta")
        //                        {
        //                            oSons.Item(linha.IdLinha - 1).SetProperty("U_P1_LinStatus", "F");
        //                            oSons.Item(linha.IdLinha - 1).SetProperty("U_P1_DTEFET", DateTime.Now);
        //                        }
        //                    }

        //                    oGeneralService.Update(oGeneralData);
        //                }
        //                else
        //                {
        //                    msg = "Erro: requerimento de Estoque não encontrado!";
        //                }
        //            }
        //            catch (Exception e)
        //            {
        //                msg = e.Message;
        //            }

        //            if (msg != null)
        //            {
        //                Log.Gravar(msg, Log.TipoLog.Erro);
        //            }

        //            return msg;
        //        }
        //        #endregion

        //        #region Número de Linhas com status em aberto
        //        public int CountOpenedLines(int DocEntry)
        //        {
        //            SAPbobsCOM.Company oCompany = Conexao.Company;
        //            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

        //            StringBuilder SQL = new StringBuilder();
        //            SQL.AppendLine("SELECT Count(*) as \"NumLine\" FROM \"@P1_IGE1\"");
        //            SQL.AppendLine("WHERE \"U_P1_LinStatus\"='O' AND \"DocEntry\"='" + DocEntry + "'");

        //            int countLines = 0;

        //            oRecordset.DoQuery(SQL.ToString());
        //            if (oRecordset.RecordCount > 0)
        //            {
        //                countLines = int.Parse(oRecordset.Fields.Item("NumLine").Value.ToString());
        //            }

        //            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
        //            oRecordset = null;

        //            return countLines;
        //        }
        //        #endregion

        //        #region Verifica se o Documento de Requisição existe
        //        public bool VerificarRequisicao(int DocEntry)
        //        {
        //            SAPbobsCOM.Company oCompany = Conexao.Company;
        //            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

        //            bool bExiste = false;

        //            StringBuilder SQL = new StringBuilder();
        //            SQL.AppendLine("SELECT Count(*) as \"NumLine\" FROM \"@P1_IGE1\"");
        //            SQL.AppendLine("WHERE \"U_P1_LinStatus\"='O' AND \"DocEntry\"='" + DocEntry + "'");

        //            oRecordset.DoQuery(SQL.ToString());

        //            if (oRecordset.RecordCount > 0)
        //            {
        //                if (oRecordset.Fields.Item("NumLine").Value.ToString() != "0")
        //                {
        //                    bExiste = true;
        //                }
        //            }

        //            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
        //            oRecordset = null;

        //            return bExiste;
        //        }
        //        #endregion

        //        #region Listar Requerimentos com lote
        //        public List<RequerimentoEstoqueLinhas> listLoteOnly(int DocEntry, bool ehLote, string[] linesChecked)
        //        {
        //            SAPbobsCOM.Company oCompany = Conexao.Company;
        //            SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

        //            List<RequerimentoEstoqueLinhas> requerimentosLinhas = new List<RequerimentoEstoqueLinhas>();

        //            StringBuilder SQL = new StringBuilder();


        //            SQL.AppendLine("SELECT T0.\"DocEntry\", T0.\"DocNum\",i.\"ManBtchNum\",");
        //            SQL.AppendLine("case when T0.\"U_P1_STATUS\" = 'O' then 'Aberta' else 'Fechada' end \"DocStatus\",");
        //            SQL.AppendLine("T1.\"LineId\",T1.\"U_P1_Quantity\", T1.\"U_P1_ItemCode\",i.\"ItemName\",T1.\"U_P1_WhsCode\", T1.\"U_P1_Quantity\",");
        //            SQL.AppendLine("case when T1.\"U_P1_LinStatus\" = 'O' then 'Aberta'");
        //            SQL.AppendLine("when T1.\"U_P1_LinStatus\" = 'C'");
        //            SQL.AppendLine("then 'Cancelada' else 'Fechada' end \"LinStatus\"");
        //            SQL.AppendLine("FROM \"@P1_OIGE\"  T0 inner join \"@P1_IGE1\"  T1 on T0.\"DocEntry\" = T1.\"DocEntry\"");
        //            SQL.AppendLine("inner join oitm i on t1.\"U_P1_ItemCode\" = i.\"ItemCode\"");
        //            SQL.AppendLine("WHERE T0.\"DocEntry\" = '" + DocEntry + "' ");

        //            SQL.AppendLine("AND T1.\"LineId\" in (" + string.Join(",", linesChecked) + ")");

        //            if (ehLote)
        //            {
        //                SQL.AppendLine(" AND i.\"ManBtchNum\" = 'Y'");
        //            }
        //            else
        //            {
        //                SQL.AppendLine(" AND i.\"ManBtchNum\" = 'N'");
        //            }

        //            oRecordSet.DoQuery(SQL.ToString());
        //            oRecordSet.MoveFirst();
        //            while (!oRecordSet.EoF)
        //            {
        //                RequerimentoEstoqueLinhas linha = new RequerimentoEstoqueLinhas();
        //                linha.DocEntry = int.Parse(oRecordSet.Fields.Item("DocEntry").Value.ToString().Trim());
        //                linha.CodigoDeposito = oRecordSet.Fields.Item("U_P1_WhsCode").Value.ToString();
        //                linha.DocNum = int.Parse(oRecordSet.Fields.Item("DocNum").Value.ToString().Trim());
        //                linha.DocStatus = oRecordSet.Fields.Item("DocStatus").Value.ToString();
        //                linha.IdLinha = int.Parse(oRecordSet.Fields.Item("LineId").Value.ToString().Trim());
        //                linha.ManBtchNum = oRecordSet.Fields.Item("ManBtchNum").Value.ToString();
        //                linha.CodigoItem = oRecordSet.Fields.Item("U_P1_ItemCode").Value.ToString();
        //                linha.DecricaoItem = oRecordSet.Fields.Item("ItemName").Value.ToString();
        //                if (String.IsNullOrEmpty(oRecordSet.Fields.Item("U_P1_Quantity").Value.ToString().Trim()))
        //                {
        //                    linha.Quantidade = 0;
        //                }
        //                else
        //                {
        //                    linha.Quantidade = double.Parse(oRecordSet.Fields.Item("U_P1_Quantity").Value.ToString().Trim());
        //                }
        //                linha.StatusLinha = oRecordSet.Fields.Item("LinStatus").Value.ToString();

        //                requerimentosLinhas.Add(linha);
        //                oRecordSet.MoveNext();
        //            }

        //            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
        //            oRecordSet = null;

        //            return requerimentosLinhas;
        //        }
        //        #endregion

        //        #region LIST de lote

        //        public List<Lotes> listLotes(string ItemCode, string codigoDeposito)
        //        {
        //            SAPbobsCOM.Company oCompany = Conexao.Company;
        //            SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

        //            List<Lotes> lotes = new List<Lotes>();

        //            StringBuilder SQL = new StringBuilder();

        //            SQL.AppendLine("SELECT \"WhsCode\", \"ItemName\", \"Quantity\",\"ItemCode\",\"SysNumber\",");
        //            SQL.AppendLine("\"BatchNum\" FROM \"OIBT\" T0 WHERE \"Quantity\" >0 AND \"ItemCode\" = '" + ItemCode + "' AND \"WhsCode\" = '" + codigoDeposito + "' ");
        //            oRecordSet.DoQuery(SQL.ToString());

        //            oRecordSet.MoveFirst();
        //            while (!oRecordSet.EoF)
        //            {
        //                Lotes linha = new Lotes();
        //                linha.WhsCode = oRecordSet.Fields.Item("WhsCode").Value.ToString();
        //                linha.ItemName = oRecordSet.Fields.Item("ItemName").Value.ToString();
        //                linha.Quantity = double.Parse(oRecordSet.Fields.Item("Quantity").Value.ToString().Trim());
        //                linha.ItemCode = oRecordSet.Fields.Item("ItemCode").Value.ToString();
        //                linha.SysNumber = oRecordSet.Fields.Item("SysNumber").Value.ToString();
        //                linha.BatchNum = oRecordSet.Fields.Item("BatchNum").Value.ToString();

        //                lotes.Add(linha);
        //                oRecordSet.MoveNext();
        //            }

        //            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
        //            oRecordSet = null;

        //            return lotes;
        //        }

        //        #endregion

        //        #region Notifica responsavel pelo setor

        //        public void NotificaResp(Login user, List<RequerimentoEstoqueLinhas> reqEst)
        //        {
        //            List<Login> resp = getResponsavelSetor();
        //            List<String> itens = new List<String>();
        //            if (resp.Count() != 0)
        //            {
        //                foreach (var Responsavel in resp)
        //                {
        //                    if (!String.IsNullOrEmpty(Responsavel.email))
        //                    {
        //                        foreach (var item in reqEst)
        //                        {
        //                            itens.Add("<tr style=\"text-align:center;\"><td>" + item.Quantidade + " </td><td> " + item.DecricaoItem + " </td></tr>");
        //                        }
        //                        envio.EnviaEmail(Responsavel.email, user.User, null, "Requerimento de Estoque", itens);
        //                    }
        //                }
        //            }

        //            //BillMessageController.SendMail(null, null, resp.email, null); //envia um email para o responsavel do setor
        //        }

        //        #endregion

        //        #region Notifica soliciante estoque

        //        public void NotificaUsuario(Login user, int DocEntry)
        //        {
        //            RequerimentoEstoque doc = GetByKey(DocEntry);
        //            List<String> itens = new List<String>();

        //            //BillMessageController.SendMail(null, null, resp.email, null); //envia um email para quem fez o pedido
        //            //string EmailEnvio,string Responsavel,string Idoc,string tipoDoc)
        //            if (!String.IsNullOrEmpty(doc.Email))
        //            {
        //                itens.Add(doc.DataLancamento.ToString("dd/MM/yyyy"));
        //                envio.EnviaEmail(doc.Email, doc.Requisitante, doc.DocNum.ToString(), "Notificação Usuário", itens);
        //            }

        //        }

        //        #endregion

        //        #region pega responsavel pelo setor estoque

        //        public List<Login> getResponsavelSetor()
        //        {
        //            SAPbobsCOM.Company oCompany = Conexao.Company;
        //            SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

        //            List<Login> respSetor = new List<Login>();

        //            StringBuilder SQL = new StringBuilder();

        //            SQL.AppendLine("SELECT T0.\"empID\", concat(concat(T0.\"firstName\", '.'), T0.\"lastName\") AS \"Name\",");
        //            SQL.AppendLine("coalesce(T0.\"BPLId\", 1) \"BPLId\", coalesce(T0.\"email\", '') \"email\", coalesce(T0.\"userId\", -1) \"userId\", 'F' \"Tipo\"");
        //            SQL.AppendLine("FROM OHEM T0");
        //            SQL.AppendLine("INNER JOIN \"@P1_ACCESS\" T1 ON T1.\"U_P1_EMPId\" = T0.\"empID\"");
        //            SQL.AppendLine("WHERE T1.\"U_P1_ReqS\" = 'T'");

        //            //SQL.AppendLine("AND \"empID\" = '7'");

        //            oRecordSet.DoQuery(SQL.ToString());

        //            oRecordSet.MoveFirst();
        //            while (!oRecordSet.EoF)
        //            {
        //                Login linha = new Login();
        //                linha.Id = int.Parse(oRecordSet.Fields.Item("userId").Value.ToString());
        //                linha.email = oRecordSet.Fields.Item("email").Value.ToString();
        //                linha.User = oRecordSet.Fields.Item("Name").Value.ToString();
        //                respSetor.Add(linha);

        //                oRecordSet.MoveNext();
        //            }

        //            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
        //            oRecordSet = null;

        //            return respSetor;
        //        }

        //        #endregion

    }

}