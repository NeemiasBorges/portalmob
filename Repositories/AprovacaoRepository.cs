﻿using PowerOne.Database;
using PowerOne.Models;
using PowerOne.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Threading;
using System.Globalization;

namespace PowerOne.Repository
{
    public class AprovacaoRepository
    {
        //#region aprovacao interna
        //public string caiNaAprovacao(string tipoDoc, int DocEntry, string Etapa, Login login)
        //{
        //    Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-br", true);
        //    int iRetcode;
        //    string msg = null;
        //    try
        //    {
        //        SAPbobsCOM.Company oCompany;
        //        oCompany = Conexao.Company;
        //        SAPbobsCOM.UserTable oAccess = oCompany.UserTables.Item("P1_APRVDOC");
        //        if (oAccess.GetByKey(DocEntry.ToString()))
        //        {
        //            //oAccess.UserFields.Fields.Item("U_P1_CLASSDOC").Value = tipoDoc;
        //            oAccess.UserFields.Fields.Item("U_P1_NVLAPRV").Value = Etapa;
        //            //oAccess.UserFields.Fields.Item("U_P1_DOCNUM").Value = DocEntry;

        //            iRetcode = oAccess.Update();
        //        }
        //        else
        //        {
        //            oAccess.Name = DocEntry.ToString();
        //            oAccess.Code = DocEntry.ToString();
        //            oAccess.UserFields.Fields.Item("U_P1_CLASSDOC").Value = tipoDoc;
        //            oAccess.UserFields.Fields.Item("U_P1_NVLAPRV").Value = Etapa;
        //            oAccess.UserFields.Fields.Item("U_P1_DOCNUM").Value = DocEntry;
        //            oAccess.UserFields.Fields.Item("U_P1_DTSOLIC").Value = DateTime.UtcNow;

        //            iRetcode = oAccess.Add();
        //        }


        //        if (iRetcode != 0)
        //        {
        //            msg = oCompany.GetLastErrorDescription();
        //            Log.Gravar(msg, Log.TipoLog.Erro);
        //        }

        //        System.Runtime.InteropServices.Marshal.ReleaseComObject(oAccess);
        //        oAccess = null;

        //        if (msg != null)
        //        {
        //            Log.Gravar(msg, Log.TipoLog.Erro);
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        msg = e.Message;
        //    }

        //    return msg;
        //}
        //#endregion

        //#region pega Aprovacoes
        //public List<Aprovacao> getSuspensos(List<String> auths,string Status)
        //{
        //    List<Aprovacao> suspensos = new List<Aprovacao>();

        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

        //    StringBuilder SQL = new StringBuilder();
        //    //SQL.AppendLine("SELECT \"U_P1_CLASSDOC\" as \"Tipo\",\"U_P1_SOLIC\",CASE WHEN \"U_P1_NVLAPRV\" = 'W' THEN 'Pendente' WHEN \"U_P1_NVLAPRV\" = 'N' THEN 'Rejeitada' WHEN  \"U_P1_NVLAPRV\" = 'Y' THEN 'Aprovada' ELSE 'Cancelada'" +
        //    //    " END  \"U_P1_NVLAPRV\"," +
        //    //    "CASE WHEN \"U_P1_CLASSDOC\" = 'PC' THEN 'Prestação de Contas' WHEN \"U_P1_CLASSDOC\" = 'LD' THEN 'Lançamento de Despesa' WHEN \"U_P1_CLASSDOC\" = 'RE' THEN 'Requerimento Estoque' END \"U_P1_CLASSDOC\", " +
        //    //    "\"U_P1_DOCNUM\" FROM \"@P1_APRVDOC\" ");
        //    string filtros = "";
        //    int aux = 0;
        //    foreach (var item in auths)
        //    {
        //        if (aux != 0)
        //        {
        //            filtros += ",";
        //        }
        //        filtros += "'" + item + "'";
        //        aux++;
        //    }

        //        SQL.AppendLine("SELECT *");
        //        SQL.AppendLine("FROM(");

        //        SQL.AppendLine("SELECT TOP 50 \"U_P1_CLASSDOC\" as \"Tipo\", \"U_P1_SOLIC\",");
        //        SQL.AppendLine("CASE WHEN \"U_P1_NVLAPRV\" = 'W' THEN 'Pendente' WHEN \"U_P1_NVLAPRV\" = 'N' THEN 'Rejeitada' WHEN  \"U_P1_NVLAPRV\" = 'Y' THEN 'Aprovada' ELSE 'Cancelada' END  \"U_P1_NVLAPRV\",");
        //        SQL.AppendLine("CASE WHEN \"U_P1_CLASSDOC\" = 'PC' THEN 'Prestação de Contas' WHEN \"U_P1_CLASSDOC\" = 'LD' THEN 'Lançamento de Despesa' WHEN \"U_P1_CLASSDOC\" = 'RE' THEN 'Requerimento Estoque' END \"U_P1_CLASSDOC\", \"U_P1_DOCNUM\" as \"DOCNUM\"  , \"U_P1_DTSOLIC\" AS \"DATA\" FROM \"@P1_APRVDOC\"");
        //        SQL.AppendLine("WHERE \"U_P1_CLASSDOC\" IN ("+ filtros + ") ");

        //    if (auths.Contains("RE"))
        //    {
        //        SQL.AppendLine("UNION ALL");
        //        SQL.AppendLine("SELECT TOP 50 'RE' \"Tipo\", CONCAT(CONCAT(H.\"firstName\", '.'), H.\"lastName\") as \"Requisitante\",");
        //        SQL.AppendLine("CASE WHEN COALESCE(T0.\"U_P1_STATUS\", 'O') = 'O'  THEN 'Pendente' WHEN COALESCE(T0.\"U_P1_STATUS\", 'O') = 'A' THEN 'Aprovada' ELSE 'Cancelada' END \"DocStatus\",");
        //        SQL.AppendLine("'Requerimento de Estoque' as \"U_P1_CLASSDOC\", T0.\"DocEntry\" as \"DOCNUM\" , T0.\"U_P1_DTLANC\" AS \"DATA\" FROM \"@P1_OIGE\"  T0 INNER JOIN OHEM H");
        //        SQL.AppendLine("ON T0.\"U_P1_Owner\" = H.\"empID\" ");

        //    }
        //        SQL.AppendLine(") T3 WHERE 1=1 AND \"U_P1_NVLAPRV\" = '" + Status + "'");
        //        SQL.AppendLine(" ORDER BY T3.\"DATA\" DESC, \"DOCNUM\" DESC");


        //    oRecordset.DoQuery(SQL.ToString());

        //    while (!oRecordset.EoF)
        //    {
        //        Aprovacao item = new Aprovacao();
      
        //        item.NumeroDoc      = int.Parse(oRecordset.Fields.Item("DOCNUM").Value.ToString());
        //        item.NvlAprov       = oRecordset.Fields.Item("U_P1_NVLAPRV").Value.ToString();
        //        item.ClassDoc       = oRecordset.Fields.Item("U_P1_CLASSDOC").Value.ToString();
        //        item.Solicitante    = oRecordset.Fields.Item("U_P1_SOLIC").Value.ToString();
        //        item.Tipo           = oRecordset.Fields.Item("Tipo").Value.ToString();
        //        item.DataSolicitac  = DateTime.Parse(oRecordset.Fields.Item("DATA").Value.ToString());

        //        suspensos.Add(item);

        //        oRecordset.MoveNext();
        //    }

        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
        //    oRecordset = null;

        //    return suspensos;
        //}
        //#endregion
    }
}