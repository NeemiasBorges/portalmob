﻿using DataBase;
using Models;
using PortalMOB.Repositories.Interfaces;
using PowerOne.Database;
using PowerOne.Models;
using PowerOne.Models.ViewModel;
using PowerOne.Util;
using SAPbobsCOM;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Timers;
using static PowerOne.Util.Log;
using Newtonsoft.Json;
using Utils;
using DAL;

namespace PowerOne.Repository
{
    public class LoginRepository : ILoginRepository
    {

        #region Consulta valor log login

        //public bool ConsultaLogLogin(Login login)
        //{
        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

        //    bool aux = true;

        //    StringBuilder SQL = new StringBuilder();
        //    SQL.AppendLine("SELECT \"U_P1_Status\", \"U_P1_MACaddress\" FROM \"@P1_ACCESS\" WHERE \"U_P1_EMPId\" = " + login.Id + " ");

        //    oRecordSet.DoQuery(SQL.ToString());


        //    if (!oRecordSet.EoF)
        //    {
        //        oRecordSet.MoveFirst();
        //        if (oRecordSet.Fields.Item("U_P1_MACaddress").Value.ToString() != "")
        //        {
        //            if (oRecordSet.Fields.Item("U_P1_MACaddress").Value.ToString() != login.MACaddress)
        //            {
        //                aux = false;
        //            }
        //            else
        //            {
        //                aux = true;
        //            }
        //        }

        //        //if (oRecordSet.Fields.Item("U_P1_Status").Value.ToString() != "")
        //        //{
        //        //    if (oRecordSet.Fields.Item("U_P1_Status").Value.ToString() == "D")
        //        //    {
        //        //        aux = false;
        //        //    }
        //        //    else
        //        //    {
        //        //        aux = true;
        //        //    }
        //        //}
        //    }

        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
        //    oRecordSet = null;

        //    return aux;
        //}
        #endregion

        #region Logout update login
        //public string logoutLogLogin(Login login)
        //{
        //    string msg = null;

        //    SAPbobsCOM.Company oCompany;
        //    oCompany = Conexao.Company;
        //    SAPbobsCOM.UserTable oAccess = oCompany.UserTables.Item("P1_ACCESS");

        //    if (oAccess.GetByKey(login.Id.ToString()))
        //    {
        //        oAccess.UserFields.Fields.Item("U_P1_Status").Value = "D";

        //        int iRetcode = oAccess.Update();

        //        if (iRetcode != 0)
        //        {
        //            msg = oCompany.GetLastErrorDescription();
        //            Log.Gravar(msg, Log.TipoLog.Erro);
        //        }
        //    }

        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oAccess);
        //    oAccess = null;

        //    if (msg != null)
        //    {
        //        Log.Gravar(msg, Log.TipoLog.Erro);
        //    }

        //    return msg;
        //}
        //#endregion

        //#region add valor logado
        //public string AddValorLogin(Login login)
        //{
        //    string msg = null;

        //    SAPbobsCOM.Company oCompany;
        //    oCompany = Conexao.Company;
        //    SAPbobsCOM.UserTable oAccess = oCompany.UserTables.Item("P1_ACCESS");

        //    if (oAccess.GetByKey(login.Id.ToString()))
        //    {
        //        oAccess.UserFields.Fields.Item("U_P1_Status").Value = "L";
        //        oAccess.UserFields.Fields.Item("U_P1_MACaddress").Value = login.MACaddress.ToString();

        //        int iRetcode = oAccess.Update();

        //        if (iRetcode != 0)
        //        {
        //            msg = oCompany.GetLastErrorDescription();
        //            Log.Gravar(msg, Log.TipoLog.Erro);
        //        }
        //    }

        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oAccess);
        //    oAccess = null;

        //    if (msg != null)
        //    {
        //        Log.Gravar(msg, Log.TipoLog.Erro);
        //    }

        //    return msg;

        //}
        #endregion

        #region Login
        public Login Login(Login login)
        {
            Login user = null;

            try
            {
                StringBuilder SQL = new StringBuilder();

                SQL.AppendLine("SELECT TOP 1 T0.\"empID\" as \"Id\", concat(concat(T0.\"firstName\", '.'), T0.\"lastName\") AS \"User\",");
                SQL.AppendLine("coalesce(T0.\"BPLId\", 1) \"IdFilial\", coalesce(T0.\"email\", '') \"email\", coalesce(T0.\"userId\", -1) \"UserId\"");
                SQL.AppendLine("FROM OHEM T0 ");
                SQL.AppendLine("INNER JOIN \"@P1_ACCESS\" T1 ON T1.\"U_P1_EMPId\" = T0.\"empID\"");
                SQL.AppendLine("WHERE \"email\" = '" + login.email + "' AND COALESCE(T0.\"U_P1_STPORTAL\",'') != 'N' ");
                SQL.AppendLine("AND(T1.\"U_P1_Password\" = '" + Criptografia.CalculateSHA1(login.Password) + "' OR");
                SQL.AppendLine("(T1.\"U_P1_Password\" = 'NOVO' and T1.\"U_P1_Password\" = '" + login.Password + "')) FOR JSON PATH");

                user = Conexao.ConsultaBanco<Login[]>(SQL.ToString())[0];
            }
            catch (Exception e)
            {
                Log.Gravar(e.Message,TipoLog.Erro);
            }

            return user;
        }
        #endregion

        #region Alterar Senha
        public void EditarSenha(AlterarSenhaViewModel alterarSenha)
        {
            //SAPbobsCOM.Company oCompany;
            //oCompany = Conexao.Company;
            //SAPbobsCOM.UserTable oAccess = oCompany.UserTables.Item("P1_ACCESS");

            //if (oAccess.GetByKey(alterarSenha.Id.ToString()))
            //{
            //    oAccess.UserFields.Fields.Item("U_P1_Password").Value = Criptografia.CalculateSHA1(alterarSenha.NewPassword);

            //    int iRetcode = oAccess.Update();

            //    if (iRetcode != 0)
            //    {
            //        string sErrMessage = oCompany.GetLastErrorDescription();
            //        Log.Gravar(sErrMessage, Log.TipoLog.Erro);
            //    }
            //}

            //System.Runtime.InteropServices.Marshal.ReleaseComObject(oAccess);
            //oAccess = null;
        }
        #endregion

        //#region Alterar Senha
        //public void AltSenhaPN(AlterarSenhaViewModel alterarSenha)
        //{
        //    SAPbobsCOM.Company oCompany;
        //    oCompany = Conexao.Company;
        //    SAPbobsCOM.UserTable oAccess = oCompany.UserTables.Item("P1_PNACESS");

        //    if (oAccess.GetByKey(alterarSenha.Id.ToString()))
        //    {
        //        oAccess.UserFields.Fields.Item("U_P1_PNPassword").Value = Criptografia.CalculateSHA1(alterarSenha.NewPassword);

        //        int iRetcode = oAccess.Update();

        //        if (iRetcode != 0)
        //        {
        //            string sErrMessage = oCompany.GetLastErrorDescription();
        //            Log.Gravar(sErrMessage, Log.TipoLog.Erro);
        //        }
        //    }

        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oAccess);
        //    oAccess = null;

        //}
        //#endregion

        //#region Alterar Parceiro de negocio
        //public void EditarSenhaPN(AlterarSenhaViewModel alterarSenha)
        //{
        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    string msg = null;

        //    try
        //    {
        //        SAPbobsCOM.BusinessPartners oPN = (SAPbobsCOM.BusinessPartners)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners);


        //        if (oPN.GetByKey(alterarSenha.CodPNLink))
        //        {
        //            //oPN.Password = Criptografia.CalculateSHA1(alterarSenha.NewPassword); valor que a criptografia gera ultrapassa o valor suportado pelo SAP, Criar UDF?
        //            //oPN.Password = alterarSenha.NewPassword;
        //            oPN.UserFields.Fields.Item("U_P1_Password").Value = Criptografia.CalculateSHA1(alterarSenha.NewPassword);

        //            if (oPN.Update() != 0)
        //            {
        //                msg = oCompany.GetLastErrorDescription();
        //                Log.Gravar(msg, Log.TipoLog.Erro);
        //            }

        //            System.Runtime.InteropServices.Marshal.ReleaseComObject(oPN);
        //            oPN = null;
        //        }
        //        else
        //        {
        //            msg = "Documento inexistente!";
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        msg = e.Message;
        //    }

        //    if (msg != null)
        //    {
        //        Log.Gravar(msg, Log.TipoLog.Erro);
        //    }

        //}
        //#endregion

        #region Pegar Usuários por: Primeiro Nome, Últim Nome e Email

        public string GetIdUsuarioPorEmail(string email)
        {
            string empID = null;
            try
            {
            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT \"empID\" FROM OHEM ");
            SQL.AppendLine("WHERE \"email\" = '" + email + "' ");

                empID = Conexao.ConsultaBanco<string>(SQL.ToString());
            }
            catch (Exception e)
            {
                Log.Gravar(e.Message, TipoLog.Erro);
            }

            return empID;
        }
        #endregion

        //#region Pegar PN por: Cod E Email
        //public Login GetPNEmail(string email)
        //{
        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

        //    Login User = new Login();
        //    StringBuilder SQL = new StringBuilder();
        //    SQL.AppendLine("SELECT \"Code\", \"Name\" FROM \"@P1_PNACESS\" ");
        //    SQL.AppendLine("WHERE \"Name\" = '" + email + "' ");


        //    oRecordset.DoQuery(SQL.ToString());

        //    oRecordset.MoveFirst();
        //    while (!oRecordset.EoF)
        //    {
        //        User.email = oRecordset.Fields.Item("Name").Value.ToString();
        //        User.Id = int.Parse(oRecordset.Fields.Item("Code").Value.ToString());
        //        oRecordset.MoveNext();
        //    }

        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
        //    oRecordset = null;

        //    return User;
        //}
        //#endregion

        //#region Pegar Id Todos os Usuários 
        //public List<Login> GetAllIdUsers()
        //{
        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

        //    List<Login> usersId = new List<Login>();

        //    StringBuilder SQL = new StringBuilder();
        //    SQL.AppendLine("SELECT \"empID\", \"firstName\" FROM \"OHEM\"");

        //    oRecordSet.DoQuery(SQL.ToString());
        //    while (!oRecordSet.EoF)
        //    {
        //        usersId.Add(
        //            new Models.Login()
        //            {
        //                Id = int.Parse(oRecordSet.Fields.Item("empID").Value.ToString()),
        //                User = oRecordSet.Fields.Item("FirstName").Value.ToString()
        //            });
        //        oRecordSet.MoveNext();
        //    }


        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
        //    oRecordSet = null;

        //    return usersId;
        //}
        //#endregion

        //#region Pegar Todos os Usuários 
        //public List<Login> GetAllUsers()
        //{
        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

        //    List<Login> usersId = new List<Login>();

        //    StringBuilder SQL = new StringBuilder();
        //    SQL.AppendLine("SELECT \"empID\", \"firstName\",\"lastName\",\"U_P1_VINCULOPN\" FROM \"OHEM\"");

        //    oRecordSet.DoQuery(SQL.ToString());
        //    while (!oRecordSet.EoF)
        //    {
        //        usersId.Add(
        //            new Models.Login()
        //            {
        //                Id = int.Parse(oRecordSet.Fields.Item("empID").Value.ToString()),
        //                User = oRecordSet.Fields.Item("FirstName").Value.ToString() + " " + oRecordSet.Fields.Item("lastName").Value.ToString(),
        //                CodPNLink = oRecordSet.Fields.Item("U_P1_VINCULOPN").Value.ToString()

        //            });
        //        oRecordSet.MoveNext();
        //    }


        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
        //    oRecordSet = null;

        //    return usersId;
        //}
        //#endregion

        //#region Pegar Usuários por pesquisa
        //public List<Login> Search(string pesquisa)
        //{
        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

        //    List<Login> colaboradores = new List<Login>();

        //    StringBuilder SQL = new StringBuilder();
        //    SQL.AppendLine("SELECT TOP 15 \"empID\", \"firstName\", \"lastName\" FROM \"OHEM\" ");
        //    SQL.AppendLine("WHERE (( UPPER(\"empID\") like '%" + pesquisa + "%') OR ( UPPER(\"firstName\") like '%" + pesquisa + "%') OR ( UPPER(\"lastName\") like '%" + pesquisa + "%')) ");

        //    oRecordSet.DoQuery(SQL.ToString());
        //    while (!oRecordSet.EoF)
        //    {
        //        colaboradores.Add(
        //            new Login()
        //            {
        //                Id = int.Parse(oRecordSet.Fields.Item("empID").Value.ToString()),
        //                User = oRecordSet.Fields.Item("firstName").Value.ToString(),
        //                UserLastName = oRecordSet.Fields.Item("lastName").Value.ToString()

        //            });
        //        oRecordSet.MoveNext();
        //    }


        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
        //    oRecordSet = null;

        //    return colaboradores;
        //}
        //#endregion

        //#region Pegar Usuários por pesquisa
        //public List<Login> SearchAdv(string pesquisa)
        //{
        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

        //    List<Login> colaboradores = new List<Login>();
        //    StringBuilder SQL = new StringBuilder();
        //    SQL.AppendLine("SELECT \"empID\", \"firstName\", \"lastName\" FROM \"OHEM\" ");
        //    SQL.AppendLine("WHERE (\"position\" = '6' OR \"jobTitle\" = 'Advogado') AND(( UPPER(\"empID\") like '%" + pesquisa + "%') OR ( UPPER(\"firstName\") like '%" + pesquisa + "%') OR ( UPPER(\"lastName\") like '%" + pesquisa + "%')) ORDER BY \"firstName\"");

        //    oRecordSet.DoQuery(SQL.ToString());
        //    while (!oRecordSet.EoF)
        //    {
        //        colaboradores.Add(
        //            new Login()
        //            {
        //                Id = int.Parse(oRecordSet.Fields.Item("empID").Value.ToString()),
        //                User = oRecordSet.Fields.Item("firstName").Value.ToString(),
        //                UserLastName = oRecordSet.Fields.Item("lastName").Value.ToString()

        //            });
        //        oRecordSet.MoveNext();
        //    }

        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
        //    oRecordSet = null;

        //    return colaboradores;
        //}
        //#endregion

        //#region Atualizar Senha de Usuários já cadastrados
        //public void ToFillTheEmptyPassword()
        //{
        //    SAPbobsCOM.Company oCompany;
        //    oCompany = Conexao.Company;
        //    SAPbobsCOM.UserTable oAccess = oCompany.UserTables.Item("P1_ACCESS");
        //    //string senhaCript = "NOVO";
        //    //senhaCript += Criptografia.Encrypt(senhaCript, true);
        //    List<Models.Login> users = GetUserEmptyPasswords();

        //    if (users != null)
        //    {
        //        foreach (var user in users)
        //        {
        //            if (oAccess.GetByKey(user.Id.ToString()))
        //            {
        //                oAccess.UserFields.Fields.Item("U_P1_Password").Value = "NOVO";

        //                int iRetcode = oAccess.Update();

        //                if (iRetcode != 0)
        //                {
        //                    string sErrMessage = oCompany.GetLastErrorDescription();
        //                }
        //            }

        //            //System.Runtime.InteropServices.Marshal.ReleaseComObject(oAccess);
        //            //oAccess = null;
        //        }
        //    }

        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oAccess);
        //    oAccess = null;

        //}

        //public List<Models.Login> GetUserEmptyPasswords()
        //{
        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

        //    List<Models.Login> userLogin = new List<Models.Login>();

        //    string SQL = "SELECT \"U_P1_EMPId\" FROM \"@P1_ACCESS\" WHERE \"U_P1_Password\" IS NULL";

        //    oRecordset.DoQuery(SQL);

        //    while (!oRecordset.EoF)
        //    {
        //        //oRecordset.MoveFirst();
        //        userLogin.Add(new Models.Login()
        //        {
        //            Id = int.Parse(oRecordset.Fields.Item(0).Value.ToString())
        //        });

        //        oRecordset.MoveNext();
        //    }

        //    //if (!oRecordset.EoF)
        //    //{
        //    //    oRecordset.MoveFirst();
        //    //    userLogin.Add(new Models.Login()
        //    //    {
        //    //        Id = int.Parse(oRecordset.Fields.Item(0).Value.ToString()),
        //    //        User = oRecordset.Fields.Item(1).Value.ToString()
        //    //    });
        //    //}
        //    //else
        //    //{
        //    //    userLogin = null;
        //    //}

        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
        //    oRecordset = null;

        //    return userLogin;
        //}

        //#endregion

        #region pegar endereço mac
        public string GetMacAddress()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-br", true);
            string chars = "ABCDEFGHIJKM";
            string pass = "";
            Random random = new Random();
            for (int f = 0; f < 4; f++)
            {
                pass += chars.Substring(random.Next(0, chars.Length - 1), 1);
            }
            string aux1 = DateTime.Now.ToString(CultureInfo.InvariantCulture).Trim();
            string tratado = aux1.Replace("/", "").Trim().Replace(":", "").Trim().Replace(" ", "");
            string code = String.Concat(pass, tratado.Remove(0, 6));
            code = code.Substring(0, 12);
            return code;
        }
        #endregion

        #region ConsultaId OHEM
        public int ConsultaId(Login login)
        {
            Login user = null;

            try
            {
                StringBuilder SQL = new StringBuilder();
                SQL.AppendLine("SELECT \"empID\" AS \"Id\" FROM OHEM WHERE \"email\" = '" + login.email + "' FOR JSON PATH");

                user = Conexao.ConsultaBanco<Login[]>(SQL.ToString())[0];

            }
            catch (Exception e)
            {
                Log.Gravar(e.Message, TipoLog.Erro);
            }

            return user.Id;
        }
        #endregion

        //#region ConsultaId OCPR
        //public string ConsultaCliente(Login login)
        //{
        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

        //    string Id = null;
        //    StringBuilder SQL = new StringBuilder();
        //    SQL.AppendLine("SELECT \"CntctCode\" FROM OCPR WHERE \"E_MailL\" = '" + login.email + "'");
        //    oRecordset.DoQuery(SQL.ToString());

        //    oRecordset.MoveFirst();
        //    while (!oRecordset.EoF)
        //    {
        //        Id = oRecordset.Fields.Item("CntctCode").Value.ToString();
        //        oRecordset.MoveNext();
        //    }

        //    oRecordset = null;

        //    return Id;
        //}
        //#endregion

        #region consulta o id na p1acess
        public bool ConsultaCadastro(int idUser)
        {
            DataTable user = null;
            bool Valid = false;
       
            try
            {
                StringBuilder SQL = new StringBuilder();
                SQL.AppendLine("SELECT COALESCE(\"U_P1_EMPId\",0) as \"Id\" FROM \"@P1_ACCESS\" ");
                SQL.AppendLine("WHERE \"U_P1_EMPId\" = '" + idUser + "' FOR JSON PATH");

                user = Conexao.ConsultaBanco(SQL.ToString());

                if (user.Rows.Count != 0)
                {
                    Valid = true;
                }
            }
            catch (Exception e)
            {
                Log.Gravar(e.Message, TipoLog.Erro);
            }

            return Valid;
        }
        #endregion

        //#region consulta o id na p1PNacess
        //public bool ConsultaCadastroCliente(string idUser)
        //{
        //    bool Valid;

        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

        //    StringBuilder SQL = new StringBuilder();
        //    SQL.AppendLine("SELECT \"Code\" FROM \"@P1_PNACESS\" WHERE \"Code\" = '" + idUser + "' ");

        //    oRecordset.DoQuery(SQL.ToString());

        //    oRecordset.MoveFirst();
        //    if (!oRecordset.EoF)
        //    {
        //        Valid = true;
        //    }
        //    else
        //    {
        //        Valid = false;
        //    }

        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
        //    oRecordset = null;

        //    return Valid;
        //}
        //#endregion

        #region Add valor caso nao exista 
        public string AddAcess(Login login, int idUser)
        {
 
            string msg = null;
            string jsonEnvio = "{\"Code\":   " + idUser + ",\"Name\" : " + idUser + ",\"U_P1_EMPId\": " + idUser + ",\"U_P1_Password\" : \"NOVO\"}";

            try
            {
                msg = RequestUtils.Request(ConstantUtils.URL_P1_ACESS, RequestUtils.POST, LoginDAL.sessionId, jsonEnvio, false);
                //Request(string url, string metodoEnvio, string sessionId, string jsonEnvio, bool sAddHeaderPatch)
            }
            catch (Exception e)
            {
                msg = e.Message;
            }

            if (msg != null)
            {
                Log.Gravar(msg, Log.TipoLog.Erro);
            }

            return msg;

        }
        #endregion

        //#region Add valor caso nao exista 
        //public string AddAcessPN(Login login, string idUser)
        //{
        //    //string[] array = login.User.Split('.');
        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.UserTable oLoginAdd = oCompany.UserTables.Item("P1_PNACESS");

        //    string msg = null;
        //    try
        //    {

        //        if (!oLoginAdd.GetByKey(idUser.ToString()))
        //        {
        //            oLoginAdd.Name = login.email;
        //            oLoginAdd.Code = idUser;
        //            oLoginAdd.UserFields.Fields.Item("U_P1_PNPassword").Value = "NOVO";
        //            if (oLoginAdd.Add() != 0)
        //            {
        //                msg = oCompany.GetLastErrorDescription();
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        msg = e.Message;
        //    }

        //    if (msg != null)
        //    {
        //        Log.Gravar(msg, Log.TipoLog.Erro);
        //    }

        //    return msg;

        //}
        //#endregion

        //#region Listar Usuários
        //public List<Login> List()
        //{
        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

        //    List<Login> colaboradores = new List<Login>();

        //    StringBuilder SQL = new StringBuilder();
        //    SQL.AppendLine("SELECT \"empID\", \"firstName\", \"lastName\" FROM \"OHEM\" ORDER BY \"firstName\" ");
        //    if (Conexao.Company.DbServerType == SAPbobsCOM.BoDataServerTypes.dst_HANADB)
        //    {
        //        SQL.AppendLine("WITH parameters('LOCALE' = 'pt-BR')");
        //    }

        //    oRecordSet.DoQuery(SQL.ToString());
        //    while (!oRecordSet.EoF)
        //    {
        //        colaboradores.Add(
        //            new Login()
        //            {
        //                Id = int.Parse(oRecordSet.Fields.Item("empID").Value.ToString()),
        //                User = oRecordSet.Fields.Item("firstName").Value.ToString(),
        //                UserLastName = oRecordSet.Fields.Item("lastName").Value.ToString()

        //            });
        //        oRecordSet.MoveNext();
        //    }

        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
        //    oRecordSet = null;

        //    return colaboradores;
        //}
        //#endregion

        //#region  Listar Usuários com permisão de apontar hora 
        //public List<Autorizacoes> ListUsersApontarHora(Login login)
        //{
        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

        //    List<Autorizacoes> colaboradores = new List<Autorizacoes>();

        //    StringBuilder SQL = new StringBuilder();
        //    SQL.AppendLine("SELECT \"empID\", \"firstName\", \"lastName\", \"U_P1_YNLIMITDT\", \"U_P1_LIMITDATE\", \"U_P1_Grupo\" FROM \"OHEM\" T0 ");
        //    SQL.AppendLine("LEFT JOIN \"@P1_ACCESS\" T1 ON (T0.\"empID\" = T1.\"Code\") ");
        //    SQL.AppendLine("LEFT JOIN \"@P1_ACCGRP\" T2 ON (T2.\"Code\" = T1.\"U_P1_Grupo\") ");
        //    SQL.AppendLine("WHERE 1 = 1 ");
        //    SQL.AppendLine("AND \"U_P1_LSTATUS\" = 'sim' ");
        //    //SQL.AppendLine("AND ((T1.\"U_P1_RevHora\" = 'N' OR COALESCE(T1.\"U_P1_RevHora\",'') = '') AND (T2.\"U_P1_RevHora\" = 'N' OR COALESCE(T2.\"U_P1_RevHora\",'') = '')) ");
        //    SQL.AppendLine("AND (T1.\"U_P1_APHoras\" = 'R' OR T1.\"U_P1_APHoras\" = 'T') OR (T2.\"U_P1_APHoras\" = 'R' OR T2.\"U_P1_APHoras\" = 'T') ");
        //    SQL.AppendLine("ORDER BY \"firstName\" ");

        //    oRecordSet.DoQuery(SQL.ToString());
        //    while (!oRecordSet.EoF)
        //    {
        //        Autorizacoes colaborador = new Autorizacoes();
        //        colaborador.UserId = int.Parse(oRecordSet.Fields.Item("empID").Value.ToString());
        //        colaborador.UserName = oRecordSet.Fields.Item("firstName").Value.ToString();
        //        colaborador.UserLastName = oRecordSet.Fields.Item("lastName").Value.ToString();
        //        colaborador.YNLimitDate = oRecordSet.Fields.Item("U_P1_YNLIMITDT").Value.ToString();
        //        colaborador.LimitDate = DateTime.Parse(oRecordSet.Fields.Item("U_P1_LIMITDATE").Value.ToString());

        //        AutorizacoesGrupo grupo = new AutorizacoesGrupo();
        //        grupo.Name = oRecordSet.Fields.Item("U_P1_GRUPO").Value.ToString();
        //        colaborador.Grupo = grupo;

        //        if (login.Autorizacoes.Coordenador == "Y" && login.Autorizacoes.Tipo != "Adm")
        //        {
        //            if (colaborador.Grupo.Name == login.Autorizacoes.Grupo.Name)
        //            {
        //                colaboradores.Add(colaborador);
        //            }
        //        }
        //        else
        //        {
        //            colaboradores.Add(colaborador);
        //        }

        //        oRecordSet.MoveNext();
        //    }

        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
        //    oRecordSet = null;

        //    return colaboradores;
        //}
        //#endregion

        //#region att Senha Vazia PN
        //public void attSenhaVaziaPN(Login login)
        //{
        //    string msg = null;
        //    SAPbobsCOM.Company oCompany;
        //    oCompany = Conexao.Company;
        //    //SAPbobsCOM.UserTable oAccess = oCompany.UserTables.Item("OCRD");

        //    //if (oAccess.GetByKey(login.CodPNLink.ToString()))
        //    //{
        //    //    //oAccess.UserFields.Fields.Item("U_P1_Password").Value = "NOVO";

        //    //    //int iRetcode = oAccess.Update();

        //    //    //if (iRetcode != 0)
        //    //    //{
        //    //    //    msg = oCompany.GetLastErrorDescription();
        //    //    //    Log.Gravar(msg, Log.TipoLog.Erro);
        //    //    //}
        //    //}

        //    //System.Runtime.InteropServices.Marshal.ReleaseComObject(oAccess);
        //    //oAccess = null;

        //    if (msg != null)
        //    {
        //        Log.Gravar(msg, Log.TipoLog.Erro);
        //    }
        //}
        //#endregion

        //#region Pegar Usuários por pesquisa
        //public List<Login> Search(string pesquisa)
        //{
        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

        //    List<Login> colaboradores = new List<Login>();

        //    StringBuilder SQL = new StringBuilder();
        //    SQL.AppendLine("SELECT TOP 15 \"empID\", \"firstName\", \"lastName\" FROM \"OHEM\" ");
        //    SQL.AppendLine("WHERE (( UPPER(\"empID\") like '%" + pesquisa + "%') OR ( UPPER(\"firstName\") like '%" + pesquisa + "%') OR ( UPPER(\"lastName\") like '%" + pesquisa + "%')) ");

        //    oRecordSet.DoQuery(SQL.ToString());
        //    while (!oRecordSet.EoF)
        //    {
        //        colaboradores.Add(
        //            new Login()
        //            {
        //                Id = int.Parse(oRecordSet.Fields.Item("empID").Value.ToString()),
        //                User = oRecordSet.Fields.Item("firstName").Value.ToString(),
        //                UserLastName = oRecordSet.Fields.Item("lastName").Value.ToString()

        //            });
        //        oRecordSet.MoveNext();
        //    }


        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
        //    oRecordSet = null;

        //    return colaboradores;
        //}
        //#endregion

        //#region Pegar Revisor por pesquisa
        //public List<Login> SearchRevisores(string pesquisa)
        //{
        //    SAPbobsCOM.Company oCompany = Conexao.Company;
        //    SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

        //    List<Login> colaboradores = new List<Login>();

        //    StringBuilder SQL = new StringBuilder();
        //    SQL.AppendLine("SELECT TOP 15 CAST(\"U_P1_EMPId\" AS INT) as \"U_P1_EMPId\", T0.\"Name\", T1.\"lastName\" FROM \"@P1_ACCESS\" T0");
        //    SQL.AppendLine("LEFT JOIN \"OHEM\" T1 ON(T1.\"empID\" = T0.\"Code\")");
        //    SQL.AppendLine("WHERE((UPPER(\"U_P1_EMPId\") like '%" + pesquisa + "%') OR(UPPER(\"Name\") like '%" + pesquisa + "%') OR(UPPER(\"lastName\") like '%" + pesquisa + "%'))");
        //    SQL.AppendLine("AND \"U_P1_RevHora\" = 'S' ORDER BY \"Name\" ASC");

        //    oRecordSet.DoQuery(SQL.ToString());
        //    while (!oRecordSet.EoF)
        //    {
        //        colaboradores.Add(
        //            new Models.Login()
        //            {
        //                Id = int.Parse(oRecordSet.Fields.Item("U_P1_EMPId").Value.ToString()),
        //                User = oRecordSet.Fields.Item("Name").Value.ToString() + " " + oRecordSet.Fields.Item("lastName").Value.ToString()
        //            });
        //        oRecordSet.MoveNext();
        //    }


        //    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
        //    oRecordSet = null;

        //    return colaboradores;
        //}
        ////#endregion
    }
}