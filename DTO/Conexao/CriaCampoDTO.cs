﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    class CriaCampoDTO
    {
        public class CriaCampo
        {
            public string Description { get; set; }
            public string Name { get; set; }
            public string SubType { get; set; }
            public string TableName { get; set; }
            public int EditSize { get; set; }
            public string Type { get; set; }
            public List<ValidValuesMD> ValidValuesMD { get; set; }

        }

        public class ValidValuesMD
        {
            public string Description { get; set; }
            public string Value { get; set; }
         
        }

    }
}
