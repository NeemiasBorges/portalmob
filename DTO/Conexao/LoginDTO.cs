﻿
using Utils;

namespace DTO
{
    public class LoginDTO
    {
        public string CompanyDB { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }
        //public int Language { get; set; }

        /// <summary>
        /// construtor para gerar o json de login
        /// </summary>
        /// <param name="_CompanyDB">Banco</param>
        /// <param name="_Password">Senha Usuário</param>
        /// <param name="_UserName">Usuário</param>
        public LoginDTO(string _CompanyDB, string _Password, string _UserName)
        {
            CompanyDB = _CompanyDB;
            Password = _Password;
            UserName = _UserName;
            //Language = 3;
        }
    }
}