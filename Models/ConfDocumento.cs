﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PowerOne.Models
{
    public class ConfDocumento
    {

        public string Reembolso { get; set; }
        public string LancamentoDesp { get; set; }
        public string PrestacaoConta { get; set; }
        public string Imposto { get; set; }
        public string Cfop { get; set; }
        public string NomeCFOP { get; set; }
        public string IdFilial { get; set; }
        public string Filial { get; set; }
        public string IdDeposito { get; set; }
        public string Deposito { get; set; }
        public string VisivelUnd { get; set; }
        public string VisivelProj { get; set; }
        public string VisivelItem { get; set; }
        public string Serie { get; set; }
        public string CardcodeVisivel { get; set; }
        public string VisMovimento { get; set; }
        public string VisStatus { get; set; }
        public string ViSolicitante { get; set; }
        public string CamposProcesso { get; set; }
        public string codEtapa { get; set; }
        public string valorPotencial { get; set; }
        public string StatusBloqueio { get; set; }
        public string IsHourBloqueio { get; set; }
        public DateTime HorasBloqueio { get; set; }
        public int BeforeDaysBloqueio { get; set; }
    }
}