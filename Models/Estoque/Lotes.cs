﻿using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Models.Estoque
{
    public class Lotes
    {
        [Display(Name = "WhsCode")]
        public string WhsCode { get; set; }

        [Display(Name = "ItemName")]
        public string ItemName { get; set; }

        [Display(Name = "Quantity")]
        public double Quantity { get; set; }

        [Display(Name = "ItemCode")]
        public string ItemCode { get; set; }

        [Display(Name = "SysNumber")]
        public string SysNumber { get; set; }

        [Display(Name = "BatchNum")]
        public string BatchNum { get; set; }

        [Display(Name = "QuantidadeDisponivel")]
        public double QuantidadeDisponivel { get; set; }

    }
}