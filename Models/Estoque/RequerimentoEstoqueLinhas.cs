﻿using Models.Estoque;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Model.Estoque
{
    public class RequerimentoEstoqueLinhas
    {
        #region Lista de propriedades

        [Display(Name = "Nº Interno")]
        public int DocEntry { get; set; }

        [Display(Name = "Nº Requisição")]
        public int DocNum { get; set; }

        [Display(Name = "Status")]
        public string DocStatus { get; set; }

        [Display(Name = "Dt. Requisição")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DataRequisicao { get; set; }

        [Display(Name = "Nº Linha")]
        public int IdLinha { get; set; }

        [Display(Name = "Código")]
        public string CodigoItem { get; set; }

        [Display(Name = "Descrição")]
        public string DecricaoItem { get; set; }

        [Display(Name = "Grupo Item")]
        public string GroupItem { get; set; }

        [Display(Name = "Quantidade Disponível em Estoque")]
        public double GetQtdDisp { get; set; }

        [Display(Name = "Quantidade")]
        public double Quantidade { get; set; }

        [Display(Name = "Descrição Lote")]
        public string BatchNumber { get; set; }

        [Display(Name = "Controle Lote")]
        public string ManBtchNum { get; set; }

        [Display(Name = "Status Linha")]
        public string StatusLinha { get; set; }

        [Display(Name = "Unidade de Medida")]
        public string UndMedida { get; set; }

        [Display(Name = "Observação")]
        public string Observacao { get; set; }

        [Display(Name = "Projeto")]
        public string Projeto { get; set; }

        [Display(Name = "Centro de Custo")]
        public string OcrCode { get; set; }

        [Display(Name = "Nome Ct. Custo")]
        public string OcrName { get; set; }

        [Display(Name = "Centro de Custo2")]
        public string OcrCode2 { get; set; }

        [Display(Name = "Nome Ct. Custo2")]
        public string OcrName2 { get; set; }

        [Display(Name = "Centro de Custo3")]
        public string OcrCode3 { get; set; }

        [Display(Name = "Nome Ct. Custo3")]
        public string OcrName3 { get; set; }

        [Display(Name = "Centro de Custo4")]
        public string OcrCode4 { get; set; }

        [Display(Name = "Nome Ct. Custo4")]
        public string OcrName4 { get; set; }

        [Display(Name = "Centro de Custo5")]
        public string OcrCode5 { get; set; }
        
        [Display(Name = "Nome Ct. Custo5")]
        public string OcrName5 { get; set; }

        [Display(Name = "Lote")]
        public List<Lotes> Lotes { get; set; }

        [Display(Name = "Código Depósito")]
        public string CodigoDeposito { get; set; }

        [Display(Name = "Descrição Depósito")]
        public string DescricaoDeposito { get; set; }

        #endregion
    }
}