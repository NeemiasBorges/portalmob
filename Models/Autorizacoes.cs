﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PowerOne.Models
{
    public class Autorizacoes
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string UserLastName { get; set; }
        public string SolCompra { get; set; }
        public string MembroComite { get; set; }
        public string CriarSolCompra { get; set; }
        public string PedCompra { get; set; }
        public string CriarPedCompra { get; set; }
        public string ReqEstoque { get; set; }
        public string CriarReqEstoque { get; set; }
        public string Reembolso { get; set; }
        public string CriarLancamentoDesp { get; set; }
        public string CriarPrestacaoContas { get; set; }
        public string LancamentoDesp { get; set; }
        public string PrestacaoContas { get; set; }
        public string CriarReembolso { get; set; }
        public string CriarModeloEmail { get; set; }
        public string ModeloEmail { get; set; }
        public string AssistEnvio { get; set; }
        public string Clientes { get; set; }
        public string CriarGrpClientes { get; set; }
        public string GrpClientes { get; set; }
        public string RegistroEnvEmail { get; set; }
        public string RelatCobranças { get; set; }
        public string IdGrupo { get; set; }
        public string Licensa { get; set; }
        public string ApontamentoHoras { get; set; }
        public string RevisaoHoras { get; set; }
        public string Revisor { get; set; }
        public string RelatorioTimesheet { get; set; }
        public string VisualizarContratos { get; set; }
        public string PN { get; set; }
        public string OportVenda { get; set; }
        public AutorizacoesGrupo Grupo { get; set; }
        public string Coordenador { get; set; }
        public string Tipo { get; set; }
        public string YNLimitDate { get; set; }
        public DateTime LimitDate { get; set; }
        public string MACaddress { get; set; }
        public string ConsultaPN { get; set; }

        //autorizações do cliente
        public string RecebeEmail { get; set; }
        public string AttDados { get; set; }
        public string Questiona { get; set; }
        public string VisualizarFatura { get; set; }
        public string ExibeContrato { get; set; }
    }
}