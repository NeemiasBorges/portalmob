﻿using System.ComponentModel.DataAnnotations;

namespace PowerOne.Models
{
    public class Login
    {
        public int Id { get; set; }
        public int IdFilial { get; set; }

        public int UserId { get; set; }

        //[StringLength(60, MinimumLength = 3, ErrorMessage = "O tamanho do {0} deve estar entre {2} e {1}")]
        //[Required(ErrorMessage = "{0} é obrigatório")]
        //[Display(Name = "Usuário")]
        public string User { get; set; }

        public string UserLastName { get; set; }

        [StringLength(60, MinimumLength = 3, ErrorMessage = "O tamanho do {0} deve estar entre {2} e {1}")]
        [Required(ErrorMessage = "{0} é obrigatório")]
        [Display(Name = "Senha")]
        public string Password { get; set; }

        public string Group { get; set; }

        [Display(Name = "Tema")]
        public Tema Tema { get; set; }

        [Display(Name = "Autorizações")]
        public Autorizacoes Autorizacoes { get; set; }

        public ConfDocumento DocsConfig { get; set; }
        public string MACaddress { get; set; }

        [Required(ErrorMessage = "{0} é obrigatório")]
        [Display(Name = "E-mail")]
        public string email { get; set; }
        public string CodPNLink { get; set; }
        public string Tipo { get; set; }
        public string sessionId { get; set; }


    }
}