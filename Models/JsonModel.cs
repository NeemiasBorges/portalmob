﻿

namespace Models
{
    public class JsonModel
    {
        public string status { get; set; }
        public string message { get; set; }
        public dynamic data { get; set; }

        /// <summary>
        /// Construtor com parametros
        /// </summary>
        /// <param name="_status">Status se foi erro ou sucesso.</param>
        /// <param name="_message">Mensagem referente a ação</param>
        /// <param name="_data">Complemento do retorno informado erro mais especifico ou ID da requisição.</param>
        public JsonModel(string _status, string _message, dynamic _data)
        {
            status = _status;
            message = _message;
            data = _data;
        }

        /// <summary>
        /// Construtor vazio
        /// </summary>
        public JsonModel() { }
    }
}