﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace PowerOne.Util
{
    static class Log
    {
        //Atributos
        private static StreamWriter oStremWriter;
        private static string sArquivo;


        //private static string sBaseArquivo = HttpContext.Current.Request.MapPath(HttpContext.Current.Request.ApplicationPath) + "\\Logs\\";
        private static string sBaseArquivo = System.Web.HttpContext.Current.Server.MapPath("~/Logs");

        #region Escrever no Arquivo de Log
        /// <summary>
        /// Método que grava as mensagens do log no arquivo
        /// </summary>
        /// <param name="sMensagem">Mensagem a ser gravado no log</param>
        /// <param name="sTipo">Tipo da mensagem (Log.Tipo)</param>
        public static void Gravar(string sMensagem, TipoLog oTipoLog)
        {
            string sTipo = "";
            if (oTipoLog == TipoLog.Mensagem)
                sTipo = "[I] MENSAGEM :: " + DateTime.Now.ToString("dd/MM/yyyy - HH:mm:ss") + "; ";
            else if (oTipoLog == TipoLog.Aviso)
                sTipo = "[!] AVISO    :: " + DateTime.Now.ToString("dd/MM/yyyy - HH:mm:ss") + "; ";
            else if (oTipoLog == TipoLog.Erro)
                sTipo = "[E] ERRO     :: " + DateTime.Now.ToString("dd/MM/yyyy - HH:mm:ss") + "; ";

            //Concatena o tipo para formar o arquivo final
            sArquivo = "log" + DateTime.Now.ToString("ddMMyyyy") + ".txt";

            try
            {
                //Verifica se o diretório base não existe
                if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Logs")))
                    Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~/Logs"));

                //Grava a mensagem de Log no arquivo
                oStremWriter = new StreamWriter(sBaseArquivo + "\\" + sArquivo, true, Encoding.Default);
                if (string.IsNullOrEmpty(sTipo))
                    oStremWriter.WriteLine(sMensagem);
                else
                    oStremWriter.WriteLine(sTipo + sMensagem);
                oStremWriter.Flush();
                oStremWriter.Close();
            }
            catch
            { }
        }

        #endregion

        #region Enum de Tipo de Log
        public enum TipoLog
        {
            Mensagem,
            Aviso,
            Erro,
            Nenhum,
        }
        #endregion
    }
}