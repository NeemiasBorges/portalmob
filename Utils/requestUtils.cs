﻿using System;
using System.Web.Script.Serialization;
using System.Threading;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using DAL;

namespace Utils
{
    public static class RequestUtils
    {
        /* Constantes de Requisições */
        public const string GET = "GET";
        public const string POST = "POST";
        public const string PATCH = "PATCH";

        /// <summary>
        /// Requisição para o Service Layer.
        /// </summary>
        /// <param name="url">URL de envio</param>
        /// <param name="metodoEnvio">Método de envio [GET, POST, PATCH].</param>
        /// <param name="sessionId">SessionId que é o que controla todas as interações com o service layer.</param>
        /// <param name="jsonEnvio">Json formatado para envio, caso não tiver, passar null.</param>
        /// <returns>Retorna informando se houve sucesso ou erro.</returns>
        #region MÉTODO DE REQUEST | PARAMETROS: URL, MÉTODO DE ENVIO, SESSION ID E JSON
        public static string Request(string url, string metodoEnvio, string sessionId, string jsonEnvio, bool sAddHeaderPatch)
        {
            JsonUtils jsUtils = new JsonUtils();

            //System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            /* Parametriza a requisição */
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Accept = "application/json;odata=minimalmetadata";
            request.KeepAlive = true; //keep alive
            request.ServicePoint.Expect100Continue = false; //content
            request.ServicePoint.MaxIdleTime = 100000;
            request.ServicePoint.ConnectionLimit = 100;
            request.AllowAutoRedirect = true;

            request.ContentType = "application/json;odata=minimalmetadata;charset=utf8";
            request.Timeout = Timeout.Infinite; //Tempo de TimeOut requisição
            request.Method = metodoEnvio;
            if (sAddHeaderPatch)
            {
                request.Headers.Add("B1S-ReplaceCollectionsOnPatch", "true");
            }
            request.Headers.Add("cookie", $"B1SESSION={sessionId}; CompanyDB={ConstantUtils.SAP_BASE}; ROUTEID=.node" + new Random().Next(0, 4)); //Cookies de permissão para requisições no Service Layer
            request.Headers.Add("Prefer", "odata.maxpagesize=0");

            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
            dynamic results;

            /* Verifica se haverá json para ser 'populado' no body da requisição */
            if (jsonEnvio.Length > 0)
            {
                /* Trata json (body) da requisição */
                System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                Byte[] byteArray = encoding.GetBytes(jsonEnvio);
                request.ContentLength = byteArray.Length;
                request.ContentType = @"application/json";

                using (Stream dataStream = request.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                }
            }

            try
            {
                string retorno;
                /* Faz a requisição e se prepara para capturar o retorno */
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    StreamReader reader = new StreamReader(response.GetResponseStream());
                    var result = reader.ReadToEnd();
                    if (response.Method == PATCH && result.Length == 0)
                    {
                        return MessageUtils.SUCESSO_ATUALIZACAO;
                    }
                    if (response.Method == POST && result.Length == 0)
                    {
                        return MessageUtils.SUCESSO_CANCELAMENTO;
                    }
                    retorno = Convert.ToString(result);
                    JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                    results = JsonConvert.DeserializeObject<dynamic>(retorno);
                }

                return results.ToString();
            }
            catch (WebException ex)
            {
                try
                {
                    if (ex.Message == "The underlying connection was closed: The connection was closed unexpectedly." ||
                        ex.Message == "The underlying connection was closed: A connection that was expected to be kept alive was closed by the server.")
                    {
                        return Request(url, metodoEnvio, LoginDAL.sessionId, jsonEnvio, sAddHeaderPatch);
                    }

                    var resp = new StreamReader(ex.Response.GetResponseStream()).ReadToEnd();

                    dynamic obj = JsonConvert.DeserializeObject(resp);
                    var messageFromServer = obj.error.message;

                    if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory.ToString() + "/erro/"))
                        Directory.CreateDirectory((AppDomain.CurrentDomain.BaseDirectory.ToString() + "/erro/"));

                    string nomeArquivo = AppDomain.CurrentDomain.BaseDirectory.ToString() + "/erro/" + "insert_" + DateTime.Now.ToString("yyyy-MM-dd-HHmmssFFF") + ".txt";

                    results = JsonConvert.DeserializeObject<dynamic>(messageFromServer.ToString());
                    /* CASO A SESSIONID TENHA EXPIRADO, FAZ UMA NOVA CONEXÃO E FORÇA O ENVIO DA REQUEST ATUAL. */
                    if (results.value == MessageUtils.SESSAO_INVALIDA)
                    {
                        LoginDAL login = new LoginDAL();
                        login.Conectar();
                        return Request(url, metodoEnvio, LoginDAL.sessionId, jsonEnvio, sAddHeaderPatch);
                        //return jsUtils.JsonRetorno("-1", "Erro: Sessão Encerrada! Reenvie!", "", "");
                    }

                    /* CRIA UM NOVO ARQUIVO E DEVOLVE UM STREAMWRITER PARA ELE */
                    StreamWriter writer = new StreamWriter(nomeArquivo);
                    writer.WriteLine(messageFromServer.ToString());
                    writer.Close();

                    results = JsonConvert.DeserializeObject<dynamic>(messageFromServer.ToString());
                    return jsUtils.JsonRetorno("-1", "Erro: " + results.value, "", "");
                }
                catch (Exception)
                {

                    return jsUtils.JsonRetorno("-1", "Erro: " + ex.Message, "", "");
                }
            }
        }
        #endregion

        /// <summary>
        /// Método de requisição para o Service Layer em formato batch(lote)
        /// </summary>
        /// <param name="sessionId">essionId que é o que controla todas as interações com o service layer.</param>
        /// <param name="jsonEnvio">Json em formato batch.</param>
        /// <param name="batch">Id do batch para identificar que toda a requisição compoem um lote.</param>
        /// <returns>Retorno se houve sucesso ou então qual o erro capturado.</returns>
        #region MÉTODO DE REQUEST PARA BATCH | PARAMENTROS: SESSION ID, JSON PARA ENVIO E BATCH
        public static string RequestBatch(string sessionId, string jsonEnvio, string batch)
        {
            JsonUtils jsUtils = new JsonUtils();

            /* Parametriza a requisição */
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConstantUtils.URL_BATCH);
            //request.Accept = "application/json;odata=minimalmetadata";
            request.KeepAlive = true; //keep alive
            request.ServicePoint.Expect100Continue = false; //content
            request.AllowAutoRedirect = true;
            request.ContentType = $"multipart/mixed;boundary=batch_{batch}";
            request.Timeout = 10000000; //Tempo de TimeOut requisição
            request.Method = POST;
            request.Headers.Add("cookie", $"B1SESSION={sessionId}; CompanyDB={ConstantUtils.SAP_BASE}; ROUTEID=.node0"); //Cookies de permissão para requisições no Service Layer
            request.Headers.Add("Prefer", "odata.maxpagesize=0");
            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
            dynamic results;

            /* Verifica se haverá json para ser 'populado' no body da requisição */
            if (jsonEnvio.Length > 0)
            {
                /* Trata json (body) da requisição */
                System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                Byte[] byteArray = encoding.GetBytes(jsonEnvio);
                request.ContentLength = byteArray.Length;

                using (Stream dataStream = request.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                }
            }

            try
            {
                /* Faz a requisição e se prepara para capturar o retorno */
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    //var multipartContent = await response.Content.ReadAsMultipartAsync();
                    StreamReader reader = new StreamReader(response.GetResponseStream());

                    results = reader.ReadToEnd();
                }

                return results.ToString();
            }
            catch (WebException ex)
            {
                var resp = new StreamReader(ex.Response.GetResponseStream()).ReadToEnd();

                dynamic obj = JsonConvert.DeserializeObject(resp);
                var messageFromServer = obj.error.message;

                if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory.ToString() + "/erro/"))
                    Directory.CreateDirectory((AppDomain.CurrentDomain.BaseDirectory.ToString() + "/erro/"));

                string nomeArquivo = AppDomain.CurrentDomain.BaseDirectory.ToString() + "/erro/" + "insert_" + DateTime.Now.ToString("yyyy-MM-dd-HHmmssFFF") + ".txt";

                /* CRIA UM NOVO ARQUIVO E DEVOLVE UM STREAMWRITER PARA ELE */
                StreamWriter writer = new StreamWriter(nomeArquivo);
                writer.WriteLine(messageFromServer.ToString());
                writer.Close();

                results = JsonConvert.DeserializeObject<dynamic>(messageFromServer.ToString());
                /* CASO A SESSIONID TENHA EXPIRADO, FAZ UMA NOVA CONEXÃO E FORÇA O ENVIO DA REQUEST ATUAL. */
                if (results.value == MessageUtils.SESSAO_INVALIDA)
                {
                    LoginDAL login = new LoginDAL();
                    login.Conectar();
                    return RequestBatch(LoginDAL.sessionId, jsonEnvio, batch);
                    //return jsUtils.JsonRetorno("-1", "Erro: Sessão Encerrada! Reenvie!", "", "");
                }
                results = JsonConvert.DeserializeObject<dynamic>(messageFromServer.ToString());
            }
            return results.ToString();
        }
        #endregion
    }
}