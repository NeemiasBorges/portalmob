﻿using System.Text;

namespace Utils
{
    public class BatchUtils
    {
        /// <summary>
        /// Monta o corpo da requisição em lote.
        /// </summary>
        /// <param name="batchBody">StringBuilder do corpo que está sendo montado.</param>
        /// <param name="changeSet">ChangeSet da requisição batch</param>
        /// <param name="url">url do corpo batch</param>
        /// <param name="json">json para o batch</param>
        /// <param name="contentId">numero da requisição dentro do batch</param>
        public void MontaBlocoBody(StringBuilder batchBody, string changeSet, string url, string json, int contentId)
        {
            /* Seguir essa estrutura pois o Service Layer confere o formato */
            batchBody.Append($"--changeset_{changeSet}\n");
            batchBody.Append("Content-Type: application/http\n");
            batchBody.Append("Content-Transfer-Encoding: binary\n");
            batchBody.Append($"Content-ID: {contentId}\n\n");

            batchBody.Append(url);//URL
            batchBody.Append("\n");
            batchBody.Append("Content-Type: application/json\n\n");

            batchBody.Append(json);//JSON
            batchBody.Append("\n");
        }
    }
}