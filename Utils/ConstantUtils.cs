﻿using PowerOne.Models;
using System;
using System.Configuration;

namespace Utils
{
    public class ConstantUtils
    {
        #region Constantes do Web.config
        /* Configurações do Banco e Usuário usados para a conexão com o Service Layer */
        public static string SAP_USUARIO = ConfigurationManager.AppSettings["SAP_USUARIO"].ToString();
        public static string SAP_SENHA = ConfigurationManager.AppSettings["SAP_SENHA"].ToString();
        public static string SAP_BASE = ConfigurationManager.AppSettings["SAP_BASE"].ToString();
        public static string Servidor = ConfigurationManager.AppSettings["Server"].ToString();
        public static string SAP_USER_BD = ConfigurationManager.AppSettings["SAP_USER_BD"].ToString();
        public static string SAP_SENHA_BD = ConfigurationManager.AppSettings["SAP_SENHA_BD"].ToString();

        public static string URL_SERVICE_LAYER = ConfigurationManager.AppSettings["URL_SERVICE_LAYER"].ToString();
        #endregion

        public static string URL_ADD_USERFIELDS = URL_SERVICE_LAYER + "UserFieldsMD";
        public static string URL_ADD_USERTABLES = URL_SERVICE_LAYER + "UserTablesMD";

        public static string URL_LOGIN = URL_SERVICE_LAYER + "Login";
        public static string URL_P1_ACESS = URL_SERVICE_LAYER + "U_P1_ACCESS";
        public static string URL_BATCH = URL_SERVICE_LAYER + "$batch";

        // URL'S UDT
        public static string URL_UDT = URL_SERVICE_LAYER + "UserTablesMD";

        // URL'S BUSINESS PARTNER
        public static string URL_BP = URL_SERVICE_LAYER + "BusinessPartners";
        public static string URL_BP_PARAM_CARDCODE = URL_BP + "('{0}')";

        // URL'S CONTRATO
        public static string URL_CONTRATO = URL_SERVICE_LAYER + "BlankAgreement";
        public static string URL_CONTRATO_PARAM_ABSENTRY = URL_CONTRATO + "('{0}')";

        // URL'S ATIVIDADE
        public static string URL_Atividade = URL_SERVICE_LAYER + "Activities";
        public static string URL_Atividade_lastIntegratedId_notNull = URL_SERVICE_LAYER + "Activities?$orderby=U_G2_andamentoId desc&$filter=U_G2_andamentoId ne null&$top=1";
        public static string URL_AllAtividadesIntegratedIds = URL_SERVICE_LAYER + "Activities?$select=U_G2_andamentoId&$filter=U_G2_andamentoId ne null";


        //Informações para Login
        public static Login SESSION = null;
        public static Autorizacoes AUTHORIZATIONS = null;
    }
}