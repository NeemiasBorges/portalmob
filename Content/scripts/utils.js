﻿function GetContractByPN() {

    let idPN = $("#idPn").val()

    $.ajax({
        url: '/TimeSheet/GetContractByPN',
        type: 'get',
        dataType: 'json',
        data: {
            'idPN': idPN
        },
        success: function (data) {
            preencherContratoPorPN(data)
        },
        error: function () {
            alert("Erro ao enviar os dados.");
        }
    });

}
function preencherContratoPorPN(contratos) {
    $('select#idContrato option').remove();

    var row = '';
    row += "<option value='' selected disabled></option>";
    $('select#idContrato').append(row);

    contratos.forEach(function (contrato) {
        row = '';

        row += "<option value='" + contrato.Id + "'>" + contrato.Descricao + "</option>";

        $('select#idContrato').append(row);
    });
}