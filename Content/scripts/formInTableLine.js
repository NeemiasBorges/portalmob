﻿function toggleTableForm(element) {
    //console.log(element)
    
    var _tr = jQuery(element).parent().parent();
    jQuery(element).removeClass('d-none');

    var formVisivel = _tr.find("td:eq( 3 )").find(".input-table-form").first().hasClass('visible-false');

    if (formVisivel) {
        _tr.find("td").each(function () {
            jQuery(this).find('.input-table-form').each(function () {
                jQuery(this).removeClass('visible-false');
            });
            jQuery(this).find('.td-table-form').each(function () {
                jQuery(this).addClass('visible-false');
            });
        });

    } else {
        _tr.find("td").each(function () {
            jQuery(this).find('.input-table-form').each(function () {
                jQuery(this).addClass('visible-false');
            });
            jQuery(this).find('.td-table-form').each(function () {
                jQuery(this).removeClass('visible-false');
            });
        });
    }
}

function trocaDisableContrato() {
    if (!isAnyEmpty($("#idColaborador")) && !isAnyEmpty($("#idPn")) && $('#pesquisar-contrato').prop('disabled')) {
        $('#pesquisar-contrato').prop('disabled', false)
        $('#btn-contrato').prop('disabled', false)
    } else if (isAnyEmpty($("#idColaborador")) && isAnyEmpty($("#idPn")) && !$('#pesquisar-contrato').prop('disabled')) {
        $('#pesquisar-contrato').prop('disabled', true)
        $('#btn-contrato').prop('disabled', true)
    }
}


function adicionarLinha() {
    var form = $('.formLinha');
    if (form.hasClass('form-hide')) {
        form.removeClass('form-hide');
    } else {
        form.addClass('form-hide');
    }
}

function excluirLinha(_index) {
    linhas.splice(_index, 1);
    $('tbody tr').remove();
    if (linhas.length <= 0) {
        $('#botao').prop('disabled', true);
        $("#filial").prop('disabled', false);
        $("#bp").prop('disabled', false);
        $("#pesquisar-fornecedores").prop('disabled', false);
        $("#botao-fornecedores").prop('disabled', false);
        $("#pesquisar-filial").prop('disabled', false);
        $("#botao-filial").prop('disabled', false);
    }
    listar();
}

function checaDisable() {
    if (linhas.length > 0) {
        $('#botao').prop('disabled', false);
        $("#filial").prop('disabled', true);
        $("#pesquisar-fornecedores").prop('disabled', true);
        $("#botao-fornecedores").prop('disabled', true);
        $("#pesquisar-filial").prop('disabled', true);
        $("#botao-filial").prop('disabled', true);
        //return true;
    }
}


/* alertas de perda de dados ao adicionar linha para o usuário */

window.onbeforeunload = confirmExit;
function confirmExit() {
    if (linhas.length > 0) {
        return "Deseja realmente sair desta página?";
    }
}

function confAdd(a) {
    if (a != true) {
        if (linhas.length >= 1) {
            window.onbeforeunload = confirmExit;
            function confirmExit() {
                if (document.getElementById("pesquisar-filial").value != "") {
                    return "Deseja realmente sair desta página?";
                }
            }
        }
    }
}