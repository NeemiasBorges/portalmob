﻿const clock = document.querySelector("#status-timer");

let seconds = 0;
let timer;

function initClock() {
    timer = setInterval(function () {

        // reseta validação de inatividade
        seg = 0;

        seconds++;
        clock.innerHTML = getTimeFromSeconds(seconds);
    }, 1000);
}

function getTimeFromSeconds(seconds) {
    const date = new Date(seconds * 1000);
    return date.toLocaleTimeString("pt-BR", {
        hour12: false,
        timeZone: "UTC"
    });
}

function startClock() {
    clearInterval(timer);
    initClock();
}

function stopClock() {
    clearInterval(timer);
}

//function restartClock() {
//    clearInterval(timer);
//    clock.innerHTML = "00:00:00";
//    seconds = 0;
//}


/* ----- RETOMAR CRONÔMETRO COM HORA DE INÍCIO PRÉ DEFINIDA ---- */
function continueClock(hora) {

    let time = hora.split(':')
    seconds = (parseInt(time[0]) * 3600) + (parseInt(time[1]) * 60) + parseInt(time[2]);

    timer = setInterval(function () {

        // reseta validação de inatividade
        seg = 0;

        seconds++;

        clock.innerHTML = getTimeFromSeconds(seconds);
    }, 1000);
}