﻿//inicio input filial
function SearchFilial() {
    var BPLName = $('#pesquisar-filial').val();
    var BPLName = BPLName.toUpperCase()

    $.ajax({
        url: '/Searchs/SearchFilial',
        type: 'get',
        dataType: 'json',
        data: {
            'BPLName': BPLName,
        },
        success: function (data) {
            listarFilial(data);
        },
    });
}

function listarFilial(listItens) {
    $('tbody#lista-tabela-filial tr').remove();
    listItens.forEach(function (filial) {
        var row = ""
        row += "<td>" + "<input type='radio' name='item-radio' id='" + filial.BplId +"Filial' onchange='preencherFilial(this);listarDepositos()' value='" + filial.BplName + "' data-bplid='" + filial.BplId + "'></td>";
        row += "<td>" + filial.BplName + "</td>";
        row += "<td>" + filial.BplId + "</td>";

        $('tbody#lista-tabela-filial').append("<tr class='tr tr-click' onclick='fnc(this);' data-tipo='filialApontamento' data-id='" + filial.BplId + "Filial'>" + row + "<tr>");
    });
}

function preencherFilial(_radio) {

    $('#pesquisar-filial').val(jQuery(_radio).val());
    $('#idFilial').val(jQuery(_radio).data("bplid"));
    $('#filial-modal').modal('hide');
}

$(document).on('keydown', '#pesquisar-filial', function (tab) {
    if (tab.which == 9 || tab.which == 13) {
        SearchFilial();
        $('#filial-modal').modal('toggle');
    }
});
//fim input filial


//inicio input bp
function SearchPn() {
    var CardName = $('#pesquisar-pn').val();
    var CardName = CardName.toUpperCase()

    $.ajax({
        url: '/Searchs/SearchCliente',
        type: 'get',
        dataType: 'json',
        data: {
            'CardName': CardName,
        },
        success: function (data) {
            listarBpRadio(data)
        },
        error: function () {
            alert("Erro ao enviar os dados.");
        }
    });
}

function listarBpRadio(listItens) {
    $('tbody#lista-tabela-pn tr').remove();
    listItens.forEach(function (item) {
        var row = ""
        row += "<td class='d-none'>" + "<input type='radio' name='item-radio' id='" + item.CardCode + "' onchange='escreverBpInput(this)' ' value='" + item.CardName + "' data-cardcode='" + item.CardCode + "'></td>";
        row += "<td>" + item.CardCode + "</td>";
        row += "<td>" + item.CardName + "</td>";

        $('tbody#lista-tabela-pn').append("<tr class='tr tr-click' onclick='fnc(this);' data-tipo='bp' data-id='" + item.CardCode + "'>" + row + "<tr>");
    });
}

function escreverBpInput(_radio) {
    $('#pesquisar-pn').val(jQuery(_radio).val());
    $('#idPn').val(jQuery(_radio).data("cardcode"));
    $('#pn-modal').modal('toggle');
}

$(document).on('keydown', '#pesquisar-pn', function (tab) {
    if (tab.which == 9 || tab.which == 13) {
        SearchPn();
        $('#pn-modal').modal('toggle');
    }
});
//fim input bp


//inicio input contrato
function SearchContrato() {

    var idPN = $('#idPn').val();
    var IDProfissional = $("#idColaborador").val()

    var idEmpresa = 0
    //console.log($("#idFilial").val())
    if (!isAnyEmpty($("#idFilial"))) {
        idEmpresa = $("#idFilial").val()
    }

    //console.log(IDProfissional)

    var contrato = $('#pesquisar-contrato').val();
    var contrato = contrato.toUpperCase()

    $.ajax({
        url: '/Searchs/ContratoPorPN',
        type: 'get',
        dataType: 'json',
        data: {
            'descricao': contrato,
            'idPN': idPN,
            'IDProfissional': IDProfissional,
            'idEmpresa': idEmpresa,
        },
        success: function (data) {
            listarContrato(data);
        },
    });
}

function listarContrato(listItens) {
    $('tbody#lista-tabela-contrato tr').remove();
    listItens.forEach(function (item) {
        var row = ""
        row += "<td class='d-none'>" + "<input type='radio' name='item-radio' id='" + item.Id +"Contrato' onchange='preencherContrato(this);' value='" + item.Descricao + "' data-id='" + item.Id + "'></td>";
        row += "<td>" + (item.Descricao != "" ? item.Descricao : " - ")  + "</td>";
        row += "<td>" + item.Numero + "</td>";        

        $('tbody#lista-tabela-contrato').append("<tr class='tr tr-click' onclick='fnc(this);' data-tipo='contrato' data-id='" + item.Id +"Contrato'>" + row + "<tr>");
    });
}

function preencherContrato(_radio) {
    //console.log(_radio);
    $('#pesquisar-contrato').val(jQuery(_radio).val());
    $('#idContrato').val(jQuery(_radio).data("id"));
    $('#contrato-modal').modal('hide');
}

$(document).on('keydown', '#pesquisar-contrato', function (tab) {
    if (tab.which == 9 || tab.which == 13) {
        SearchContrato();
        $('#contrato-modal').modal('toggle');
    }
});
//fim input contrato


//inicio input colaborador
function SearchColaborador() {
    var pesquisa = $('#pesquisar-colaborador').val();
    var pesquisa = pesquisa.toUpperCase()

    $.ajax({
        url: '/Searchs/Colaborador',
        type: 'get',
        dataType: 'json',
        data: {
            'pesquisa': pesquisa,
        },
        success: function (data) {
            listarColaborador(data);
        },
    });
}

function listarColaborador(listItens) {
    $('tbody#lista-tabela-colaborador tr').remove();
    listItens.forEach(function (item) {
        var row = "";
        row += "<td class='d-none'>" + "<input type='radio' name='item-radio' id='" + item.Id + "' onchange='preencherColaborador(this);' value='" + item.User + "' data-idcolab='"  + item.Id + "'></td>";
        row += "<td>" + item.Id + "</td>";
        row += "<td>" + item.User + "</td>";
        row += "<td>" + item.UserLastName + "</td>";

        $('tbody#lista-tabela-colaborador').append("<tr class='tr tr-click' onclick='fnc(this)' data-tipo='colaborador' data-id='" + item.Id + "'>" + row + "<tr>");
    });
}

function preencherColaborador(_radio) {
    $('#pesquisar-colaborador').val(jQuery(_radio).val());
    $('#idColaborador').val(jQuery(_radio).data("idcolab"));
    $('#colaborador-modal').modal('hide');
}

$(document).on('keydown', '#pesquisar-colaborador', function (tab) {
    if (tab.which == 9 || tab.which == 13) {
        SearchColaborador();
        $('#colaborador-modal').modal('toggle');
    }
});
//fim input colaborador