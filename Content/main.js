﻿/* Mensagens para usuário */
function escreverMensagemEmHead(msg) {
    $('#alert').text(msg);
    $('#alert').removeClass('alert-hide');
    $('#alert').addClass('alert-danger');
    setTimeout(function () {
        $('#alert').removeClass('alert-danger');
        $('#alert').text(msg);
        $(msg).text('');
    }, 4000);
    window.scroll(0, 0);
}

function escreverMensagemEmInput(id_span, msg) {
    $(id_span).text(msg);
    setTimeout(function () {
        $(id_span).text('');
    }, 4000);
}

function escreverSucesso(msg) {
    $('#alert').text(msg);
    $('#alert').removeClass('alert-hide');
    $('#alert').addClass('alert-success');
    setTimeout(function () {
        $('#alert').removeClass('alert-success');
        $('#alert').text(msg);
        $(msg).text('');
    }, 1000);

}

// *Mensagem com Sweet Alert
function addSucces(msg, a, b) {
    var BodyTxt = "";
    if (a) {
        BodyTxt = '<p>Deseja conferir o último documento adicionado? <br>Essa ação te levará até a página de detalhes.</p>' +
            '<a class="btn btn-group btn-success " href="./RequerimentoEstoque/Index">Ir para Listagem</a>'
    } else {
        BodyTxt = '<p>Deseja conferir o último documento adicionado? <br>Essa ação te levará até a página de detalhes.</p>' +
            '<a class="btn btn-group btn-success " href="./Index">Ir para Listagem</a>'
    }
    let timerInterval
    Swal.fire({
        icon: 'success',
        title: msg,
        html: BodyTxt,
        timer: 4300,
        showConfirmButton: false,
        timerProgressBar: true,
        willOpen: () => {
            Swal.showLoading()
            timerInterval = setInterval(() => {
                const content = Swal.getContent()
                if (content) {
                    const b = content.querySelector('b')
                    if (b) {
                        b.textContent = Swal.getTimerLeft()
                    }
                }
            }, 100)
        },
        willClose: () => {
            clearInterval(timerInterval)
        }
    }).then((result) => {
        if (result.dismiss === Swal.DismissReason.timer) {
        }

    })
}



/* funções auxiliares para validação e máscara */

function isAnyEmpty(el) {
    var isEmpty = false, v;
    el.each(function (e) {
        v = $(this).val();
        if (v == "" || v == null || v == undefined) {
            isEmpty = true
        }
    });
    return isEmpty;
}

function dataFormatada(dataInformada) {
    return (new Date(dataInformada + 'T00:00')).toLocaleDateString('pt-BR');
}

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function SomenteNumero(e, numeros) {

    var v = numeros.value;
    var m = v.match(/,/g);
    if (m && m.length >= 1) {
        if (e.keyCode == "44") {
            return false
        }
    }

    //versao mobile
    //if (e.keyCode == "162" || e.keyCode == "163" || e.keyCode == "17" || e.keyCode == "42" || e.keyCode == "18" ) {
    //	return false
    //}

    //computador
    var tecla = (window.event) ? event.keyCode : e.which;

    if ((tecla > 47 && tecla < 58)) return true;

    else {
        if (tecla == 8 || tecla == 44 || tecla == 13) return true;
        else return false;
    }
}

function SomenteNumeroSemVirgula(e, numeros) {

    var v = numeros.value;
    var m = v.match(/,/g);

    if (e.keyCode == "44") {
        return false
    }
}

function mascaraData(val) {
    var pass = val.value;
    var expr = /[0123456789]/;

    for (i = 0; i < pass.length; i++) {
        // charAt -> retorna o caractere posicionado no índice especificado
        var lchar = val.value.charAt(i);
        var nchar = val.value.charAt(i + 1);

        if (i == 0) {
            // search -> retorna um valor inteiro, indicando a posição do inicio da primeira
            // ocorrência de expReg dentro de instStr. Se nenhuma ocorrencia for encontrada o método retornara -1
            // instStr.search(expReg);
            if ((lchar.search(expr) != 0) || (lchar > 3)) {
                val.value = "";
            }

        } else if (i == 1) {

            if (lchar.search(expr) != 0) {
                // substring(indice1,indice2)
                // indice1, indice2 -> será usado para delimitar a string
                var tst1 = val.value.substring(0, (i));
                val.value = tst1;
                continue;
            }

            if ((nchar != '/') && (nchar != '')) {
                var tst1 = val.value.substring(0, (i) + 1);

                if (nchar.search(expr) != 0)
                    var tst2 = val.value.substring(i + 2, pass.length);
                else
                    var tst2 = val.value.substring(i + 1, pass.length);

                val.value = tst1 + '/' + tst2;
            }

        } else if (i == 4) {

            if (lchar.search(expr) != 0) {
                var tst1 = val.value.substring(0, (i));
                val.value = tst1;
                continue;
            }

            if ((nchar != '/') && (nchar != '')) {
                var tst1 = val.value.substring(0, (i) + 1);

                if (nchar.search(expr) != 0)
                    var tst2 = val.value.substring(i + 2, pass.length);
                else
                    var tst2 = val.value.substring(i + 1, pass.length);

                val.value = tst1 + '/' + tst2;
            }
        }

        if (i >= 6) {
            if (lchar.search(expr) != 0) {
                var tst1 = val.value.substring(0, (i));
                val.value = tst1;
            }
        }
    }

    if (pass.length > 10)
        val.value = val.value.substring(0, 10);
    return true;
}

function mascara(o, f) {
    v_obj = o
    v_fun = f
    setTimeout("execmascara() ", 1)
}

function execmascara() {
    v_obj.value = v_fun(v_obj.value)
}

function moeda(v) {
    v = v.replace(/\D/g, "") // permite digitar apenas numero
    v = v.replace(/(\d{1})(\d{17})$/, "$1.$2") // coloca ponto antes dos ultimos digitos
    v = v.replace(/(\d{1})(\d{13})$/, "$1.$2") // coloca ponto antes dos ultimos 13 digitos
    v = v.replace(/(\d{1})(\d{10})$/, "$1.$2") // coloca ponto antes dos ultimos 10 digitos
    v = v.replace(/(\d{1})(\d{7})$/, "$1.$2") // coloca ponto antes dos ultimos 7 digitos
    v = v.replace(/(\d{1})(\d{1,2})$/, "$1,$2") // coloca virgula antes dos ultimos 2 digitos
    return v;
}

function soNumeros(v) {
    return v.replace(/\D/g, "")
}



/* Gerênciador de selects */

function fnc(_tr) {
    var id = jQuery(_tr).attr("data-id");
    var tipo = jQuery(_tr).attr("data-tipo");

    radiobtn = document.getElementById(id);
    radiobtn.checked = true;

    if (tipo == "item") {
        escreverItemInput(radiobtn);
    } else if (tipo == "itemstq") {
        escreverItemInputestq(radiobtn);
    } else if (tipo == "filial") {
        preencherFilial(radiobtn);
        listarDepositos();
    } else if (tipo == "filialApontamento") {
        preencherFilial(radiobtn);
    } else if (tipo == "bp") {
        escreverBpInput(radiobtn);
    } else if (tipo == "bpColab") {
        escreverBpColabInput(radiobtn);
    } else if (tipo == "imposto") {
        escreverImpostoInput(radiobtn);
    } else if (tipo == "cfop") {
        escreverCFOPInput(radiobtn);
    } else if (tipo == "deposito") {
        preencherDeposito(radiobtn);
    } else if (tipo == "contrato") {
        preencherContrato(radiobtn);
    } else if (tipo == "projeto") {
        preencherProjeto(radiobtn);
    } else if (tipo == "projetoDt") {
        preencherProjetoDt(radiobtn);
    } else if (tipo == "colaborador") {
        preencherColaborador(radiobtn);
    } else if (tipo == "cliente") {
        preencherCliente(radiobtn);
    }else if (tipo == "etapa") {
        escreverEtapaInput(radiobtn);
        console.log('etapa')
    }
}

function fncTimeSheet(_tr) {
    var id = jQuery(_tr).attr("data-id");
    var tipo = jQuery(_tr).attr("data-tipo");

    radiobtn = document.getElementById(id);
    radiobtn.checked = true;

    if (tipo == "pntimesheet") {
        escreverBpEdit(radiobtn);
    } else if (tipo == "contrato") {
        preencherContratoEdit(radiobtn);
    } else if (tipo == "contrato-pesquisa") {
        preencherContratoPesquisa(radiobtn);
    } else if (tipo == "contrato-pesquisaR2") {
        preencherContratoPesquisaR2(radiobtn);
    } else if (tipo == "filialEdit") {
        preencherFilialEdit(radiobtn);
    } else if (tipo == "filialR2") {
        preencherFilialR2(radiobtn);
    } else if (tipo == "colaboradorR2") {
        preencherColaboradorR2(radiobtn);
    }
}



/* Multiplos modais simultâneos */

(function ($, window) {
    'use strict';

    var MultiModal = function (element) {
        this.$element = $(element);
        this.modalCount = 0;
    };

    MultiModal.BASE_ZINDEX = 1040;

    MultiModal.prototype.show = function (target) {
        var that = this;
        var $target = $(target);
        var modalIndex = that.modalCount++;

        $target.css('z-index', MultiModal.BASE_ZINDEX + (modalIndex * 20) + 10);

        // Bootstrap triggers the show event at the beginning of the show function and before
        // the modal backdrop element has been created. The timeout here allows the modal
        // show function to complete, after which the modal backdrop will have been created
        // and appended to the DOM.
        window.setTimeout(function () {
            // we only want one backdrop; hide any extras
            if (modalIndex > 0)
                $('.modal-backdrop').not(':first').addClass('hidden');

            that.adjustBackdrop();
        });
    };

    MultiModal.prototype.hidden = function (target) {
        this.modalCount--;

        if (this.modalCount) {
            this.adjustBackdrop();

            // bootstrap removes the modal-open class when a modal is closed; add it back
            $('body').addClass('modal-open');
        }
    };

    MultiModal.prototype.adjustBackdrop = function () {
        var modalIndex = this.modalCount - 1;
        $('.modal-backdrop:first').css('z-index', MultiModal.BASE_ZINDEX + (modalIndex * 20));
    };

    function Plugin(method, target) {
        return this.each(function () {
            var $this = $(this);
            var data = $this.data('multi-modal-plugin');

            if (!data)
                $this.data('multi-modal-plugin', (data = new MultiModal(this)));

            if (method)
                data[method](target);
        });
    }

    $.fn.multiModal = Plugin;
    $.fn.multiModal.Constructor = MultiModal;

    $(document).on('show.bs.modal', function (e) {
        $(document).multiModal('show', e.target);
    });

    $(document).on('hidden.bs.modal', function (e) {
        $(document).multiModal('hidden', e.target);
    });
}(jQuery, window));




//Exibe input comprovante
function verifStatus() {
    if ($('#statusPagamento').val() == "efetuado") {
        $('#divDataCompr').removeClass('d-none')
    } else {
        $('#divDataCompr').addClass('d-none')
    }
}

//Funcão para a descrição/obs ser auto-size
!function (t, e, i, n) { function s(e, i) { this.element = e, this.$element = t(e), this.init() } var h = "textareaAutoSize", o = "plugin_" + h, r = function (t) { return t.replace(/\s/g, "").length > 0 }; s.prototype = { init: function () { var i = parseInt(this.$element.css("paddingBottom")) + parseInt(this.$element.css("paddingTop")) + parseInt(this.$element.css("borderTopWidth")) + parseInt(this.$element.css("borderBottomWidth")) || 0; r(this.element.value) && this.$element.height(this.element.scrollHeight - i), this.$element.on("input keyup", function (n) { var s = t(e), h = s.scrollTop(); t(this).height(0).height(this.scrollHeight - i), s.scrollTop(h) }) } }, t.fn[h] = function (e) { return this.each(function () { t.data(this, o) || t.data(this, o, new s(this, e)) }), this } }(jQuery, window, document);





// funções atualmente não usadas
/*function trocaClasse(elemento, antiga, nova) {
	elemento.classList.remove(antiga);
	elemento.classList.add(nova);
}

function showTitle() {
	var div = document.querySelector('div');
	trocaClasse(div, 'hideElement', 'showElement');
}*/
// fim funções atualmente não usadas