﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Web.Script.Serialization;
using Utils;
using DTO;
using static Utils.JsonUtils;

namespace DAL
{
    public class LoginDAL : UtilsClassDAL
    {
        private JsonUtils jsonUtils = null;
        public static string sessionId = "";

        /// <summary>
        /// MÉTODO DE CONECTAR NO SERVICE LAYER
        /// </summary>
        /// <returns>JSON COM O STATUS DA AÇÃO | SE SUCESSO SALVA O SESSIONID PUBLICAMENTE</returns>
        #region MÉTODO DE CONECTAR NO SERVICE LAYER | RETORNO: JSON COM O STATUS DA AÇÃO | SE SUCESSO SALVA O SESSIONID PUBLICAMENTE
        public string Conectar()
        {
            jsonUtils = new JsonUtils();

            /* Verifica se já está conectado */
            if (sessionId.Length != 0)
                return jsonUtils.JsonRetorno("0", MessageUtils.SUCESSO_CONEXAO, "", "");

            /* Instancia objeto e transforma no json usado para realizar conexão */
            LoginDTO login = new LoginDTO(ConstantUtils.SAP_BASE, ConstantUtils.SAP_SENHA, ConstantUtils.SAP_USUARIO);

            string json = string.Empty;

            if (ConstantUtils.Servidor != "Server2016\\SQLEXPRESS")
            {
                json = JsonConvert.SerializeObject(login);
            }
            else
            {
                json = JsonConvert.SerializeObject(login,
                    new JsonSerializerSettings()
                    { ContractResolver = new IgnorePropertiesResolver(new[] { "Language" }) }
                );
            }

            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            /* Parametriza a requisição */
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConstantUtils.URL_LOGIN);

            request.Accept = "application/json;odata=minimalmetadata";
            request.KeepAlive = true; //keep alive
            request.ServicePoint.Expect100Continue = false; //content
            request.AllowAutoRedirect = true;
            request.ContentType = "application/json;odata=minimalmetadata;charset=utf8";
            request.Timeout = 10000000;
            request.Method = "POST";

            dynamic results;

            /* Trata json (body) da requisição */
            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            Byte[] byteArray = encoding.GetBytes(json);

            request.ContentLength = byteArray.Length;
            request.ContentType = @"application/json"; //define tipo do body

            using (Stream dataStream = request.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
            }

            try
            {
                string retorno;
                /* Faz a requisição e se prepara para capturar o retorno */
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    StreamReader reader = new StreamReader(response.GetResponseStream());
                    var result = reader.ReadToEnd();
                    retorno = Convert.ToString(result);
                    JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                    results = JsonConvert.DeserializeObject<dynamic>(retorno);
                }

                sessionId = results.SessionId;
                return jsonUtils.JsonRetorno("0", MessageUtils.SUCESSO_CONEXAO, "", "");
            }
            catch (WebException ex)
            {
                var resp = new StreamReader(ex.Response.GetResponseStream()).ReadToEnd();

                dynamic obj = JsonConvert.DeserializeObject(resp);
                var messageFromServer = obj.error.message;

                if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory.ToString() + "/erro/"))
                    Directory.CreateDirectory((AppDomain.CurrentDomain.BaseDirectory.ToString() + "/erro/"));

                string nomeArquivo = AppDomain.CurrentDomain.BaseDirectory.ToString() + "/erro/" + "log_" + DateTime.Now.ToString("yyyy-MM-dd-HHmmssFFF") + ".txt";

                /* CRIA UM NOVO ARQUIVO E DEVOLVE UM STREAMWRITER PARA ELE */
                StreamWriter writer = new StreamWriter(nomeArquivo);
                writer.WriteLine(messageFromServer.ToString());
                writer.Close();

                results = JsonConvert.DeserializeObject<dynamic>(messageFromServer.ToString());
                return jsonUtils.JsonRetorno("-1", MessageUtils.ERRO_CONEXAO + results.value, "", "");
            }
        }
        #endregion
    }
}