﻿using PortalMOB.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controllers
{
    public class HomeController : Controller
    {

        #region services

        private readonly IEstoqueRepository _estoqueRepository;

        public HomeController(
        IEstoqueRepository estoqueRepository)
        {
            _estoqueRepository = estoqueRepository;
        }

        #endregion

        #region Pagina index
        public ActionResult Index()
        {
            if (((PowerOne.Models.Login)Session["auth"]) == null)
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        #endregion


        #region estoque
        public ActionResult Estoque()
        {
            TempData["estoque"] = _estoqueRepository.contEstoque();
            return View();
        }
        #endregion

        #region Configurações
        public ActionResult Configuracoes()
        {
            return View();
        }

        #endregion


    }
}