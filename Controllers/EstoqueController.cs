﻿using PortalMOB.Repositories.Interfaces;
using System.Web.Mvc;
using PowerOne.Controllers;
using PowerOne.Models;
using Models.Estoque;
using System.Collections.Generic;

namespace PortalMOB.Controllers
{
    public class EstoqueController : Controller
    {

        #region Instancing Repositories 
        private readonly IEstoqueRepository _estoqueRepository;

        public EstoqueController(IEstoqueRepository estoqueRepository)
        {
            _estoqueRepository = estoqueRepository;
        }
        #endregion

        #region Index / List
        public ActionResult Index(string dtInicio = null, string dtFim = null, string status = "T")
        {
            Login login = (Login)Session["auth"];
            Autorizacoes autorizacoes = ((Login)Session["auth"]).Autorizacoes;

            if (
                autorizacoes.ReqEstoque == "N" &&
                autorizacoes.Grupo.ReqEstoque == "N"
               )
                return RedirectToAction("NaoAutorizado", "Home");

            List<RequerimentoEstoque> requerimentos = _estoqueRepository.List(
                login, dtInicio, dtFim, status
            );            

            return View(requerimentos);
        }
        #endregion
    }
}