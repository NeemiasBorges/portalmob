﻿using PowerOne.Models;
using PowerOne.Repository;
using PowerOne.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Hosting;
using PortalMOB.Repositories.Interfaces;
using PortalMOB.Models;

namespace PowerOne.Controllers
{
    //[Authorize]
    public class ConfiguracoesController : Controller
    {
        //#region Serviços
        //private ConfiguracoesService _configuracoesService = new ConfiguracoesService();
        //private AutorizacoesService _autorizacoesService = new AutorizacoesService();
        //private ProjetosService _projetosService = new ProjetosService();
        //private LicencaService _licencaService = new LicencaService();
        //private CRMService _CRMService = new CRMService();
        //private AtividadeService _atividadeService = new AtividadeService();
        //private LoginService _loginService = new LoginService();
        //#endregion
        private readonly IConfiguracoesRepository _configuracoesRepository;

        public ConfiguracoesController(
        IConfiguracoesRepository configuracoesRepository)
        {
            _configuracoesRepository = configuracoesRepository;
        }


        #region Configurar Email
        //public ActionResult Email()
        //{
        //    if (((Login)Session["auth"]).Autorizacoes.Tipo != "Adm")
        //    {
        //        return RedirectToAction("NaoAutorizado", "Home");
        //    }

        //    ConfiguracoesEmail configuracoesEmail = _configuracoesService.GetConfiguracoesEmail();

        //    return View(configuracoesEmail);
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Email(ConfiguracoesEmail configEmail)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return View(configEmail);
        //    }

        //    string msg = _configuracoesService.ConfigurarEmail(configEmail);

        //    if (msg != null)
        //    {
        //        TempData["Error"] = msg;
        //        Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
        //    }
        //    else
        //    {
        //        TempData["Success"] = "Email Configurado com Sucesso!";
        //    }

        //    return View();
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Anexo(ConfiguracoesEmail confAnexo)
        //{
        //    string msg = _configuracoesService.ConfiguraAnexo(confAnexo);

        //    if (msg != null)
        //    {
        //        TempData["Error"] = msg;
        //        Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
        //    }
        //    else
        //    {
        //        TempData["Success"] = "Anexos do Email Configurados com Sucesso!";
        //    }

        //    return RedirectToAction(nameof(Email));
        //}
        #endregion

        #region Temas
        public ActionResult Temas()
        {
            return View();
        }

        #endregion



        #region Configurar Modelos RPT para impressao
        //public ActionResult
        //    ConfigurarModelosImpressao(
        //    HttpPostedFileBase solicitacao,
        //    HttpPostedFileBase pedido,
        //    HttpPostedFileBase nfentrada,
        //    HttpPostedFileBase requerimentoEstoque,
        //    HttpPostedFileBase reembolso)
        //{
        //    string msg = null;

        //    try
        //    {
        //        if (solicitacao != null)
        //        {
        //            string fileName = Path.GetFileName(solicitacao.FileName);
        //            string path = Path.Combine(Server.MapPath("~/Files/RPT"), fileName);

        //            // salva o modelo na pasta de ~/Files/RPT do portal
        //            solicitacao.SaveAs(path);

        //            // pega só diretório do arquivo para salvar no SAP
        //            string dirPath = Path.Combine("~/Files/RPT", fileName);

        //            msg = _configuracoesService.ConfigurarModeloImpressao("SC", "Modelo p/ Solicitação de Compras", dirPath);
        //        }
        //        else if (pedido != null)
        //        {
        //            string fileName = Path.GetFileName(pedido.FileName);
        //            string path = Path.Combine(Server.MapPath("~/Files/RPT"), fileName);

        //            // salva o modelo na pasta de ~/Files/RPT do portal
        //            pedido.SaveAs(path);

        //            // pega só diretório do arquivo para salvar no SAP
        //            string dirPath = Path.Combine("~/Files/RPT", fileName);

        //            msg = _configuracoesService.ConfigurarModeloImpressao("PC", "Modelo p/ Pedido de Compra", dirPath);
        //        }
        //        else if (requerimentoEstoque != null)
        //        {
        //            string fileName = Path.GetFileName(requerimentoEstoque.FileName);
        //            string path = Path.Combine(Server.MapPath("~/Files/RPT"), fileName);

        //            // salva o modelo na pasta de ~/Files/RPT do portal
        //            requerimentoEstoque.SaveAs(path);

        //            // pega só diretório do arquivo para salvar no SAP
        //            string dirPath = Path.Combine("~/Files/RPT", fileName);

        //            msg = _configuracoesService.ConfigurarModeloImpressao("RE", "Modelo p/ Requerimento de Estoque", dirPath);
        //        }
        //        else if (reembolso != null)
        //        {
        //            string fileName = Path.GetFileName(reembolso.FileName);
        //            string path = Path.Combine(Server.MapPath("~/Files/RPT"), fileName);

        //            // salva o modelo na pasta de ~/Files/RPT do portal
        //            reembolso.SaveAs(path);

        //            // pega só diretório do arquivo para salvar no SAP
        //            string dirPath = Path.Combine("~/Files/RPT", fileName);

        //            msg = _configuracoesService.ConfigurarModeloImpressao("RB", "Modelo p/ Reembolso", dirPath);
        //        }
        //        else if (nfentrada != null)
        //        {
        //            string fileName = Path.GetFileName(nfentrada.FileName);
        //            string path = Path.Combine(Server.MapPath("~/Files/RPT"), fileName);

        //            // salva o modelo na pasta de ~/Files/RPT do portal
        //            nfentrada.SaveAs(path);

        //            // pega só diretório do arquivo para salvar no SAP
        //            string dirPath = Path.Combine("~/Files/RPT", fileName);

        //            msg = _configuracoesService.ConfigurarModeloImpressao("NE", "Modelo p/ Nota fiscal entrada", dirPath);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        msg = e.Message;
        //    }

        //    if (msg != null)
        //    {
        //        TempData["Error"] = msg;
        //        Log.Gravar(msg, Log.TipoLog.Erro);
        //    }
        //    else
        //    {
        //        TempData["Success"] = "Modelos para impressão de Documentos definidos com sucesso!";
        //    }

        //    return RedirectToAction(nameof(DocConfig));
        //}
        #endregion

        #region DOWNLOAD ARQUIVO RPT
        //public ActionResult DownloadRPT(string code)
        //{
        //    try
        //    {
        //        string file_path = _configuracoesService.GetDiretorioModeloImpressao(code);
        //        string name = Path.GetFileName(file_path);

        //        //FileIOPermission writePermission = new FileIOPermission(FileIOPermissionAccess.Write, file_path);
        //        //if (writePermission.AllFiles == System.Security.Permissions.FileIOPermissionAccess.AllAccess)
        //        //{
        //        //    throw new Exception("Não é possível fazer o download, permissão de leitura não concedida");
        //        //}

        //        return File(file_path, "application/rpt", name);
        //    }
        //    catch (Exception e)
        //    {
        //        TempData["Error"] = e.Message;
        //        return RedirectToAction("DocConfig");
        //    }
        //}

        //private Exception Exception(string v)
        //{
        //    throw new NotImplementedException();
        //}

        #endregion

        #region Trocar Icones
        //[HttpPost]
        //public ActionResult ChangeIcons(string icon)
        //{
        //    if (icon != null)
        //    {
        //        string msg = _configuracoesService.ConfigurarTema(((Models.Login)Session["auth"]).Id.ToString(), null, icon);

        //        if (msg != null)
        //        {
        //            TempData["Error"] = msg;
        //            Log.Gravar(msg, Log.TipoLog.Erro);
        //        }
        //        else
        //        {
        //            ((Models.Login)Session["auth"]).Tema = _configuracoesService.GetTema(((Models.Login)Session["auth"]).Id.ToString());
        //            TempData["Success"] = "icones alterados com sucesso!";
        //        }
        //    }
        //    else
        //    {
        //        TempData["Error"] = "Selecione um icone!";
        //        Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
        //    }

        //    return RedirectToAction(nameof(Temas));
        //}
        #endregion

        #region Upload de Logo
        //[HttpPost]
        //public ActionResult UploadLogo(HttpPostedFileBase logo)
        //{
        //    string msg = null;
        //    try
        //    {
        //        if (logo != null)
        //        {
        //            string path = Path.Combine(Server.MapPath("~/Content/img"), "logo-company.png");
        //            logo.SaveAs(path);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        msg = e.Message;
        //    }

        //    if (msg != null)
        //    {
        //        TempData["Error"] = msg;
        //        Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
        //    }
        //    else
        //    {
        //        TempData["Success"] = "Logo do site alterada com sucesso!";
        //    }

        //    return RedirectToAction(nameof(Temas));
        //}
        #endregion

        #region Configurar Cor do Menu
        [HttpPost]
        public ActionResult TrocarCorMenu(string color)
        {
            Resultado resultado = new Resultado();
            if (color != null)
            {
                resultado = _configuracoesRepository.ConfigurarTema(((Models.Login)Session["auth"]).UserId.ToString(), color, null);

                if (resultado.Msg != null)
                {
                    TempData["Error"] = resultado.Msg;
                    Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
                }
                else
                {
                    ((Models.Login)Session["auth"]).Tema = _configuracoesRepository.GetTema(((Models.Login)Session["auth"]).UserId.ToString());
                    TempData["Success"] = "Cor do menu alterado com sucesso!";
                }
            }
            else
            {
                TempData["Error"] = "Selecione uma cor!";
                Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
            }

            return RedirectToAction(nameof(Temas));
        }
        #endregion

        #region Configura Centros de Custo
        //[HttpPost]
        //public ActionResult CtCustoConfig(int[] ctCusto)
        //{
        //    string msg = _configuracoesService.ConfigurarCentroCusto(ctCusto);

        //    if (msg != null)
        //    {
        //        TempData["Error"] = msg;
        //        Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
        //    }
        //    else
        //    {
        //        TempData["Success"] = "Configurações de dimensões de centro de custo alterada com sucesso!";
        //    }

        //    return RedirectToAction(nameof(DocConfig));
        //}
        //#endregion

        //#region Configurar visibilidades dos doc mkt
        //[HttpPost]
        //public ActionResult DocMktConfig(string VerProjeto, string GrpItm, string UndMedida)
        //{
        //    string msg = _configuracoesService.ConfigurarDocMkt(VerProjeto, GrpItm, UndMedida);

        //    if (msg != null)
        //    {
        //        TempData["Error"] = msg;
        //        Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
        //    }
        //    else
        //    {
        //        TempData["Success"] = "Configurações de visibilidade alterada com sucesso!";
        //    }

        //    return RedirectToAction(nameof(DocConfig));
        //}
        #endregion

        #region Autorizações
        //public ActionResult Autorizacoes()
        //{
        //    if (((Login)Session["auth"]).Autorizacoes.Tipo != "Adm")
        //    {
        //        return RedirectToAction("NaoAutorizado", "Home");
        //    }

        //    TempData["autorizacoes"] = _autorizacoesService.List();
        //    TempData["autorizacoesGrupo"] = _autorizacoesService.ListGrupo();

        //    TempData["confBloqueio"] = _autorizacoesService.PegarConfBloqueio();

        //    return View();
        //}

        //[HttpPost]
        //public ActionResult Autorizacoes(Autorizacoes autorizacoes)
        //{
        //    string msg = _autorizacoesService.Update(autorizacoes);

        //    if (msg != null)
        //    {
        //        TempData["Error"] = msg;
        //        Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
        //    }
        //    else
        //    {
        //        TempData["Success"] = $"Autorizações atualizadas com sucesso.";
        //        ((Login)Session["auth"]).Autorizacoes = _autorizacoesService.PegarAutorizacoes(((Login)Session["auth"]).Id.ToString());
        //        ((Login)Session["auth"]).Autorizacoes.Grupo = _autorizacoesService.PegarGrupoAutorizacoes(((Login)Session["auth"]).Autorizacoes);
        //    }

        //    return RedirectToAction(nameof(Autorizacoes));
        //}
        #endregion

        #region Autorizacoes para Grupo
        //public ActionResult AtualizarAutorizacoesGrupo(AutorizacoesGrupo autorizacoes)
        //{
        //    string msg = _autorizacoesService.AtualizarAutorizacoesGrupos(autorizacoes);

        //    if (msg != null)
        //    {
        //        TempData["Error"] = msg;
        //    }
        //    else
        //    {
        //        TempData["Success"] = "Autorização atualizada com sucesso!";
        //        ((Login)Session["auth"]).Autorizacoes = _autorizacoesService.PegarAutorizacoes(((Login)Session["auth"]).Id.ToString());
        //        ((Login)Session["auth"]).Autorizacoes.Grupo = _autorizacoesService.PegarGrupoAutorizacoes(((Login)Session["auth"]).Autorizacoes);
        //    }

        //    return RedirectToAction("Autorizacoes");
        //}
        #endregion

        #region Adicionar Grupo
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult AddGroup(string GroupName)
        //{
        //    string msg = null;

        //    msg = _autorizacoesService.CriarGrupos(GroupName, GroupName);

        //    if (msg != null)
        //    {
        //        TempData["Error"] = msg;
        //    }
        //    else
        //    {
        //        TempData["Success"] = "Grupo Criado com sucesso.";
        //        ((Login)Session["auth"]).Autorizacoes = _autorizacoesService.PegarAutorizacoes(((Login)Session["auth"]).Id.ToString());
        //        ((Login)Session["auth"]).Autorizacoes.Grupo = _autorizacoesService.PegarGrupoAutorizacoes(((Login)Session["auth"]).Autorizacoes);
        //    }

        //    return RedirectToAction(nameof(Autorizacoes));
        //}
        #endregion

        #region Adicionar/Atualizar o grupo de um usuário
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult AtualizarGrupoUsuario(string idUser, string idGrupo)
        //{
        //    string msg = null;
        //    if (String.IsNullOrEmpty(idGrupo))
        //    {
        //        msg = "Selecione um grupo.";
        //    }
        //    else
        //    {
        //        msg = _autorizacoesService.AtualizarGrupoDoUsuario(idUser, idGrupo);
        //    }

        //    if (msg != null)
        //    {
        //        TempData["Error"] = msg;
        //    }
        //    else
        //    {
        //        TempData["Success"] = "Grupo de usuário adicionado com sucesso.";
        //        ((Login)Session["auth"]).Autorizacoes = _autorizacoesService.PegarAutorizacoes(((Login)Session["auth"]).Id.ToString());
        //        ((Login)Session["auth"]).Autorizacoes.Grupo = _autorizacoesService.PegarGrupoAutorizacoes(((Login)Session["auth"]).Autorizacoes);
        //    }


        //    return RedirectToAction(nameof(Autorizacoes));
        //}

        #endregion


        #region Filial e Depósito padrão
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult DFPadrao(string idFilial, string Filial, string idDeposito, string Deposito = "")
        //{
        //    string msg = null;

        //    msg = _configuracoesService.ConfDFPadrao(idFilial, Filial, idDeposito, Deposito);

        //    if (idFilial == "" || Filial == "" || idDeposito == "" || Deposito == "")
        //    {
        //        TempData["Success"] = "Retirada dos valores padrões de Depósito e Filial feita com sucesso!";
        //        return RedirectToAction(nameof(DocConfig));
        //    }

        //    if (msg == null)
        //    {
        //        TempData["Success"] = "Depósito e Filial Padrões definidos com sucesso!";
        //    }
        //    else
        //    {
        //        TempData["Error"] = msg;
        //    }

        //    return RedirectToAction(nameof(DocConfig));
        //}
        #endregion


    }
}